name := """procurementweb"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

libraryDependencies ++=  Seq(
	javaJdbc,
  	cache,
  	filters,
  	"commons-io" % "commons-io" % "2.4",
  	"ch.qos.logback" % "logback-classic" % "1.1.7",
  	"net.logstash.logback" % "logstash-logback-encoder" % "4.7",
  	"ch.qos.logback" % "logback-access" % "1.1.7",
  	"com.jayway.jsonpath" % "json-path" % "2.2.0",
		"commonlib" % "commonlib" % "1.0-SNAPSHOT",

  	javaWs,
		//logger
		"org.codehaus.janino" % "janino" % "3.0.6",
		"org.codehaus.groovy" % "groovy-all" % "2.4.8",
		"com.ibm.icu" % "icu4j" % "59.1"
  
)

EclipseKeys.preTasks := Seq(compile in Compile)
EclipseKeys.projectFlavor := EclipseProjectFlavor.Java           // Java project. Don't expect Scala IDE
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(EclipseCreateSrc.ManagedClasses, EclipseCreateSrc.ManagedResources)  // Use .class files instead of generated .scala files for views and routes

fork in run := true