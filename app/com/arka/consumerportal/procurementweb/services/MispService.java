package com.arka.consumerportal.procurementweb.services;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import com.arka.consumerportal.procurementweb.services.impl.MispServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

@ImplementedBy(MispServiceImpl.class)
public interface MispService {

	CompletionStage<JsonNode> getMispBooking(String mispBookingId);

	public CompletionStage<JsonNode> updateBookingStatus(String bookingId, JsonNode booking) throws InterruptedException, ExecutionException;
}
