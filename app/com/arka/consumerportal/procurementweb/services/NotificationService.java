package com.arka.consumerportal.procurementweb.services;

import com.arka.consumerportal.procurementweb.services.impl.NotificationServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

@ImplementedBy(NotificationServiceImpl.class)
public interface NotificationService {

	JsonNode createVisitorNotification(String notificationKey, String notificationMsg);

	JsonNode getVisitorNotificationValueByKeyPrefix(String notificationKeyPrefix);

	JsonNode deleteVisitorNotificationByKeyPrefix(String notificationKeyPrefix);
	

}
