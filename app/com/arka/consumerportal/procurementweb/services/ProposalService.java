package com.arka.consumerportal.procurementweb.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.consumerportal.procurementweb.services.impl.ProposalServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

@ImplementedBy(ProposalServiceImpl.class)
public interface ProposalService {

	CompletionStage<JsonNode> getProposals(Map<String, List<String>> queryString, Map<String, List<String>> header);

	CompletionStage<JsonNode> deleteProposal(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header);

	CompletionStage<JsonNode> buyPlan(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);

	CompletionStage<JsonNode> getProposalById(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson);

	CompletionStage<JsonNode> updateProposal(String quoteId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson);

	CompletionStage<JsonNode> verifyProposal(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson);
	
	CompletionStage<JsonNode> purchaseProposal(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson);

	CompletionStage<JsonNode> deleteProposalAttribute(String quoteId, Map<String, List<String>> queryString,
			Map<String, List<String>> header);
	
	CompletionStage<JsonNode> completePurchase(JsonNode bodyJson, Map<String, List<String>> queryString,
			Map<String, List<String>> header);

}
