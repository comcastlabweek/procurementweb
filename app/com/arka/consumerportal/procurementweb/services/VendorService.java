package com.arka.consumerportal.procurementweb.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.consumerportal.procurementweb.services.impl.VendorServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

@ImplementedBy(VendorServiceImpl.class)
public interface VendorService {

	CompletionStage<JsonNode> getVendorList(Map<String, List<String>> queryString, Map<String, List<String>> header);
	
	CompletionStage<JsonNode> getVendor(String id, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP);
}
