package com.arka.consumerportal.procurementweb.services;

import java.util.List;
import java.util.Map;

import com.arka.consumerportal.procurementweb.services.impl.HealthServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

@ImplementedBy(HealthServiceImpl.class)
public interface HealthService {

	JsonNode getHealthHomeRestrictions();
	JsonNode getHealthQuotesRestrictions();
	JsonNode getHealthHomeDisease();
	JsonNode getHealthHomeCriticalDisease();
	Map<String, String> getPropertiesOfHelathErrorMsg();
	List<String> getNotIssuableDiseasesIds(JsonNode healthHomeDisease);
	JsonNode getPincode(String field, String search);
	JsonNode getPincodeByField(String field, String search);
	Map<String, String> getSumAssuredList();

}
