package com.arka.consumerportal.procurementweb.services.impl;

import static com.jayway.jsonpath.Filter.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.productmgmt.constants.DiseaseFields;
import com.arka.common.productmgmt.constants.SupportedCategoriesFields;
import com.arka.common.services.ProcurementServ;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.procurementweb.services.HealthService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.ibm.icu.math.BigDecimal;
import com.ibm.icu.text.NumberFormat;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;

import play.libs.Json;
import play.mvc.Http.Context;
public class HealthServiceImpl implements HealthService {

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	private MandatoryParams mandatoryParams;

	@Inject
	PropUtils propUtils;

	private final static String MAX_OCCURENCE = "max_occurrence";
	private final static String CONSTRAINT_VALUE = "constraint_value";
	private final static String RESTRICTION = "restriction";
	private final static String MIN = "min";
	private final static String MAX = "max";
	private final static String MAX_MEM_COUNT = "max_mem_count";
	//private final static String SUFFIX = "suffix";
	
	private final static String HEALTH = "HEALTH";
	
	Filter notIssuableFilter = filter((a-> a.item().equals(HEALTH)));

	public JsonNode collectHomeRestrictionInfo() {

		ObjectNode restrictionNode = Json.newObject();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		JsonNode restrictionForHealthHomeAttr = procurementServ
				.getRestrictionForAttributes(healthHomeAttrForRestriction(),mandatHP)
				.toCompletableFuture().join();
		System.out.println("the health restrictions got from serv-----------"+restrictionForHealthHomeAttr);
		getSelfAgeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getSpouseAgeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getFatherAgeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getMotherAgeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getSonAgeAgeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getDaughterAgeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getSelfGenderRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getSpouseGenderRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getHealthInsuranceTypeRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getSumAssuredRestriction(restrictionNode, restrictionForHealthHomeAttr);
		getPreExistingRestriction(restrictionNode, restrictionForHealthHomeAttr);

		getMaxMeberCount(restrictionNode);

		return restrictionNode;
	}

	public JsonNode collectQuotesRestrictionInfo() {
		
		ObjectNode restrictionNode = Json.newObject();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		JsonNode restrictionForHealthQoutesAttr = procurementServ
				.getRestrictionForAttributes(healthQoutesAttrForRestriction(), mandatHP)
				.toCompletableFuture().join();
		
		getPremiumAmntRestriction(restrictionNode,restrictionForHealthQoutesAttr);
		getRoomRentLimitRestriction(restrictionNode,restrictionForHealthQoutesAttr);
		getPreExistingIllnessCoverRestriction(restrictionNode,restrictionForHealthQoutesAttr);
		getCopayRestriction(restrictionNode,restrictionForHealthQoutesAttr);
		return restrictionNode;
	}

	private void getPremiumAmntRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthQoutesAttr) {
		if (restrictionForHealthQoutesAttr != null
				&& restrictionForHealthQoutesAttr.hasNonNull(HealthAttributeCode.AH_SUM_ASSURED.getCode())) {
			restrictionForHealthQoutesAttr.get(HealthAttributeCode.AH_SUM_ASSURED.getCode());
		}
		
	}
	
	
	private void getRoomRentLimitRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthQoutesAttr) {
		if (restrictionForHealthQoutesAttr != null
				&& restrictionForHealthQoutesAttr.hasNonNull(HealthAttributeCode.AH_ROOM_RENT_ELIGIBLITY.getCode())) {
			restrictionForHealthQoutesAttr.get(HealthAttributeCode.AH_ROOM_RENT_ELIGIBLITY.getCode());
		}
		
	}
	private void getCopayRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthQoutesAttr) {
		if (restrictionForHealthQoutesAttr != null
				&& restrictionForHealthQoutesAttr.hasNonNull(HealthAttributeCode.AH_CO_PAY.getCode())) {
			restrictionForHealthQoutesAttr.get(HealthAttributeCode.AH_CO_PAY.getCode());
		}
		
	}
	
	private void getPreExistingIllnessCoverRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthQoutesAttr) {
		if (restrictionForHealthQoutesAttr != null
				&& restrictionForHealthQoutesAttr.hasNonNull(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER.getCode())) {
			restrictionForHealthQoutesAttr.get(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER.getCode());
		}
		
	}

	private void getMaxMeberCount(ObjectNode restrictionNode) {
		int maxMeberCount = 0;

		maxMeberCount = getMaxOccurVal(restrictionNode, HealthAttributeCode.AH_SELF_AGE.getCode() + "_" + MAX_OCCURENCE)
				+ getMaxOccurVal(restrictionNode, HealthAttributeCode.AH_SPOUSE_AGE.getCode() + "_" + MAX_OCCURENCE)
				+ getMaxOccurVal(restrictionNode, HealthAttributeCode.AH_FATHER_AGE.getCode() + "_" + MAX_OCCURENCE)
				+ getMaxOccurVal(restrictionNode, HealthAttributeCode.AH_MOTHER_AGE.getCode() + "_" + MAX_OCCURENCE)
				+ getMaxOccurVal(restrictionNode, HealthAttributeCode.AH_DAUGHTER_AGE.getCode() + "_" + MAX_OCCURENCE)
				+ getMaxOccurVal(restrictionNode, HealthAttributeCode.AH_SON_AGE.getCode() + "_" + MAX_OCCURENCE);
		restrictionNode.put(MAX_MEM_COUNT, maxMeberCount);

	}

	private int getMaxOccurVal(JsonNode restrictionNode, String attributeName) {
		int maxMeberCount = 0;

		if (restrictionNode.hasNonNull(attributeName)) {
			maxMeberCount = restrictionNode.get(attributeName).asInt();
		}

		return maxMeberCount;
	}

	private void ageRestrictionSetup(JsonNode ageNode, String attributeName, ObjectNode restrictionNode) {
		System.out.println("this is the age node----------"+ageNode);
		if (ageNode.hasNonNull(MAX_OCCURENCE)) {
			restrictionNode.put(attributeName + "_" + MAX_OCCURENCE, ageNode.get(MAX_OCCURENCE).asText());
		}
		if (ageNode.hasNonNull(RESTRICTION) && ageNode.get(RESTRICTION).hasNonNull(CONSTRAINT_VALUE)
				&& ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).size() > 0) {
			
			if (ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).get(0) != null
					&& ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).get(0).hasNonNull(MIN)) {
				restrictionNode.put(attributeName + "_" + MIN,
						ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).get(0).get(MIN).asText());
			}
			if (ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).get(0) != null
					&& ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).get(0).hasNonNull(MAX)) {
				restrictionNode.put(attributeName + "_" + MAX,
						ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).get(0).get(MAX).asText());
			}
			
		}
	}

	
	/*private void childAgeRestrictionSetup(JsonNode ageNode, String attributeName, ObjectNode restrictionNode) {

		if (ageNode.hasNonNull(MAX_OCCURENCE)) {
			restrictionNode.put(attributeName + "_" + MAX_OCCURENCE, ageNode.get(MAX_OCCURENCE).asText());
		}
		if (ageNode.hasNonNull(RESTRICTION) && ageNode.get(RESTRICTION).hasNonNull(CONSTRAINT_VALUE)
				&& ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).size() > 0) {
			
			ArrayNode constrainArray = restrictionNode.putArray(attributeName + "_" +CONSTRAINT_VALUE);
			ageNode.get(RESTRICTION).get(CONSTRAINT_VALUE).forEach(json->{
				
				ObjectNode constarinJson = Json.newObject();
				if (json.hasNonNull(MIN)) {
					constarinJson.put( MIN,json.get(MIN).asText());
				}
				if (json.hasNonNull(MAX)) {
					constarinJson.put( MAX,json.get(MAX).asText());
				}
				if (json.hasNonNull(SUFFIX)) {
					constarinJson.put(SUFFIX,json.get(SUFFIX).asText());
				}
				
				constrainArray.add(constarinJson);
			});
			
			
			
		}
	}*/
	
	private void genderRestrictionSetup(JsonNode jsonResctionIp, String attributeName, ObjectNode jsonResctionOp) {
		setArrayConstrainValue(jsonResctionIp, attributeName, jsonResctionOp);
	}

	private void healthInsuranceTypRestrictionSetup(JsonNode jsonResctionIp, String attributeName, ObjectNode jsonResctionOp) {
		setArrayConstrainValue(jsonResctionIp, attributeName, jsonResctionOp);
	}
	
	private void sumAssuredRestrictionSetup(JsonNode jsonResctionIp, String attributeName, ObjectNode jsonResctionOp) {
		setArrayConstrainValue(jsonResctionIp, attributeName, jsonResctionOp);
	}
	
	
	private void preExistingRestrictionSetup(JsonNode jsonResctionIp, String attributeName, ObjectNode jsonResctionOp) {
		setArrayConstrainValue(jsonResctionIp, attributeName, jsonResctionOp);
	}
	private void setArrayConstrainValue(JsonNode constrainNode, String attributeName, ObjectNode restrictionNode) {

		if (constrainNode.hasNonNull(RESTRICTION) && constrainNode.get(RESTRICTION).hasNonNull(CONSTRAINT_VALUE)
				&& constrainNode.get(RESTRICTION).get(CONSTRAINT_VALUE).size() > 0) {
			ArrayNode genderArray = restrictionNode.putArray(attributeName);
			constrainNode.get(RESTRICTION).get(CONSTRAINT_VALUE).elements().forEachRemaining(gndr -> {
				genderArray.add(gndr.asText());
			});

		}
	}

	private void getDaughterAgeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_DAUGHTER_AGE.getCode())) {
			ageRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_DAUGHTER_AGE.getCode()),
					HealthAttributeCode.AH_DAUGHTER_AGE.getCode(), restrictionNode);
		}
	}

	private void getSonAgeAgeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_SON_AGE.getCode())) {
			ageRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_SON_AGE.getCode()),
					HealthAttributeCode.AH_SON_AGE.getCode(), restrictionNode);
		}
	}

	private void getMotherAgeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_MOTHER_AGE.getCode())) {
			ageRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_MOTHER_AGE.getCode()),
					HealthAttributeCode.AH_MOTHER_AGE.getCode(), restrictionNode);
		}
	}

	private void getFatherAgeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_FATHER_AGE.getCode())) {
			ageRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_FATHER_AGE.getCode()),
					HealthAttributeCode.AH_FATHER_AGE.getCode(), restrictionNode);
		}
	}

	private void getSpouseAgeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_SPOUSE_AGE.getCode())) {
			ageRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_SPOUSE_AGE.getCode()),
					HealthAttributeCode.AH_SPOUSE_AGE.getCode(), restrictionNode);
		}
	}

	private void getSelfAgeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_SELF_AGE.getCode())) {

			ageRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_SELF_AGE.getCode()),
					HealthAttributeCode.AH_SELF_AGE.getCode(), restrictionNode);
		}
	}

	private void getSelfGenderRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_SELF_GENDER.getCode())) {

			genderRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_SELF_GENDER.getCode()),
					HealthAttributeCode.AH_SELF_GENDER.getCode(), restrictionNode);
		}
	}

	private void getSpouseGenderRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {
		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_SPOUSE_GENDER.getCode())) {
			genderRestrictionSetup(restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_SELF_GENDER.getCode()),
					HealthAttributeCode.AH_SPOUSE_GENDER.getCode(), restrictionNode);

		}

	}

	private void getHealthInsuranceTypeRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {

		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode())) {

			healthInsuranceTypRestrictionSetup(
					restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode()),
					HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode(), restrictionNode);
		}
	}

	private void getSumAssuredRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {
		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_SUM_ASSURED.getCode())) {

			sumAssuredRestrictionSetup(
					restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_SUM_ASSURED.getCode()),
					HealthAttributeCode.AH_SUM_ASSURED.getCode(), restrictionNode);
		}

	}
	
	private void getPreExistingRestriction(ObjectNode restrictionNode, JsonNode restrictionForHealthHomeAttr) {
		
		if (restrictionForHealthHomeAttr != null
				&& restrictionForHealthHomeAttr.hasNonNull(HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode())) {

			preExistingRestrictionSetup(
					restrictionForHealthHomeAttr.get(HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode()),
					HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode(), restrictionNode);
		}
		
	}

	public Map<String, List<String>> healthHomeAttrForRestriction() {

		Map<String, List<String>> queryString = new LinkedHashMap<>();

		queryString.put("code",
				Arrays.asList(HealthAttributeCode.AH_SELF_AGE.getCode(), HealthAttributeCode.AH_SELF_GENDER.getCode(),
						HealthAttributeCode.AH_SPOUSE_AGE.getCode(), HealthAttributeCode.AH_SPOUSE_GENDER.getCode(),
						HealthAttributeCode.AH_FATHER_AGE.getCode(), HealthAttributeCode.AH_MOTHER_AGE.getCode(),
						HealthAttributeCode.AH_SON_AGE.getCode(), HealthAttributeCode.AH_DAUGHTER_AGE.getCode(),
						HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode(),
						HealthAttributeCode.AH_SUM_ASSURED.getCode(),HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode()));
		return queryString;

	}
	
	public Map<String, List<String>> healthQoutesAttrForRestriction() {

		Map<String, List<String>> queryString = new LinkedHashMap<>();

		queryString.put("code",
				Arrays.asList(HealthAttributeCode.AH_SUM_ASSURED.getCode(), HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER.getCode(),
						HealthAttributeCode.AH_ROOM_RENT_ELIGIBLITY.getCode(),HealthAttributeCode.AH_CO_PAY.getCode()));
		return queryString;

	}

	@Override
	public JsonNode getHealthHomeRestrictions() {
		return collectHomeRestrictionInfo();
	}

	@Override
	public Map<String, String> getPropertiesOfHelathErrorMsg() {

		Map<String, String> errorProperties = new LinkedHashMap<>();
		errorProperties.put("health.insurnce.for.error", propUtils.get("health.insurnce.for.error"));
		errorProperties.put("self.age.range.error", propUtils.get("self.age.range.error"));
		errorProperties.put("spouse.age.range.error", propUtils.get("spouse.age.range.error"));
		errorProperties.put("children.age.range.error", propUtils.get("children.age.range.error"));
		errorProperties.put("mother.age.range.error", propUtils.get("mother.age.range.error"));
		errorProperties.put("father.age.range.error", propUtils.get("father.age.range.error"));
		errorProperties.put("pincode.error", propUtils.get("pincode.error"));
		errorProperties.put("self.gender.error", propUtils.get("self.gender.error"));
		errorProperties.put("spouse.gender.error", propUtils.get("spouse.gender.error"));
		errorProperties.put("missing.pre.exixting.illness.status",
				propUtils.get("missing.pre.exixting.illness.status"));
		errorProperties.put("missing.pre.exixting.illness.values",
						propUtils.get("missing.pre.exixting.illness.values"));
		errorProperties.put("missing.parents.age", propUtils.get("missing.parents.age"));
		errorProperties.put("try.again.later", propUtils.get("try.again.later"));
		errorProperties.put("children.age.diffrent.error", propUtils.get("children.age.diffrent.error"));
		errorProperties.put("parent.age.diffrent.error", propUtils.get("parent.age.diffrent.error"));
 
		
		return errorProperties;
	}


	@Override
	public JsonNode getHealthHomeDisease() {
		Map<String,List<String>> qmap = new HashMap<>();
		qmap.put("code", Arrays.asList("HEALTH"));
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		return procurementServ.getDiseasesList(qmap, mandatHP).toCompletableFuture().join();
	}
	
	@Override
	public JsonNode getHealthHomeCriticalDisease() {
		Map<String,List<String>> qmap = new HashMap<>();
		qmap.put("code", Arrays.asList("HEALTH"));
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		qmap.put(
				DiseaseFields.DISEASE_SUPPORTED_CATEGORIES.value() + "."
						+ SupportedCategoriesFields.NOT_ISSUABLE.value(),
				new ArrayList<>(Arrays.asList("true")));

		return procurementServ.getDiseasesList(qmap, mandatHP).toCompletableFuture().join();
	}


	@Override
	public JsonNode getHealthQuotesRestrictions() {
		return collectQuotesRestrictionInfo();
	}

	
	
	@Override
	public List<String> getNotIssuableDiseasesIds(JsonNode healthHomeDisease) {
		List<String> notIssuableDiseasesList = new ArrayList<>();
		if(healthHomeDisease.hasNonNull("items")){
			
			healthHomeDisease.get("items").iterator().forEachRemaining(disease -> {
				
				if(disease.hasNonNull("not_issuable")){
					try{
					List<Object> notIssuableDiseases = JsonPath.parse(disease.toString()).read("$.not_issuable[?]", notIssuableFilter);
					
					if(notIssuableDiseases.size()>0){
						notIssuableDiseasesList.add(disease.get("id").asText());
					}
					}catch(Exception e){
					}
				}
			});
		}
		
		return notIssuableDiseasesList;
	}

	@Override
	public JsonNode getPincode(String field,String search) {
		Map<String, List<String>> mandatHP = mandatoryParams.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> queryMap = new LinkedHashMap<>();
		queryMap.put(field, Arrays.asList(search));
		queryMap.putAll( mandatQP);
		return procurementServ.searchPostalCodes(queryMap, mandatHP).toCompletableFuture().join();
	}
	@Override
	public JsonNode getPincodeByField(String field,String search) {
		Map<String, List<String>> mandatHP = mandatoryParams.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> queryMap = new LinkedHashMap<>();
		queryMap.put(field, Arrays.asList(search));
		queryMap.putAll( mandatQP);
		return procurementServ.getPostalCodes(queryMap, mandatHP).toCompletableFuture().join();
	}

	@Override
	public Map<String, String> getSumAssuredList() {
		JsonNode healthHomeRestrictions = getHealthHomeRestrictions();
		Map<String, String> sumAssured = new LinkedHashMap<>();
		
		if(healthHomeRestrictions!=null && healthHomeRestrictions.hasNonNull(HealthAttributeCode.AH_SUM_ASSURED.getCode())){
			
			healthHomeRestrictions.get(HealthAttributeCode.AH_SUM_ASSURED.getCode()).spliterator().forEachRemaining(
				(val)->{sumAssured.put(val.asText(),
						NumberFormat.getInstance(new Locale("en", "in")).format(new BigDecimal(val.asText())));
			});
			
}
return sumAssured;
	}
}
