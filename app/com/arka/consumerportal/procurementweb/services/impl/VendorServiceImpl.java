package com.arka.consumerportal.procurementweb.services.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.arka.common.services.ProcurementServ;
import com.arka.common.services.UserServ;
import com.arka.consumerportal.procurementweb.services.VendorService;
import com.fasterxml.jackson.databind.JsonNode;


public class VendorServiceImpl implements VendorService {

		@Inject
		UserServ userserv;

		@Inject 
		ProcurementServ procurementserv;
		
		@Override
		public CompletionStage<JsonNode> getVendorList(Map<String, List<String>> queryString,
				Map<String, List<String>> header) {
			return userserv.getVendorList(queryString, header);
		}
		
		@Override
		public CompletionStage<JsonNode> getVendor(String id, Map<String, List<String>> queryString,
				Map<String, List<String>> header) {
			return userserv.getVendor(id, queryString, header);
		}
}
