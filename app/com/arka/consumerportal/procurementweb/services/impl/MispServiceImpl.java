package com.arka.consumerportal.procurementweb.services.impl;

import java.util.HashMap;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import com.arka.common.services.PolicyService;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.procurementweb.services.MispService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

public class MispServiceImpl implements MispService {

	@Inject
	private PolicyService policyService;

	@Inject
	PropUtils propUtils;

	@Override
	public CompletionStage<JsonNode> getMispBooking(String bookingId) {
		return policyService.getMispBooking(bookingId, new HashMap<>(), new HashMap<>());
	}

	@Override
	public CompletionStage<JsonNode> updateBookingStatus(String bookingId, JsonNode booking)
			throws InterruptedException, ExecutionException {
		return policyService.patchMispBooking(bookingId, new HashMap<>(), new HashMap<>(), booking);
	}

}
