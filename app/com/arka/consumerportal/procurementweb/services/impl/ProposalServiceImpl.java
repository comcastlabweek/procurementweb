package com.arka.consumerportal.procurementweb.services.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.services.ProcurementServ;
import com.arka.consumerportal.procurementweb.services.ProposalService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

public class ProposalServiceImpl implements ProposalService {

	@Inject
	ProcurementServ procurementServ;
	
	@Override
	public CompletionStage<JsonNode> getProposals(Map<String,List<String>> queryString,
			Map<String, List<String>> header) {
		return procurementServ.getProposals(queryString, header);
	}
	
	@Override
	public CompletionStage<JsonNode> deleteProposal(String proposalId,Map<String,List<String>> queryString,
			Map<String, List<String>> header){
		return procurementServ.deleteproposal(proposalId, queryString, header);
	}
	

	@Override
	public CompletionStage<JsonNode> buyPlan(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return procurementServ.buyPlan(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> getProposalById(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return procurementServ.getProposalById(proposalId, queryString, header);
	}

	@Override
	public CompletionStage<JsonNode> updateProposal(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return procurementServ.updateProposal(proposalId, queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> verifyProposal(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return procurementServ.validateProposal(proposalId, queryString, header, bodyJson);
	}
	
	@Override
	public CompletionStage<JsonNode> purchaseProposal(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return procurementServ.purchaseProposal(proposalId, queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> deleteProposalAttribute(String proposalId, Map<String, List<String>> queryString,
			Map<String, List<String>> header) {
		return procurementServ.deleteProposalAttributes(proposalId, queryString, header);
	}
	
	@Override
    public CompletionStage<JsonNode> completePurchase(JsonNode bodyJson, Map<String, List<String>> queryString,
    		Map<String, List<String>> header) {
		return procurementServ.completePurchase(bodyJson, queryString, header);
    }

}
