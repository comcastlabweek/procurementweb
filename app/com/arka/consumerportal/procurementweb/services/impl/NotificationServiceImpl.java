package com.arka.consumerportal.procurementweb.services.impl;

import com.arka.common.provider.cache.CacheService;
import com.arka.consumerportal.procurementweb.services.NotificationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;

public class NotificationServiceImpl implements NotificationService {

	@Inject
	private CacheService cache;

	/**
	 * 
	 * @param notificationKey
	 *            key in catch, for uniqueness visitorId_timestamp added as
	 *            key
	 * @param notificationMsg
	 *            value in catch, 
	 */
	@Override
	public JsonNode createVisitorNotification(String notificationKey, String notificationMsg) {
		ObjectNode obj = Json.newObject();
		obj.put("value", notificationMsg);
		
		return cache.getOrElse(notificationKey +"_"+ System.nanoTime(), () ->  obj.toString()).toCompletableFuture()
				.join();
	}

	/**
	 * 
	 * @param notificationKeyPrefix
	 *            delete all data start with notificationKeyPrefix
	 * 
	 */
	@Override
	public JsonNode deleteVisitorNotificationByKeyPrefix(String notificationKeyPrefix) {
		return cache.inValidateCacheWithPrefix(notificationKeyPrefix).toCompletableFuture().join();
	}

	/**
	 * 
	 * @param notificationKeyPrefix
	 *            get all data start with notificationKeyPrefix
	 * 
	 */
	@Override
	public JsonNode getVisitorNotificationValueByKeyPrefix(String notificationKeyPrefix) {
		return cache.getValueWithKeyPrefix(notificationKeyPrefix).toCompletableFuture().join();
	}

}
