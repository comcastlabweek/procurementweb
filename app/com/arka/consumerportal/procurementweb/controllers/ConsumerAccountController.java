package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;

import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.models.constant.Status;
import com.arka.common.procurement.constants.DateConstants;
import com.arka.common.services.CmsServ;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.UserServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.EncodingUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.consumerportal.procurementweb.services.ProposalService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

public class ConsumerAccountController extends Controller {

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	ProposalService proposalService;

	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	CmsServ cmsServ;

	@Inject
	UserServ userServ;

	@Inject
	MandatoryParams mandatoryParams;

	public CompletionStage<Result> getConsumerProposals(String userId) throws InterruptedException, ExecutionException {

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (StringUtils.isNotEmpty(userId)) {

			Map<String, List<String>> queryString = new HashMap<>();
			String mispId = request().getQueryString(SessionTrackingParams.MISP_ID.getValue()) != null
					? request().getQueryString(SessionTrackingParams.MISP_ID.getValue()) : null;

			if (mispId != null) {
				JsonNode mispBookingJson = userServ
						.getReferrerById(CryptUtils.getDecryptLongData(mispId).toString(), mandatQP, mandatHP)
						.toCompletableFuture().get();
				queryString.put("referrer_code",
						Arrays.asList(mispBookingJson.get("referrerCode").asText()));
			} else {
				queryString.put("userId", Arrays.asList(userId.toString()));
			}

			queryString.put("status", Arrays.asList(Status.ACTIVE.name()));
			queryString.put("progress", Arrays.asList("IN_PROGRESS", "AFTER_VENDOR_ACK"));
			queryString.put("updated_time", Arrays.asList("9"));
			queryString.put("nor", Arrays.asList("0"));
			// queryString.putAll(mandatQP);
			JsonNode allCategory = procurementServ.getCategoryList(mandatQP, mandatHP).toCompletableFuture().join();
			return proposalService.getProposals(queryString, mandatHP).thenComposeAsync(proposalJson -> {

				ObjectNode newproposalJson = JsonUtils.newJsonObject();
				ArrayNode itemsArray = newproposalJson.putArray("items");
				if (JsonUtils.isValidField(proposalJson, "items")) {

					ObjectNode filteredProposalJson = JsonUtils.newJsonObject();
					ArrayNode addressArrayNode = filteredProposalJson.putArray("items");

					Spliterator<JsonNode> spliterator = proposalJson.get("items").spliterator();

					StreamSupport.stream(spliterator, false).sorted((j1, j2) -> {
						return EncodingUtils
								.getDateFromString(j2.get("updated_time").asText(),
										DateConstants.RESPONSE_DATE_FORMAT.value())
								.compareTo(EncodingUtils.getDateFromString(j1.get("updated_time").asText(),
										DateConstants.RESPONSE_DATE_FORMAT.value()));
					}).limit(10).forEach(filteredProposal -> {
						addressArrayNode.add(filteredProposal);
					});

					filteredProposalJson.get("items").forEach(proposal -> {
						ObjectNode newProposal = (ObjectNode) proposal;
						if (JsonUtils.isValidField(proposal, "productId")) {
							JsonNode productJson = procurementServ
									.getProductById(proposal.get("productId").asText(), mandatQP, mandatHP)
									.toCompletableFuture().join();
							if (JsonUtils.isValidField(productJson, "image_url")) {
								newProposal.put("image_url", productJson.get("image_url").asText());
							}
							if (JsonUtils.isValidField(productJson, "category_id")) {
								JsonNode categoryJson = procurementServ
										.getCategoryById(productJson.get("category_id").asText(), mandatQP, mandatHP)
										.toCompletableFuture().join();
								if (JsonUtils.isValidField(categoryJson, "_id")) {
									newProposal.set("categoryJson", categoryJson);
								}
							}
						}
						itemsArray.add(newProposal);
					});
					newproposalJson.set("allCategoryJson", allCategory);
					return CompletableFuture.completedFuture(ok(newproposalJson));
				}
				return CompletableFuture.completedFuture(ok(newproposalJson));
			});
		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}

	}

	public CompletionStage<Result> deleteProposal(String proposalId) {

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		return proposalService.deleteProposal(proposalId, mandatQP, mandatHP).thenApplyAsync(respJson -> {

			return ok(respJson);
		});
	}

}
