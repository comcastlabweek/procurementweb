/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.procurement.constants.Source;
import com.arka.common.services.ProcurementServ;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

/**
 * @author neethithevan.r
 *
 */
public class HospitalController extends Controller {

	@Inject
	private PropUtils propUtils;

	@Inject
	ProcurementServ procurementServ;

	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	MandatoryParams mandatoryParams;

	public CompletionStage<Result> getHospitalList() {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		String vendorCode = request().getQueryString("vendorCode");
		map.put(Source.SRC.value(), Arrays.asList(Source.HOSPITAL_LIST.value()));
		map.put("vendorCode", Arrays.asList(vendorCode));
		map.putAll(mandatQP);
		return procurementServ.getHospitalList(map, mandatHP).thenApplyAsync(response -> {
			if (JsonUtils.isValidField(response, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, response.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				data.put("vendorCode", vendorCode);
				return ok(com.arka.consumerportal.procurementweb.views.html.hospital.hospital_list.render(data,
						propUtils));
			} else {
				return ok(errorMsg());
			}
		}, httpExecutionContext.current());
	}

	public CompletionStage<Result> getHospitalCity() {
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.CITY.value()));
		String vendorCode = request().getQueryString("vendorCode");
		return procurementServ.getHospitalList(map, mandatHP).thenApplyAsync(jsonResp -> {
			if (JsonUtils.isValidField(jsonResp, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, jsonResp.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				data.put("vendorCode", vendorCode);
				return ok(data);
			} else {
				return ok(errorMsg());
			}
		});
	}

	public CompletionStage<Result> getCityDetails() {
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.CITY_DETAILS.value()));
		return procurementServ.getHospitalList(map, mandatHP).thenApplyAsync(response -> {
			if (JsonUtils.isValidField(response, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, response.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				return ok(data);
			} else {
				return ok(errorMsg());
			}
		});
	}

	public CompletionStage<Result> getPostalDetails() {
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.POSTALCODE.value()));
		return procurementServ.getHospitalList(map, mandatHP).thenApplyAsync(response -> {
			if (JsonUtils.isValidField(response, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, response.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				return ok(data);
			} else {
				return ok(errorMsg());
			}
		});
	}

	private JsonNode errorMsg() {
		ObjectNode error = Json.newObject();
		error.put("error", "Invalid data");
		return error;
	}
}
