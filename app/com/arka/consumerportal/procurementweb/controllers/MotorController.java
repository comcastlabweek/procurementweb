package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.services.ProcurementServ;
import com.arka.common.utils.JsonUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Inject;

import play.mvc.Controller;
import play.mvc.Http.Context;

public class MotorController extends Controller  {

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	@Inject
	MandatoryParams mandatoryParams;
	
	public CompletionStage<JsonNode> landingPage(Map<String, List<String>> queryStringMap, ArrayNode rtoResp) {
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		JsonNode rtoData = JsonUtils.newJsonObject();
		Map<String, List<String>> queryString = new HashMap<>();
		queryString.put("nor", Arrays.asList("20"));
		queryString.put("pfield", Arrays.asList("city"));
		queryString.put("distinct", Arrays.asList("distinct"));
		rtoData = procurementServ.getRtoDetailsForCarInsurance(queryString, mandatHP).toCompletableFuture()
				.join();
		if (anyCollectionQueryResponseValidator.isValidAnyCollectionQueryResp().test(rtoData)) {
			rtoResp = (ArrayNode) anyCollectionQueryResponseValidator.anyCollectionQueryGetData(rtoData);
		}
		session().put("home", "motor");
		return procurementServ.getVehicleMake(queryStringMap, mandatHP);
	}

}
