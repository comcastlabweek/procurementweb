package com.arka.consumerportal.procurementweb.controllers;

import com.arka.consumerportal.procurementweb.services.NotificationService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.Session;
import play.mvc.Result;

public class NotificationController extends Controller {

	@Inject
	FormFactory formFactory;

	@Inject
	NotificationService notificationService;

	public Result createVisitorNotification() {

		DynamicForm dynamicForm = formFactory.form().bindFromRequest();
		String visitorId = dynamicForm.get("visitorId");
		String visitorNotificatonMsg = dynamicForm.get("visitorNotificatonMsg");
		ObjectNode responseNode = Json.newObject();
		if (visitorId != null && visitorNotificatonMsg != null) {
			responseNode.set("msg", notificationService.createVisitorNotification(visitorId, visitorNotificatonMsg));
			return ok(responseNode);
		}
		responseNode.put("error", "Invalid arguement");
		return ok(responseNode);
	}

	public Result deleteVisitorNotification(String id) {
		ObjectNode responseNode = Json.newObject();
		if (id != null) {
			responseNode.set("msg", notificationService.deleteVisitorNotificationByKeyPrefix(id));
			return ok(responseNode);
		}
		responseNode.put("error", "Invalid arguement");
		return ok(responseNode);
	}

	/**
	 * 
	 * @param id
	 *            if id is null, then use current user visitor id from session
	 *            else use given id
	 * @return all matched result
	 */
	public Result getVisitorNotificationByPrefix(String id) {

		Session session = session();

		ObjectNode responseNode = Json.newObject();

		if ((id == null || id.trim().isEmpty()) && session.get("visitorId") != null) {
			id = session.get("visitorId");
		}
		if (id != null) {
			responseNode.set("msg", notificationService.getVisitorNotificationValueByKeyPrefix(id));
			return ok(responseNode);
		}
		responseNode.put("error", "Invalid arguement");
		return ok(responseNode);
	}
}
