/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.arka.common.constants.HeaderParams;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.SessionServ;
import com.arka.common.utils.CookieUtils;
import com.google.inject.Inject;

import akka.stream.javadsl.Flow;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.WebSocket;

/**
 * @author arul.p
 *
 */
public class QuoteController extends Controller{
	
	@Inject
	MandatoryParams mandatoryParams;
	
	@Inject
	private ProcurementServ procurementServ;
	
	@Inject
	SessionServ sessionServ;
	
	public WebSocket socket() {
		Request request = Context.current().request();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		
		Map<String, List<String>> headerMap = new HashMap<>();
		if (session().containsKey(SessionTrackingParams.SESSION_ID.getValue()) && session().get(SessionTrackingParams.SESSION_ID.getValue()) != null) {
			headerMap.put(HeaderParams.SESSION_ID.getValue(), Arrays.asList(session().get(SessionTrackingParams.SESSION_ID.getValue())));
		}
		if(CookieUtils.isContainKey(request, SessionTrackingParams.VISITOR_ID.getValue()) && 
				CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue()) != null) {
			headerMap.put(HeaderParams.VISITOR_ID.getValue(), Arrays.asList(CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue())));
		}
		return WebSocket.Json.accept(reqHeader -> {
			return Flow.fromFunction(planReqJson -> {
				Map<String, List<String>> queryString = new HashMap<>();
				queryString.put("source", Arrays.asList("correlationid"));
				queryString.putAll(mandatQP);
				return sessionServ.generateUniqueId(queryString, mandatHP, null).thenApplyAsync(sessionResp -> {
						headerMap.put(HeaderParams.CORRELATION_ID.getValue(),
								Arrays.asList(sessionResp.get("correlationId").asText()));
						headerMap.putAll(mandatHP);
						return procurementServ.fetchPlan(mandatQP, headerMap, planReqJson).toCompletableFuture().join();
					
				}).toCompletableFuture().get();
			});
		});
	}

}
