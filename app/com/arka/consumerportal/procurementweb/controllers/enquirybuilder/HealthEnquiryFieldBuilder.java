package com.arka.consumerportal.procurementweb.controllers.enquirybuilder;

import static com.jayway.jsonpath.Criteria.where;
import static com.jayway.jsonpath.Filter.filter;
import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.logging.constants.SystemEvent;
import com.arka.common.utils.CookieUtils;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.arka.consumerportal.procurementweb.services.HealthService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;

import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Http.Response;
import play.mvc.Http.Session;

public class HealthEnquiryFieldBuilder {

	public static final Logger logger = LoggerFactory.getLogger(HealthEnquiryFieldBuilder.class);

	private final static String INSURANCE_TYPE = "insuranceType";
	private final static String SON = "son";
	private final static String DAUGHTER = "daughter";
	private final static String SELF_AGE = "selfAge";
	private final static String GENDER = "gender";
	private final static String SPOUSE_AGE = "spouseAge";
	private final static String SPOUSE_GENDER = "spouseGender";
	private final static String MOTHER_AGE = "motherAge";
	private final static String FATHER_AGE = "fatherAge";
	private final static String SON_AGE = "sonAge";
	private final static String DAUGHTER_AGE = "daughterAge";

	private final static String MALE = "male";
	private final static String FEMALE = "female";

	private final static String SELF_ILLNESS = "selfIllness";
	private final static String SPOUSE_ILLNESS = "spouseIllness";
	private final static String SON_ILLNESS = "sonIllness";
	private final static String DAUGHTER_ILLNESS = "daughterIllness";
	private final static String MOTHER_ILLNESS = "motherIllness";
	private final static String FATHER_ILLNESS = "fatherIllness";
	private final static String INCLUDE_PARENTS = "includeParents";
	private final static String HAS_ANY_DISEASE = "hasAnyDisease";
	private final static String PINCODE = "pincode";
	private final static String PINCODE_LABEL = "pincodeLabel";
	private final static String PINCODE_DISPLAY = "pincodeDisplay";
	private final static String IS_PARENT_AND_ME_HAS_SAME_PINCODE = "isParentAndMeHasSamePincode";
	private final static String PARENTS_PINCODE = "parentsPincode";
	private final static String PARENTS_PINCODE_DISPLAY = "parentsPincodeDisplay";
	private final static String PARENTS_PINCODE_LABEL = "parentsPincodeLabel";
	private final static String DISEASES = "diseases";

	private final static String REMOVE = "remove";
	private final static String SUM_ASSURED = "sumAssured";

	/* Tags */

	private final static String SELF_AGE_TAG = "self,age,family";
	private final static String GENDER_TAG = "self,gender";
	private final static String SPOUSE_AGE_TAG = "spouse,age,family";
	private final static String SPOUSE_GENDER_TAG = "spouse,gender";
	private final static String MOTHER_AGE_TAG = "mother,age,family,parent";
	private final static String FATHER_AGE_TAG = "father,age,family,parent";
	private final static String SON_AGE_TAG = "son,age,family,child";
	private final static String DAUGHTER_AGE_TAG = "daughter,age,family,child";
	private final static String PINCODE_TAG = "pincode,self_family";
	private final static String PARENTS_PINCODE_TAG = "pincode,parent";
	private final static String PARENTS_ME_HAVE_SAME_LOCATION_TAG = "same_location";
	private final static String PARENTS_ME_HAVE_DIFF_LOCATION_TAG = "diff_location";

	private final static String SELF_ILLNESS_TAG = "self,illness";
	private final static String SPOUSE_ILLNESS_TAG = "spouse,illness";
	private final static String SON_ILLNESS_TAG = "son,illness";
	private final static String DAUGHTER_ILLNESS_TAG = "daughter,illness";
	private final static String MOTHER_ILLNESS_TAG = "mother,illness";
	private final static String FATHER_ILLNESS_TAG = "father,illness";
	public final static String HEALTH_INSURANCE_HOME = "health_insurance_home";
	public final static String INPUT_EXCLUDED = "inputExcluded";

	List<Integer> affectedSonPosition = new ArrayList<>();
	List<Integer> affectedDaughterPosition = new ArrayList<>();

	@Inject
	HealthService healthService;

	public void createHealthEnquiryObject(Map<String, String[]> asFormUrlEncodedMap, DynamicForm df,
			ObjectNode enquiryRequestJson, Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie,
			Context context) {

		ArrayNode enquiryAttribute = enquiryRequestJson.putArray("enquiryAttributes");
		List<String> sonsAge = new ArrayList<>();
		List<String> daughtersAge = new ArrayList<>();

		String[] sonsAgeArray = asFormUrlEncodedMap.get(SON_AGE + "[]");
		String[] daughtersAgeArray = asFormUrlEncodedMap.get(DAUGHTER_AGE + "[]");
		String[] remove = asFormUrlEncodedMap.get(REMOVE + "[]") == null ? new String[0]
				: asFormUrlEncodedMap.get(REMOVE + "[]");

		Map<String, String> data = df.data();
		String selfAge = data.get(SELF_AGE);
		String gender = data.get(GENDER);
		String spouseAge = data.get(SPOUSE_AGE);
		String spouseGender = data.get(SPOUSE_GENDER);
		String fatherAge = data.get(FATHER_AGE);
		String motherAge = data.get(MOTHER_AGE);
		String includeParents = data.get(INCLUDE_PARENTS);
		String isParentAndMeHasSamePincode = data.get(IS_PARENT_AND_ME_HAS_SAME_PINCODE);
		String hasAnyDisease = data.get(HAS_ANY_DISEASE);
		String existingDiseases = data.get(DISEASES);
		String insuranceType = data.get(INSURANCE_TYPE);
		String sumAssured = data.get(SUM_ASSURED);

		String pincode = data.get(PINCODE);
		String pincodeLabel = data.get(PINCODE_LABEL);
		String pincodeDisplay = data.get(PINCODE_DISPLAY);
		String parentsPincode = data.get(PARENTS_PINCODE);
		String parentsPincodeLabel = data.get(PARENTS_PINCODE_LABEL);
		String parentsPincodeDisplay = data.get(PARENTS_PINCODE_DISPLAY);

		setStringArrayToList(sonsAgeArray, sonsAge);
		setStringArrayToList(daughtersAgeArray, daughtersAge);
		removeAffectedSon(sonsAge, remove, addParmsToCookie, removeParmsFromCookie);
		removeAffectedDaughter(daughtersAge, remove, addParmsToCookie, removeParmsFromCookie);

		addSelfAge(selfAge, gender, enquiryAttribute, addParmsToCookie, removeParmsFromCookie, remove);
		addChildrenAge(sonsAge, SON, enquiryAttribute, addParmsToCookie, removeParmsFromCookie);
		addChildrenAge(daughtersAge, DAUGHTER, enquiryAttribute, addParmsToCookie, removeParmsFromCookie);
		addSpouseAge(spouseAge, spouseGender, SPOUSE_GENDER, enquiryAttribute, addParmsToCookie, removeParmsFromCookie,
				remove);
		addPincode(pincode, PINCODE, pincodeLabel, pincodeDisplay, enquiryAttribute, addParmsToCookie,
				removeParmsFromCookie);

		if (sumAssured != null) {
			enquiryAttribute.add(getEnquiryAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode(), sumAssured));
			addParmsToCookie.put(SUM_ASSURED, sumAssured);
		} else {
			removeParmsFromCookie.add(SUM_ASSURED);
		}
		if (insuranceType != null) {
			enquiryAttribute
					.add(getEnquiryAttribute(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode(), insuranceType));
			addParmsToCookie.put(INSURANCE_TYPE, insuranceType);
		} else {
			removeParmsFromCookie.add(INSURANCE_TYPE);
		}
		if (includeParents != null && Boolean.parseBoolean(includeParents)) {
			String locationTag = PARENTS_ME_HAVE_DIFF_LOCATION_TAG;
			addParentAge(fatherAge, FATHER_AGE, enquiryAttribute, addParmsToCookie, removeParmsFromCookie, remove);
			addParentAge(motherAge, MOTHER_AGE, enquiryAttribute, addParmsToCookie, removeParmsFromCookie, remove);
			addPincode(parentsPincode, PARENTS_PINCODE, parentsPincodeLabel, parentsPincodeDisplay, enquiryAttribute,
					addParmsToCookie, removeParmsFromCookie);

			if (isParentAndMeHasSamePincode != null && Boolean.parseBoolean(isParentAndMeHasSamePincode)) {
				locationTag = PARENTS_ME_HAVE_SAME_LOCATION_TAG;
			}
			enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_IS_PARENTS_TO_BE_INCLUDED.getCode(),
					Boolean.toString(true), locationTag));

			addParmsToCookie.put(INCLUDE_PARENTS, includeParents);
			addParmsToCookie.put(IS_PARENT_AND_ME_HAS_SAME_PINCODE, isParentAndMeHasSamePincode);
		} else {
			enquiryAttribute.add(getEnquiryAttribute(HealthAttributeCode.AH_IS_PARENTS_TO_BE_INCLUDED.getCode(),
					Boolean.toString(false)));
			removeParmsFromCookie.add(INCLUDE_PARENTS);
		}

		if (hasAnyDisease != null && existingDiseases != null && Boolean.valueOf(hasAnyDisease)) {

			JsonNode existingDiseasesNode = Json.parse(existingDiseases);
			JsonNode healthHomeCiriticalDisease = healthService.getHealthHomeCriticalDisease();
			existingDiseasesNode.fields().forEachRemaining(fm -> {

				String code = null;
				String tag = null;

				// if (fm.getKey().contains(SON_ILLNESS) &&
				// !Arrays.asList(remove).contains(fm.getKey())) {
				if (fm.getKey().contains(SON_ILLNESS)) {
					code = HealthAttributeCode.AH_SON_ILLNESS.getCode();
					String sonIndexTag = "son_" + fm.getKey().split("_")[1];
					tag = SON_ILLNESS_TAG + "," + sonIndexTag + "," + fm.getKey();
				} else if (fm.getKey().contains(DAUGHTER_ILLNESS)) {
					String daughterIndexTag = "daughter_" + fm.getKey().split("_")[1];
					code = HealthAttributeCode.AH_DAUGHTER_ILLNESS.getCode();
					tag = DAUGHTER_ILLNESS_TAG + "," + daughterIndexTag + "," + fm.getKey();
				} else if (fm.getKey().equalsIgnoreCase(SELF_ILLNESS)) {
					code = HealthAttributeCode.AH_SELF_ILLNESS.getCode();
					tag = SELF_ILLNESS_TAG + "," + fm.getKey();
				} else if (fm.getKey().equalsIgnoreCase(SPOUSE_ILLNESS)) {
					code = HealthAttributeCode.AH_SPOUSE_ILLNESS.getCode();
					tag = SPOUSE_ILLNESS_TAG + "," + fm.getKey();
				} else if (fm.getKey().equalsIgnoreCase(FATHER_ILLNESS)) {
					code = HealthAttributeCode.AH_FATHER_ILLNESS.getCode();
					tag = FATHER_ILLNESS_TAG + "," + fm.getKey();
				} else if (fm.getKey().equalsIgnoreCase(MOTHER_ILLNESS)) {
					code = HealthAttributeCode.AH_MOTHER_ILLNESS.getCode();
					tag = MOTHER_ILLNESS_TAG + "," + fm.getKey();
				}

				Iterator<JsonNode> diseaseItr = fm.getValue().elements();

				if (code != null) {
					while (diseaseItr.hasNext()) {
						String diseaseId = diseaseItr.next().asText();
						if (isCriticalDiseaseExist(diseaseId, healthHomeCiriticalDisease)) {
							enquiryAttribute.add(getEnquiryAttributeExclude(code, diseaseId, tag));
						} else {
							enquiryAttribute.add(getEnquiryAttributeWithTag(code, diseaseId, tag));
						}
					}
				}

			});
			addParmsToCookie.put(HAS_ANY_DISEASE, hasAnyDisease);
			addParmsToCookie.put(DISEASES, existingDiseases);
			enquiryAttribute.add(getEnquiryAttribute(
					HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode(), Boolean.toString(true)));
		} else {
			enquiryAttribute.add(getEnquiryAttribute(
					HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode(), Boolean.toString(false)));
			addParmsToCookie.put(HAS_ANY_DISEASE, hasAnyDisease);

		}
		//removeChildrenIfHasDisease(enquiryRequestJson, removeParmsFromCookie);
		//handleHealthCookie(addParmsToCookie, removeParmsFromCookie, context);

	}

	public Map<String, String> handleHealthCookie(Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie, Context context) {
		try {
			Response response = context.response();
			Request request = context.request();
			Map<String, String> cookies = CookieUtils.getCookies(request);

			if (addParmsToCookie != null && !addParmsToCookie.isEmpty()) {

				cookies.putAll(addParmsToCookie);

				if (removeParmsFromCookie != null) {
					removeParmsFromCookie.stream().forEach(cookieName -> {
						if (cookies.containsKey(cookieName)) {
							cookies.remove(cookieName);
						}
					});
				}
				CookieUtils.setCookies(cookies , response);
				return cookies;
			}
		} catch (Exception e) {
			Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put(HEALTH_INSURANCE_HOME, ExceptionUtils.getRootCauseMessage(e));
			logger.error(e.getMessage());
			return errorMap;

		}
		return Collections.emptyMap();
	}

	private boolean isCriticalDiseaseExist(String diseaseId, JsonNode healthHomeCiriticalDisease) {
		AtomicBoolean ab = new AtomicBoolean();
		ab.set(false);
		if (diseaseId != null && healthHomeCiriticalDisease.hasNonNull("items")) {
			healthHomeCiriticalDisease.get("items").forEach(d -> {
				if (d.hasNonNull("id") && d.get("id").asText().equals(diseaseId)) {
					ab.set(true);
					return;
				}
			});
		}
		return ab.get();
	}

	private void removeAffectedSon(List<String> sonsAge, String[] removeMembr, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {
		affectedSonPosition = new ArrayList<>();
		for (int i = 0; i < removeMembr.length; i++) {
			if (removeMembr[i].contains("sonIllness")) {
				String position = removeMembr[i].substring(removeMembr[i].length() - 1);
				affectedSonPosition.add(Integer.parseInt(position) - 1);
			}
		}

	}

	private void removeAffectedDaughter(List<String> daughterAge, String[] removeMembr,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie) {
		affectedDaughterPosition = new ArrayList<>();
		for (int i = 0; i < removeMembr.length; i++) {
			if (removeMembr[i].contains("daughterIllness")) {
				String position = removeMembr[i].substring(removeMembr[i].length() - 1);
				affectedDaughterPosition.add(Integer.parseInt(position) - 1);

			}
		}

	}

	private void addPincode(String pincode, String type, String label, String display, ArrayNode enquiryAttribute,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie) {
		if (pincode != null) {
			if (PARENTS_PINCODE.equals(type)) {
				enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_PINCODE_PARENT.getCode(),
						pincode, PARENTS_PINCODE_TAG));
				enquiryAttribute.add(getEnquiryAttribute("AH-PARENT_POSTAL_CODE", display)); 
				addParmsToCookie.put(PARENTS_PINCODE, pincode);
				addParmsToCookie.put(PARENTS_PINCODE_LABEL, label);
				addParmsToCookie.put(PARENTS_PINCODE_DISPLAY, display);
			} else {
				enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_PINCODE_SELF_OR_FAMILY.getCode(),
						pincode, PINCODE_TAG));
				enquiryAttribute.add(getEnquiryAttribute("AH-POSTAL_CODE", display));   
				addParmsToCookie.put(PINCODE, pincode);
				addParmsToCookie.put(PINCODE_LABEL, label);
				addParmsToCookie.put(PINCODE_DISPLAY, display);
			}
		} else {
			if (PARENTS_PINCODE.equals(type)) {
				removeParmsFromCookie.add(PARENTS_PINCODE);
				removeParmsFromCookie.add(PARENTS_PINCODE_LABEL);
				removeParmsFromCookie.add(PARENTS_PINCODE_DISPLAY);

			} else {
				removeParmsFromCookie.add(PINCODE);
				removeParmsFromCookie.add(PINCODE_LABEL);
				removeParmsFromCookie.add(PINCODE_DISPLAY);
			}
		}
	}

	private void setStringArrayToList(String[] arr, List<String> ageList) {

		if (arr != null) {
			ageList.addAll(Arrays.asList(arr));
		}

	}

	private void addSelfAge(String selfAge, String gender, ArrayNode enquiryAttribute,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie, String[] remove) {
		if (selfAge != null) {
			if (Arrays.asList(remove).contains(SELF_ILLNESS)) {
				enquiryAttribute.add(
						getEnquiryAttributeExclude(HealthAttributeCode.AH_SELF_AGE.getCode(), selfAge, SELF_AGE_TAG));
				removeParmsFromCookie.add("selfDisplay");

				addGender(gender, enquiryAttribute, addParmsToCookie, removeParmsFromCookie, remove, false);
			} else {
				enquiryAttribute.add(
						getEnquiryAttributeWithTag(HealthAttributeCode.AH_SELF_AGE.getCode(), selfAge, SELF_AGE_TAG));
				addParmsToCookie.put("selfDisplay", selfAge);

				addGender(gender, enquiryAttribute, addParmsToCookie, removeParmsFromCookie, remove, true);
			}
			addParmsToCookie.put(SELF_AGE, selfAge);
		} else {
			// removeParmsFromCookie.add( SELF_AGE);
			removeParmsFromCookie.add("selfDisplay");
		}

	}

	private void addGender(String gender, ArrayNode enquiryAttribute, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie, String[] remove, boolean b) {
		if (gender != null && !Arrays.asList(remove).contains(SELF_AGE)
				&& (gender.equalsIgnoreCase(MALE) || gender.equalsIgnoreCase(FEMALE))) {
			if (b) {
				enquiryAttribute.add(
						getEnquiryAttributeWithTag(HealthAttributeCode.AH_SELF_GENDER.getCode(), gender, GENDER_TAG));
			} else {
				enquiryAttribute.add(
						getEnquiryAttributeExclude(HealthAttributeCode.AH_SELF_GENDER.getCode(), gender, GENDER_TAG));
			}
		} else {
			removeParmsFromCookie.add(GENDER);
		}
	}

	private void addSpouseAge(String spouseAge, String spouseGender, String fieldGender, ArrayNode enquiryAttribute,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie, String[] remove) {

		if (spouseAge != null && !spouseAge.trim().isEmpty()) {
			if (Arrays.asList(remove).contains(SPOUSE_ILLNESS)) {
				enquiryAttribute.add(getEnquiryAttributeExclude(HealthAttributeCode.AH_SPOUSE_AGE.getCode(), spouseAge,
						SPOUSE_AGE_TAG));
				removeParmsFromCookie.add("spouseDisplay");
				addSpouseGender(spouseGender, fieldGender, enquiryAttribute, addParmsToCookie, removeParmsFromCookie,
						false);
			} else {
				enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_SPOUSE_AGE.getCode(), spouseAge,
						SPOUSE_AGE_TAG));
				addParmsToCookie.put("spouseDisplay", spouseAge);
				addSpouseGender(spouseGender, fieldGender, enquiryAttribute, addParmsToCookie, removeParmsFromCookie,
						true);
			}
			// addParmsToCookie.put(SPOUSE_AGE, spouseAge);

		} else {
			removeParmsFromCookie.add(SPOUSE_AGE);
			removeParmsFromCookie.add("spouseDisplay");
		}
	}

	private void addSpouseGender(String spouseGender, String field, ArrayNode enquiryAttribute,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie, boolean b) {
		if (spouseGender != null && (spouseGender.equalsIgnoreCase(MALE) || spouseGender.equalsIgnoreCase(FEMALE))) {
			if (b) {
				enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_SPOUSE_GENDER.getCode(),
						spouseGender, SPOUSE_GENDER_TAG));
			} else {
				enquiryAttribute.add(getEnquiryAttributeExclude(HealthAttributeCode.AH_SPOUSE_GENDER.getCode(),
						spouseGender, SPOUSE_GENDER_TAG));
			}
			// addParmsToCookie.put(SPOUSE_GENDER, spouseGender);
		} else {
			removeParmsFromCookie.add(SPOUSE_GENDER);
		}
	}

	private void addChildrenAge(List<String> childrenList, String childType, ArrayNode enquiryAttribute,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie) {
		List<Integer> childrenDisply = new ArrayList<>();
		AtomicInteger childCount = new AtomicInteger();
		if (childrenList.size() > 0) {
			ArrayNode ageNode = Json.newArray();

			childrenList.stream().forEach(age -> {
				if (age != null && !age.trim().isEmpty()) {
					if (childType.equals(SON)) {
						if (affectedSonPosition.contains(childCount.get())) {
							enquiryAttribute.add(getEnquiryAttributeExcludeWithTagAndAdditionalVal(
									HealthAttributeCode.AH_SON_AGE.getCode(), age,
									childType + "_" + childCount.incrementAndGet() + "," + SON_AGE_TAG));
						} else {
							enquiryAttribute.add(
									getEnquiryAttributeWithTagAndAdditionalVal(HealthAttributeCode.AH_SON_AGE.getCode(),
											age, childType + "_" + childCount.incrementAndGet() + "," + SON_AGE_TAG));
							childrenDisply.add(childCount.get());
						}
					} else {
						if (affectedDaughterPosition.contains(childCount.get())) {
							enquiryAttribute.add(getEnquiryAttributeExcludeWithTagAndAdditionalVal(
									HealthAttributeCode.AH_DAUGHTER_AGE.getCode(), age,
									childType + "_" + childCount.incrementAndGet() + "," + DAUGHTER_AGE_TAG));
						} else {
							enquiryAttribute.add(getEnquiryAttributeWithTagAndAdditionalVal(
									HealthAttributeCode.AH_DAUGHTER_AGE.getCode(), age,
									childType + "_" + childCount.incrementAndGet() + "," + DAUGHTER_AGE_TAG));
							childrenDisply.add(childCount.get());
						}
					}
					ageNode.add(age);
				}
			});
			if (childType.equals(SON)) {
				addParmsToCookie.put(SON_AGE, ageNode.toString());
				addParmsToCookie.put("sondisplay", childrenDisply.toString());
			} else {
				addParmsToCookie.put(DAUGHTER_AGE, ageNode.toString());
				addParmsToCookie.put("daughterdisplay", childrenDisply.toString());
			}
		} else {
			if (childType.equals(SON)) {
				removeParmsFromCookie.add(SON_AGE);
				removeParmsFromCookie.add("sondisplay");
			} else {
				removeParmsFromCookie.add(DAUGHTER_AGE);
				removeParmsFromCookie.add("daughterdisplay");
			}

		}
	}

	private void addParentAge(String parentAge, String field, ArrayNode enquiryAttribute,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie, String[] remove) {

		if (parentAge != null && !parentAge.trim().isEmpty()) {

			if (FATHER_AGE.equals(field)) {
				if (Arrays.asList(remove).contains(FATHER_ILLNESS)) {
					enquiryAttribute.add(getEnquiryAttributeExclude(HealthAttributeCode.AH_FATHER_AGE.getCode(),
							parentAge, FATHER_AGE_TAG));
					removeParmsFromCookie.add("fatherDisplay");
				} else {
					enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_FATHER_AGE.getCode(),
							parentAge, FATHER_AGE_TAG));
					addParmsToCookie.put("fatherDisplay", parentAge);
				}
				// addParmsToCookie.put(FATHER_AGE, parentAge);
			} else {
				if (Arrays.asList(remove).contains(MOTHER_ILLNESS)) {
					enquiryAttribute.add(getEnquiryAttributeExclude(HealthAttributeCode.AH_MOTHER_AGE.getCode(),
							parentAge, MOTHER_AGE_TAG));
					removeParmsFromCookie.add("motherDisplay");
				} else {
					enquiryAttribute.add(getEnquiryAttributeWithTag(HealthAttributeCode.AH_MOTHER_AGE.getCode(),
							parentAge, MOTHER_AGE_TAG));
					addParmsToCookie.put("motherDisplay", parentAge);
				}
				// addParmsToCookie.put(MOTHER_AGE, parentAge);
			}

		} else {
			if (FATHER_AGE.equals(field)) {
				// removeParmsFromCookie.add( FATHER_AGE);
				removeParmsFromCookie.add("fatherDisplay");
			} else {
				// removeParmsFromCookie.add( MOTHER_AGE);
				removeParmsFromCookie.add("motherDisplay");
			}
		}
	}

	private ObjectNode getEnquiryAttribute(String code, String value) {
		ObjectNode enquiryAttribute = Json.newObject();
		enquiryAttribute.put("categoryAttributeCode", code);
		enquiryAttribute.put("value", value);
		return enquiryAttribute;

	}

	private ObjectNode getEnquiryAttributeWithTag(String code, String value, String tag) {
		ObjectNode enquiryAttribute = getEnquiryAttribute(code, value);
		enquiryAttribute.set("tag", getArrayNodeFromString(tag));
		return enquiryAttribute;
	}

	private ObjectNode getEnquiryAttributeWithTagAndAdditionalVal(String code, String value, String tag) {
		ObjectNode enquiryAttribute = getEnquiryAttributeWithTag(code, value, tag);
		enquiryAttribute.put("additionalValue", "YEARS");
		return enquiryAttribute;
	}

	private ObjectNode getEnquiryAttributeExclude(String code, String value, String tag) {
		ObjectNode enquiryAttribute = getEnquiryAttributeWithTag(code, value, tag);
		enquiryAttribute.put(INPUT_EXCLUDED, Boolean.toString(true));
		return enquiryAttribute;
	}

	private ObjectNode getEnquiryAttributeExcludeWithTagAndAdditionalVal(String code, String value, String tag) {
		ObjectNode enquiryAttribute = getEnquiryAttributeExclude(code, value, tag);
		enquiryAttribute.put("additionalValue", "YEARS");
		return enquiryAttribute;
	}

	public void removeSession(Session session, String key) {
		if (session.containsKey(key)) {
			session.remove(key);
		}
	}

	public void updateHealthEnquiryObjec(ObjectNode dataJsonWithQuery, String selfFilter, String spouseFilter,
			String childrensFilter, String parentsFilter) {
	}

	public ArrayNode getArrayNodeFromString(String tag) {

		ArrayNode tagNodeArr = Json.newArray();
		if (tag != null && !tag.trim().isEmpty()) {
			List<String> tagList = Arrays.asList(tag.split(","));
			tagList.stream().forEach(tagElement -> {
				tagNodeArr.add(tagElement);
			});
		}
		return tagNodeArr;

	}

	public Map<String, String> resetHealthSessionVariables(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie, List<String> notIssuableDiseasesList, Context context) {

		logger.debug("restting session values : " + Json.prettyPrint(response));

		resetSelfAge(response, addParmsToCookie, removeParmsFromCookie);
		resetSelfGender(response, addParmsToCookie, removeParmsFromCookie);
		resetSpuseAge(response, addParmsToCookie, removeParmsFromCookie);
		resetSpouseGender(response, addParmsToCookie, removeParmsFromCookie);
		resetFatherAge(response, addParmsToCookie, removeParmsFromCookie);
		resetMotherAge(response, addParmsToCookie, removeParmsFromCookie);
		resetSonAge(response, addParmsToCookie, removeParmsFromCookie);
		resetDaughterAge(response, addParmsToCookie, removeParmsFromCookie);
		resetHasParent(response, addParmsToCookie, removeParmsFromCookie);
		resetIsParentAndMeHasSameLocation(response, addParmsToCookie, removeParmsFromCookie);
		resetInsuranceType(response, addParmsToCookie, removeParmsFromCookie);
		resetSumAssured(response, addParmsToCookie, removeParmsFromCookie);
		resetSelfPincode(response, addParmsToCookie, removeParmsFromCookie);
		resetParentsPincode(response, addParmsToCookie, removeParmsFromCookie);
		Map<String, List<String>> IllnessMap = new LinkedHashMap<>();

		resetChildrenIllness(response, HealthAttributeCode.AH_SON_ILLNESS.getCode(), SON_ILLNESS, IllnessMap);
		resetChildrenIllness(response, HealthAttributeCode.AH_DAUGHTER_ILLNESS.getCode(), DAUGHTER_ILLNESS, IllnessMap);
		resetElderIllness(response, HealthAttributeCode.AH_FATHER_ILLNESS.getCode(), FATHER_ILLNESS, IllnessMap);
		resetElderIllness(response, HealthAttributeCode.AH_MOTHER_ILLNESS.getCode(), MOTHER_ILLNESS, IllnessMap);
		resetElderIllness(response, HealthAttributeCode.AH_SPOUSE_ILLNESS.getCode(), SPOUSE_ILLNESS, IllnessMap);
		resetElderIllness(response, HealthAttributeCode.AH_SELF_ILLNESS.getCode(), SELF_ILLNESS, IllnessMap);

		collectChildrenCoverd(response, IllnessMap, notIssuableDiseasesList, addParmsToCookie, removeParmsFromCookie,
				SON);
		collectChildrenCoverd(response, IllnessMap, notIssuableDiseasesList, addParmsToCookie, removeParmsFromCookie,
				DAUGHTER);
		collectEldrersCoverd(response, IllnessMap, notIssuableDiseasesList, addParmsToCookie, removeParmsFromCookie);

		if (IllnessMap.isEmpty()) {
			addParmsToCookie.put(DISEASES, Json.newObject().toString());
			addParmsToCookie.put(HAS_ANY_DISEASE, "false");
		} else {
			try {
				//addParmsToCookie.put(DISEASES, Json.toJson(IllnessMap).toString());
				addParmsToCookie.put(HAS_ANY_DISEASE, "true");
			
				if(IllnessMap.containsKey(SELF_ILLNESS)){
					removeParmsFromCookie.add("selfDisplay");
				}
				if(IllnessMap.containsKey(SPOUSE_ILLNESS)){
					removeParmsFromCookie.add("spouseDisplay");
				}
				if(IllnessMap.containsKey(MOTHER_ILLNESS)){
					removeParmsFromCookie.add("motherDisplay");
				}
				if(IllnessMap.containsKey(FATHER_ILLNESS)){
					removeParmsFromCookie.add("fatherDisplay");
				}
				
			} catch (Exception e) {
				throw new ArkaRunTimeException(e);
			}
		}
		if (isSelfAndSpouseExclude(response) && !isExcludedSelfOrSpouseHasNoIllness(response,IllnessMap)) {
			removeParmsFromCookie.add("sondisplay");
			removeParmsFromCookie.add("daughterdisplay");
		
		}
	
		if (!IllnessMap.isEmpty()) {
			Map<String, String> handleHealthCookie = handleHealthCookie(addParmsToCookie, removeParmsFromCookie, context);
			handleHealthCookie.put(DISEASES, Json.toJson(IllnessMap).toString());
			return handleHealthCookie;
		}
		return handleHealthCookie(addParmsToCookie, removeParmsFromCookie, context);

	}

	private void collectChildrenCoverd(JsonNode response, Map<String, List<String>> illnessMap,
			List<String> notIssuableDiseasesList, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie, String childType) {

		ObjectNode children = Json.newObject();

		if (childType != null && childType.equals(SON)) {
			children = getChildAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SON_AGE.getCode(), SON_AGE,
					SON);
			getChildrenCovered(children, SON_AGE, "sondisplay", illnessMap, notIssuableDiseasesList, addParmsToCookie,
					removeParmsFromCookie, SON_ILLNESS);

		} else if (childType != null && childType.equals(DAUGHTER)) {
			children = getChildAttributeValueFromEnquiry(response, HealthAttributeCode.AH_DAUGHTER_AGE.getCode(),
					DAUGHTER_AGE, DAUGHTER);
			getChildrenCovered(children, DAUGHTER_AGE, "daughterdisplay", illnessMap, notIssuableDiseasesList,
					addParmsToCookie, removeParmsFromCookie, DAUGHTER_ILLNESS);
		}

	}

	private void getChildrenCovered(ObjectNode children, String childType, String sessionKey,
			Map<String, List<String>> illnessMap, List<String> notIssuableDiseasesList,
			Map<String, String> addParmsToCookie, List<String> removeParmsFromCookie, String illnessKey) {
		Set<String> nonAffectedChildSet = new LinkedHashSet<>();

		if (children != null && children.hasNonNull(childType)) {
			children.get(childType).forEach(child -> {
				if (child.hasNonNull("childPosition")) {
					if (illnessMap.get(illnessKey + "_" + child.get("childPosition").asText()) != null) {
						List<String> notIssuableDiseasesListTemp = new ArrayList<>();
						notIssuableDiseasesListTemp.addAll(notIssuableDiseasesList);
						notIssuableDiseasesListTemp
								.retainAll(illnessMap.get(illnessKey + "_" + child.get("childPosition").asText()));
						if (notIssuableDiseasesListTemp.size() == 0) {
							nonAffectedChildSet.add(child.get("childPosition").asText());
						}
					} else {
						nonAffectedChildSet.add(child.get("childPosition").asText());
					}
				}
			});
		}
		if (nonAffectedChildSet.size() > 0) {
			addParmsToCookie.put(sessionKey, nonAffectedChildSet.toString());
		} else {
			removeParmsFromCookie.add(sessionKey);
		}

	}

	private void collectEldrersCoverd(JsonNode response, Map<String, List<String>> illnessMap,
			List<String> notIssuableDiseasesList, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		illnessMap.forEach((k, v) -> {
			List<String> notIssuableDiseasesListTemp = new ArrayList<>();
			notIssuableDiseasesListTemp.addAll(notIssuableDiseasesList);
			notIssuableDiseasesListTemp.retainAll(v);

			if (k.equalsIgnoreCase(SELF_ILLNESS)) {
				if (notIssuableDiseasesListTemp.size() == 0) {
					addParmsToCookie.put("selfDisplay", Boolean.toString(true));
				} else {
					removeParmsFromCookie.add("selfDisplay");
				}
			} else if (k.equalsIgnoreCase(SPOUSE_ILLNESS)) {
				if (notIssuableDiseasesListTemp.size() == 0) {
					addParmsToCookie.put("spouseDisplay", Boolean.toString(true));
				} else {
					removeParmsFromCookie.add("spouseDisplay");
				}
			} else if (k.equalsIgnoreCase(FATHER_ILLNESS)) {
				if (notIssuableDiseasesListTemp.size() == 0) {
					addParmsToCookie.put("fatherDisplay", Boolean.toString(true));
				} else {
					removeParmsFromCookie.add("fatherDisplay");
				}

			} else if (k.equalsIgnoreCase(MOTHER_ILLNESS)) {
				if (notIssuableDiseasesListTemp.size() == 0) {
					addParmsToCookie.put("motherDisplay", Boolean.toString(true));
				} else {
					removeParmsFromCookie.add("motherDisplay");
				}
			}

		});

	}

	Filter sonIllnessFilter = filter(where("code").is(HealthAttributeCode.AH_SON_ILLNESS.getCode()));
	Filter daughterIllnessFilter = filter(where("code").is(HealthAttributeCode.AH_DAUGHTER_ILLNESS.getCode()));
	Filter fatherIllnessFilter = filter(where("code").is(HealthAttributeCode.AH_FATHER_ILLNESS.getCode()));
	Filter motherIllnessFilter = filter(where("code").is(HealthAttributeCode.AH_MOTHER_ILLNESS.getCode()));
	Filter spouseIllnessFilter = filter(where("code").is(HealthAttributeCode.AH_SPOUSE_ILLNESS.getCode()));
	Filter selfIllnessFilter = filter(where("code").is(HealthAttributeCode.AH_SELF_ILLNESS.getCode()));

	private void resetElderIllness(JsonNode response, String code, String childType,
			Map<String, List<String>> illnessMap) {
		List<String> illnessIds = new ArrayList<>();
		switch (childType) {
		case SELF_ILLNESS: {
			List<Map<String, Object>> Illness = JsonPath.parse(response.toString()).read("$.items[?]",
					selfIllnessFilter);
			Illness.stream().forEach((k) -> {
				illnessIds.add(k.get("value").toString());
			});
			if (illnessIds.size() > 0) {
				illnessMap.put(SELF_ILLNESS, illnessIds);
			}
			break;
		}
		case SPOUSE_ILLNESS: {
			List<Map<String, Object>> Illness = JsonPath.parse(response.toString()).read("$.items[?]",
					spouseIllnessFilter);
			Illness.stream().forEach((k) -> {
				illnessIds.add(k.get("value").toString());
			});
			if (illnessIds.size() > 0) {
				illnessMap.put(SPOUSE_ILLNESS, illnessIds);
			}
			break;
		}
		case FATHER_ILLNESS: {
			List<Map<String, Object>> Illness = JsonPath.parse(response.toString()).read("$.items[?]",
					fatherIllnessFilter);
			Illness.stream().forEach((k) -> {
				illnessIds.add(k.get("value").toString());
			});
			if (illnessIds.size() > 0) {
				illnessMap.put(FATHER_ILLNESS, illnessIds);
			}
			break;
		}
		case MOTHER_ILLNESS: {
			List<Map<String, Object>> Illness = JsonPath.parse(response.toString()).read("$.items[?]",
					motherIllnessFilter);
			Illness.stream().forEach((k) -> {
				illnessIds.add(k.get("value").toString());
			});

			if (illnessIds.size() > 0) {
				illnessMap.put(MOTHER_ILLNESS, illnessIds);
			}
			break;
		}
		}
	}

	private Map<String, List<String>> resetChildrenIllness(JsonNode response, String attrCode, String childType,
			Map<String, List<String>> childIllnessMap) {

		if (response.hasNonNull("items")) {
			try {
				List<Map<String, Object>> childIllness = new ArrayList<>();
				if (childType.equals(SON_ILLNESS)) {
					childIllness = JsonPath.parse(response.toString()).read("$.items[?]", sonIllnessFilter);
				} else if (childType.equals(DAUGHTER_ILLNESS)) {
					childIllness = JsonPath.parse(response.toString()).read("$.items[?]", daughterIllnessFilter);
				}
				childIllness.stream().forEach((k) -> {
					List<String> tags = JsonPath.read(k, "$.tag[*]");
					tags.stream().forEach(tag -> {

						if (tag.lastIndexOf(childType + "_") != -1) {

							List<String> diseaseLst = new ArrayList<>();
							if (childIllnessMap.containsKey(tag)) {
								diseaseLst = childIllnessMap.get(tag);

							}
							diseaseLst.add(k.get("value").toString());
							childIllnessMap.put(tag, diseaseLst);
						}
					});
				});
			} catch (Exception e) {
				throw new ArkaRunTimeException(e);
			}
		} else {
		}

		return childIllnessMap;
	}

	public void resetSelfAge(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode selfAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SELF_AGE.getCode());

		if (selfAgeJson.hasNonNull("attrVal") && selfAgeJson.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(SELF_AGE, selfAgeJson.get("attrVal").get("value").asText());
			//if(selfAgeJson.get("attrVal").hasNonNull(INPUT_EXCLUDED) && !selfAgeJson.get("attrVal").get(INPUT_EXCLUDED).asBoolean()){
			addParmsToCookie.put("selfDisplay", Boolean.toString(true));
			/*}else{
				removeParmsFromCookie.add("selfDisplay");
			}*/
			addParmsToCookie.put("selfExcluded", selfAgeJson.get("attrVal").get(INPUT_EXCLUDED).asText());
		} else {
			removeParmsFromCookie.add(SELF_AGE);
			removeParmsFromCookie.add("selfDisplay");
			removeParmsFromCookie.add("selfExcluded");
		}
	}

	private void resetHasParent(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode json = getAttributeValueFromEnquiry(response,
				HealthAttributeCode.AH_IS_PARENTS_TO_BE_INCLUDED.getCode());

		if (json.hasNonNull("attrVal") && json.get("attrVal").hasNonNull("value")
				&& json.get("attrVal").get("value").asBoolean()) {
			addParmsToCookie.put(INCLUDE_PARENTS, json.get("attrVal").get("value").asText());
		} else {
			removeParmsFromCookie.add(INCLUDE_PARENTS);
		}
	}

	private void resetIsParentAndMeHasSameLocation(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode json = getAttributeValueFromEnquiry(response,
				HealthAttributeCode.AH_IS_PARENTS_TO_BE_INCLUDED.getCode());

		if (json.hasNonNull("attrVal") && json.get("attrVal").hasNonNull("tag")
				&& json.get("attrVal").get("tag").size() > 0) {
			if (json.get("attrVal").get("tag").toString().contains(PARENTS_ME_HAVE_SAME_LOCATION_TAG)) {
				addParmsToCookie.put(IS_PARENT_AND_ME_HAS_SAME_PINCODE, PARENTS_ME_HAVE_SAME_LOCATION_TAG);
			} else if (json.get("attrVal").get("tag").toString().contains(PARENTS_ME_HAVE_DIFF_LOCATION_TAG)) {
				addParmsToCookie.put(IS_PARENT_AND_ME_HAS_SAME_PINCODE, PARENTS_ME_HAVE_DIFF_LOCATION_TAG);
			} else {
				removeParmsFromCookie.add(IS_PARENT_AND_ME_HAS_SAME_PINCODE);
			}
		} else {
			removeParmsFromCookie.add(IS_PARENT_AND_ME_HAS_SAME_PINCODE);
		}

	}

	private void resetInsuranceType(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {
		JsonNode json = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode());

		if (json.hasNonNull("attrVal") && json.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(INSURANCE_TYPE, json.get("attrVal").get("value").asText());
		} else {
			removeParmsFromCookie.add(INSURANCE_TYPE);
		}
	}

	private void resetSelfPincode(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {
		JsonNode json = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_PINCODE_SELF_OR_FAMILY.getCode());

		if (json.hasNonNull("attrVal") && json.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(PINCODE, json.get("attrVal").get("value").asText());
			JsonNode pincodeJson = healthService.getPincodeByField("_id", json.get("attrVal").get("value").asText());
			if (pincodeJson != null && pincodeJson.hasNonNull("items")) {
				pincodeJson.get("items").forEach(city -> {
					if (city.hasNonNull("_id") && city.hasNonNull("place") && city.hasNonNull("postal_code")
							&& city.hasNonNull("district") && city.hasNonNull("state")) {
						addParmsToCookie.put(PINCODE_DISPLAY, city.get("postal_code").asText());
						addParmsToCookie.put(PINCODE_LABEL,
								city.get("place").asText() + " (" + city.get("district").asText() + ", "
										+ city.get("state").asText() + ", " + city.get("postal_code").asText() + ")");
					}
				});
			}

		} else {
			removeParmsFromCookie.add(PINCODE);
			removeParmsFromCookie.add(PINCODE_LABEL);
			removeParmsFromCookie.add(PINCODE_DISPLAY);
		}

	}

	private void resetParentsPincode(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {
		JsonNode json = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_PINCODE_PARENT.getCode());
		if (json.hasNonNull("attrVal") && json.get("attrVal").hasNonNull("value")) {
			JsonNode pincodeJson = healthService.getPincodeByField("_id", json.get("attrVal").get("value").asText());
			if (pincodeJson != null && pincodeJson.hasNonNull("items")) {
				addParmsToCookie.put(PARENTS_PINCODE, json.get("attrVal").get("value").asText());
				pincodeJson.get("items").forEach(city -> {
					if (city.hasNonNull("_id") && city.hasNonNull("place") && city.hasNonNull("postal_code")
							&& city.hasNonNull("district") && city.hasNonNull("state")) {
						addParmsToCookie.put(PARENTS_PINCODE_DISPLAY, city.get("postal_code").asText());
						addParmsToCookie.put(PARENTS_PINCODE_LABEL,
								city.get("place").asText() + " (" + city.get("district").asText() + ", "
										+ city.get("state").asText() + ", " + city.get("postal_code").asText() + ")");
					}
				});
			}
		} else {
			removeParmsFromCookie.add(PARENTS_PINCODE);
			removeParmsFromCookie.add(PARENTS_PINCODE_LABEL);
			removeParmsFromCookie.add(PARENTS_PINCODE_DISPLAY);
		}

	}

	private void resetSumAssured(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {
		JsonNode json = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SUM_ASSURED.getCode());

		if (json.hasNonNull("attrVal") && json.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(SUM_ASSURED, json.get("attrVal").get("value").asText());
		} else {
			removeParmsFromCookie.add(SUM_ASSURED);
		}

	}

	public void resetSelfGender(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode genderJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SELF_GENDER.getCode());
		if (genderJson.hasNonNull("attrVal") && genderJson.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(GENDER, genderJson.get("attrVal").get("value").asText());
		} else {
			removeParmsFromCookie.add(GENDER);
		}
	}

	public void resetSpuseAge(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode spouseAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SPOUSE_AGE.getCode());
		if (spouseAgeJson.hasNonNull("attrVal") && spouseAgeJson.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(SPOUSE_AGE, spouseAgeJson.get("attrVal").get("value").asText());
			
			//if(spouseAgeJson.get("attrVal").hasNonNull(INPUT_EXCLUDED) && !spouseAgeJson.get("attrVal").get(INPUT_EXCLUDED).asBoolean()){
				addParmsToCookie.put("spouseDisplay", Boolean.toString(true));
			/*}else{
				removeParmsFromCookie.add("spouseDisplay");
			}*/
			
			addParmsToCookie.put("spouseExcluded", spouseAgeJson.get("attrVal").get(INPUT_EXCLUDED).asText());
		} else {
			removeParmsFromCookie.add(SPOUSE_AGE);
			removeParmsFromCookie.add("spouseDisplay");
			removeParmsFromCookie.add("spouseExcluded");

		}
	}

	public void resetSpouseGender(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode spouseGenderJson = getAttributeValueFromEnquiry(response,
				HealthAttributeCode.AH_SPOUSE_GENDER.getCode());
		if (spouseGenderJson.hasNonNull("attrVal") && spouseGenderJson.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(SPOUSE_GENDER, spouseGenderJson.get("attrVal").get("value").asText());
		} else {
			removeParmsFromCookie.add(SPOUSE_GENDER);
		}
	}

	public void resetFatherAge(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {
		JsonNode fatherAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_FATHER_AGE.getCode());
		if (fatherAgeJson.hasNonNull("attrVal") && fatherAgeJson.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(FATHER_AGE, fatherAgeJson.get("attrVal").get("value").asText());
			addParmsToCookie.put("fatherDisplay", Boolean.toString(true));
			addParmsToCookie.put("fatherExcluded", fatherAgeJson.get("attrVal").get(INPUT_EXCLUDED).asText());
		} else {
			removeParmsFromCookie.add(FATHER_AGE);
			removeParmsFromCookie.add("fatherDisplay");
			removeParmsFromCookie.add("fatherExcluded");
		}
	}

	public void resetMotherAge(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode motherAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_MOTHER_AGE.getCode());
		if (motherAgeJson.hasNonNull("attrVal") && motherAgeJson.get("attrVal").hasNonNull("value")) {
			addParmsToCookie.put(MOTHER_AGE, motherAgeJson.get("attrVal").get("value").asText());
			addParmsToCookie.put("motherDisplay", Boolean.toString(true));
			addParmsToCookie.put("motherExcluded", motherAgeJson.get("attrVal").get(INPUT_EXCLUDED).asText());
		} else {
			removeParmsFromCookie.add(MOTHER_AGE);
			removeParmsFromCookie.add("motherDisplay");
			removeParmsFromCookie.add("motherExcluded");
		}
	}

	public void resetSonAge(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode childAgeJson = getChildAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SON_AGE.getCode(),
				SON_AGE, SON);

		/* If self and spouse excluded then exclude chidren */
		if (childAgeJson.hasNonNull(SON_AGE)) {
			addParmsToCookie.put(SON_AGE, childAgeJson.toString());
		} else {
			removeParmsFromCookie.add(SON_AGE);
		}
	}

	private boolean isSelfAndSpouseExclude(JsonNode response) {
		JsonNode selfAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SELF_AGE.getCode());
		JsonNode spouseAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SPOUSE_AGE.getCode());

		if(selfAgeJson !=null && selfAgeJson.hasNonNull("attrVal") && selfAgeJson.get("attrVal").hasNonNull(INPUT_EXCLUDED)
				&& selfAgeJson.get("attrVal").get(INPUT_EXCLUDED).asBoolean()){
			
			if(spouseAgeJson!=null && spouseAgeJson.hasNonNull("attrVal") && spouseAgeJson.get("attrVal").hasNonNull("value") && !"".equals(spouseAgeJson.get("attrVal").get("value").asText())){
				return ( spouseAgeJson.hasNonNull("attrVal")
						&& spouseAgeJson.get("attrVal").hasNonNull(INPUT_EXCLUDED)
						&& spouseAgeJson.get("attrVal").get(INPUT_EXCLUDED).asBoolean());
			}else{
				return true;
			}
			
		}else{
			return false;
		}
		
		

	}

	private boolean isExcludedSelfOrSpouseHasNoIllness(JsonNode response, Map<String, List<String>> illnessMap) {
		JsonNode selfAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SELF_AGE.getCode());
		JsonNode spouseAgeJson = getAttributeValueFromEnquiry(response, HealthAttributeCode.AH_SPOUSE_AGE.getCode());

		if(selfAgeJson !=null && selfAgeJson.hasNonNull("attrVal") && selfAgeJson.get("attrVal").hasNonNull("value")
				&& selfAgeJson.get("attrVal").get(INPUT_EXCLUDED).asBoolean()){
			
			if(spouseAgeJson!=null && spouseAgeJson.hasNonNull("attrVal") && 
					spouseAgeJson.get("attrVal").hasNonNull("value") &&
					!"".equals(spouseAgeJson.get("attrVal").get("value").asText())){
				
				if(spouseAgeJson.hasNonNull("attrVal")&& spouseAgeJson.get("attrVal").hasNonNull(INPUT_EXCLUDED)
						&& spouseAgeJson.get("attrVal").get(INPUT_EXCLUDED).asBoolean() ){
					
					return (!illnessMap.containsKey(SPOUSE_ILLNESS) || !illnessMap.containsKey(SELF_ILLNESS));
					
				}else{
					return true; 
				}
				
			}else{
				if(illnessMap.containsKey(SELF_ILLNESS)){
					return false;
				}else{
					return true;
				}
				
			}
			
		}else{
			if(illnessMap.containsKey(SELF_ILLNESS)){
				return false;
			}else{
				return true;
			}
		}
		
		
		
		

	}

	public void resetDaughterAge(JsonNode response, Map<String, String> addParmsToCookie,
			List<String> removeParmsFromCookie) {

		JsonNode childAgeJson = getChildAttributeValueFromEnquiry(response,
				HealthAttributeCode.AH_DAUGHTER_AGE.getCode(), DAUGHTER_AGE, DAUGHTER);
		/* If self and spouse excluded then exclude chidren */
		if (childAgeJson.hasNonNull(DAUGHTER_AGE)) {
			addParmsToCookie.put(DAUGHTER_AGE, childAgeJson.toString());
		} else {
			removeParmsFromCookie.add(DAUGHTER_AGE);
		}
	}

	public JsonNode getAttributeValueFromEnquiry(JsonNode response, String attrCode) {
		ObjectNode value = Json.newObject();
		if (response.hasNonNull("items")) {
			response.get("items").forEach(field -> {
				if (field.hasNonNull("code") && field.hasNonNull("value")
						&& field.get("code").asText().equals(attrCode)) {
					value.set("attrVal", field);
				}
			});
		} else {
		}
		return value;
	}

	public ObjectNode getChildAttributeValueFromEnquiry(JsonNode response, String attrCode, String sessionKey,
			String childType) {

		ObjectNode value = Json.newObject();
		ArrayNode childrenArray = value.putArray(sessionKey);
		if (response.hasNonNull("items")) {
			response.get("items").forEach(field -> {
				if (field.hasNonNull("code") && field.hasNonNull("value") && field.get("code").asText().equals(attrCode)
						&& field.hasNonNull("tag")) {
					ObjectNode child = Json.newObject();
					child.put("age", field.get("value").asText());
					childPosition(field.get("tag"), childType, child);
					child.put("excluded", field.get(INPUT_EXCLUDED).asText());
					childrenArray.add(child);
				}
			});
		} else {
		}

		return value;
	}

	public void childPosition(JsonNode tag, String childType, ObjectNode child) {

		tag.forEach(item -> {
			if (item.asText().lastIndexOf(childType + "_") != -1) {
				child.put("childPosition", item.asText().split(childType + "_")[1]);
			}
		});
	}

	public Map<String, String> getHealthCookie(Request request) {
		
		try {
			return CookieUtils.getCookies(request);
		} catch (Exception e) {
			logger.info("{}", keyValue(SystemEvent.SYSTEM_EXCEPTION.name(), e.getLocalizedMessage()));
			throw new ArkaRunTimeException(e);
		}
	}

	public void removeChildrenIfHasDisease(ObjectNode enquiryParams, List<String> removeParmsFromCookie) {
		if (enquiryParams != null && enquiryParams.hasNonNull("enquiryAttributes")) {
			ObjectNode meAndSpouseExcluded = Json.newObject();
			AtomicBoolean hasWifeAge = new AtomicBoolean(false);
			
			enquiryParams.get("enquiryAttributes").forEach(enq -> {

				if (enq != null && enq.hasNonNull("categoryAttributeCode") 
						&& ( enq.get("categoryAttributeCode").asText().equals(HealthAttributeCode.AH_SELF_AGE.getCode()) ||
								enq.get("categoryAttributeCode").asText().equals(HealthAttributeCode.AH_SPOUSE_AGE.getCode())) ) {
					
					if ( enq.get("categoryAttributeCode").asText().equals(HealthAttributeCode.AH_SELF_AGE.getCode()) && enq.hasNonNull(INPUT_EXCLUDED)
							&& enq.get(INPUT_EXCLUDED).asBoolean() ) {
								meAndSpouseExcluded.put(SELF_AGE, true);
					}
					if (enq.get("categoryAttributeCode").asText().equals(HealthAttributeCode.AH_SPOUSE_AGE.getCode())) {
						
						if(enq.hasNonNull(INPUT_EXCLUDED)&& enq.get(INPUT_EXCLUDED).asBoolean() ){
							meAndSpouseExcluded.put(SPOUSE_AGE, true);
						}
						hasWifeAge.set(true);
					}
				}

				if (isMeAndSpouseExcluded(meAndSpouseExcluded)) {
					return;
				}
			});
			//this will used to remove children, if spouse is not available and self has disease
			if(!hasWifeAge.get()){
				meAndSpouseExcluded.put(SPOUSE_AGE, true);
			}

			if (isMeAndSpouseExcluded(meAndSpouseExcluded)) {
				removeChildrensFromEnquiry(enquiryParams);
				removeParmsFromCookie.add(SON_AGE);
				removeParmsFromCookie.add(DAUGHTER_AGE);
			}
		}
	}

	private boolean isMeAndSpouseExcluded(ObjectNode meAndSpouseExcluded) {
		if (meAndSpouseExcluded != null && meAndSpouseExcluded.hasNonNull(SELF_AGE)
				&& meAndSpouseExcluded.get(SELF_AGE).asBoolean() && meAndSpouseExcluded.hasNonNull(SPOUSE_AGE)
				&& meAndSpouseExcluded.get(SPOUSE_AGE).asBoolean()) {
			return true;
		}
		return false;
	}

	/* If self and spouse excluded then exclude chidren */
	private void removeChildrensFromEnquiry(ObjectNode enquiryParams) {
		if (enquiryParams != null && enquiryParams.hasNonNull("enquiryAttributes")) {
			ArrayNode enquiryAttribute = Json.newArray();
			enquiryParams.get("enquiryAttributes").forEach(enq -> {
				if (enq != null && enq.hasNonNull("categoryAttributeCode")
						&& (enq.get("categoryAttributeCode").asText().equals(HealthAttributeCode.AH_SON_AGE.getCode())
								|| enq.get("categoryAttributeCode").asText()
										.equals(HealthAttributeCode.AH_DAUGHTER_AGE.getCode()))) {

					ObjectNode enqParms = (ObjectNode) enq;
					enqParms.put(INPUT_EXCLUDED, Boolean.toString(true));
					enquiryAttribute.add(enqParms);
				} else {
					enquiryAttribute.add(enq);
				}

			});
			enquiryParams.set("enquiryAttributes", enquiryAttribute);
		}
	}
}
