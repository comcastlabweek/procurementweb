/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.HeaderParams;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.procurement.validators.Validators;
import com.arka.common.services.CmsServ;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.SessionServ;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.CookieUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.Validation;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.arka.consumerportal.procurementweb.constants.ProcurementErrors;
import com.arka.consumerportal.procurementweb.models.EnquiryRequest;
import com.arka.consumerportal.procurementweb.models.EnquiryRequest.EnquiryRequestBuilder;
import com.arka.consumerportal.procurementweb.services.MispService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Http.Session;
import play.mvc.Result;
import play.mvc.WebSocket;

/**
 * @author Jakpren
 *
 */
public class MotorInsuranceQuoteController extends Controller {

	private static final String MISP_BOOKING_ID = "misp_booking_id";
	private static final String REFERRER_CODE = "referrer_code";
	private static final String VEHICLE_TYPE = "vehicleType";

	private static final int LAST_7_CHARACTER = 7;

	private static final Logger LOG = LoggerFactory.getLogger(MotorInsuranceQuoteController.class);

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	@Inject
	private PropUtils propUtils;

	@Inject
	ActorSystem actorSystem;

	@Inject
	CmsServ cmsServ;

	@Inject
	SessionServ sessionServ;

	@Inject
	private MispService mispService;

	@Inject
	Materializer materializer;

	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	MandatoryParams mandatoryParams;

	public CompletionStage<Result> get() {
		Map<String, String[]> queryParameter = request().queryString();
		final String mispBookingId = getMispBookingId(queryParameter);
		final String referrerCode = getReferrerCode(queryParameter);
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(queryParameter);

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, String> cookieMap = new HashMap<>();
		queryStringMap.putAll(mandatQP);
		String vehicleState = queryStringMap.get("vehicle_state") != null ? queryStringMap.get("vehicle_state").get(0)
				: null;

		String vehicleType = ("rollovercar".equals(vehicleState) || "newcar".equals(vehicleState)) ? "car"
				: (("rolloverbike".equals(vehicleState) || "newbike".equals(vehicleState))) ? "bike" : null;

		if (vehicleType != null) {
			mandatQP.put(VEHICLE_TYPE, Arrays.asList(vehicleType));
		}
		return procurementServ.getVehicleCodeForMakeModelFuelTypeVariant(queryStringMap, mandatHP)
				.thenApplyAsync(vehicleCodeJson -> {
					if (isValidVehileCodeResp().test(vehicleCodeJson)) {
						JsonNode _idVehicleCodeJson = vehicleCodeJson.get("vehicle_code")
								.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0);
						String vehicleCode = _idVehicleCodeJson.get("code").asText();
						EnquiryRequestBuilder enquiryBuilder = new EnquiryRequestBuilder();
						enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_VEHCODE.getCode(), vehicleCode);
						enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_VEHSTATE.getCode(), vehicleState);

						// ADD ALL ADDITIONAL COVER
						enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AI_ALL_ADDITIONAL_COVER.getCode(), "true");
						
						// fromQueryString
						attributeValueAssigner(CarAttributeCode.AC_COVER_POLICY_EXPIRED_BEFORE_90_DAYS.getCode(),
								"expiredbefore90days", enquiryBuilder, queryStringMap);
						attributeValueAssigner(CarAttributeCode.IS_OWNERSHIP_CHANGED.getCode(),
								CarAttributeCode.IS_OWNERSHIP_CHANGED.getCode(), enquiryBuilder, queryStringMap);
						attributeValueAssigner(CarAttributeCode.IS_PREVIOUS_POLICY_NCB_ADDON_SELECTED.getCode(),
								CarAttributeCode.IS_PREVIOUS_POLICY_NCB_ADDON_SELECTED.getCode(), enquiryBuilder, queryStringMap);
						attributeValueAssigner(CarAttributeCode.AC_CLAIMED.getCode(), "claimed", enquiryBuilder,
								queryStringMap);
						attributeValueAssigner(CarAttributeCode.AC_REGNO.getCode(), "regNo[]", enquiryBuilder,
								queryStringMap);

						if (Validators.attributeValidater("rtoArray[]", queryStringMap)
								|| Validators.attributeValidater("regNo[]", queryStringMap)) {
							Map<String, List<String>> rtoQueryStr = new HashMap<>();
							String code = queryStringMap.containsKey("rtoArray[]")
									? queryStringMap.get("rtoArray[]").get(0)
									: (queryStringMap.containsKey("regNo[]")
											? (queryStringMap.get("regNo[]").get(0) + "-"
													+ queryStringMap.get("regNo[]").get(1)).toUpperCase()
											: "");
							rtoQueryStr.put("code", Arrays.asList(code));
							rtoQueryStr.putAll(mandatQP);
							System.out.println("rtoQueryStr :"+rtoQueryStr);
							JsonNode rtoCode = procurementServ.getRtoDetailsForCarInsurance(rtoQueryStr, mandatHP)
									.toCompletableFuture().join();
							System.out.println("rtoCode :"+rtoCode);
							JsonNode rtoResp = anyCollectionQueryResponseValidator.anyCollectionQueryGetData(rtoCode);
							if (rtoResp.size() > 0 && JsonUtils.isValidIndex(rtoResp, 0)
									&& JsonUtils.isValidField(rtoResp.get(0), "_id")) {
								enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_RTOCODE.getCode(),
										rtoResp.get(0).get("code").asText());
							}

						}
						if (Validators.attributeValidater("previouspolicyexpired", queryStringMap)) {
							if (queryStringMap.get("previouspolicyexpired").get(0).equals("not_expired")) {
								enquiryBuilder.addEnquiryAttribute(
										CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(),
										LocalDate.now().toString());
							}
							if (queryStringMap.get("previouspolicyexpired").get(0).equals("expired")) {
								if (queryStringMap.get("expiredbefore90days").get(0).equals("false")) {
									enquiryBuilder.addEnquiryAttribute(
											CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(),
											LocalDate.now().minusDays(1).toString());
								}
								if (queryStringMap.get("expiredbefore90days").get(0).equals("true")) {
									enquiryBuilder.addEnquiryAttribute(
											CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(),
											LocalDate.now().minusDays(91).toString());
								}
							}
						}
						if (!Validators.attributeValidater("claimed", queryStringMap)
								&& !Validators.attributeValidater("previouspolicyexpired", queryStringMap)) {
							enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_POLICY_START_DATE.getCode(),
									LocalDate.now().toString());
						}
						attributeValueAssigner(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode(), "previouspolicyexpired",
								enquiryBuilder, queryStringMap);
						if (Validators.attributeValidater("ncb", queryStringMap)) {
							attributeValueAssigner(CarAttributeCode.AC_NCB_PERCENT.getCode(), "ncb", enquiryBuilder,
									queryStringMap);
						} else {
							if (Validators.attributeValidater("claimed", queryStringMap)
									&& queryStringMap.get("claimed").get(0).equals("false"))
								enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_NCB_PERCENT.getCode(), "20");
						}
						if (Validators.attributeValidater("ncgLgpFitValue", queryStringMap)) {
							attributeValueAssigner(CarAttributeCode.AC_CNG_OR_LPG_EXTERNALLY_FITTED_VALUE.getCode(),
									"ncgLgpFitValue", enquiryBuilder, queryStringMap);
							enquiryBuilder.addEnquiryAttribute(
									CarAttributeCode.AC_IS_CNG_OR_LPG_EXTERNALLY_FITTED.getCode(), "true");
						} else if (Validation.queryValidator("fuel_type").test(queryStringMap)
								&& Validation.queryValueGetter(queryStringMap, "fuel_type").equals("lpg/cng")) {
							enquiryBuilder.addEnquiryAttribute(
									CarAttributeCode.AC_IS_CNG_OR_LPG_EXTERNALLY_FITTED.getCode(), "false");
						}

						if (queryStringMap.containsKey("year") && queryStringMap.get("year").size() > 0) {
							int year = Integer.parseInt(queryStringMap.get("year").get(0));
							if (LocalDate.now().getYear() < year) {
								enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
										LocalDate.of(year, 1, 1).toString());
							} else if (LocalDate.now().getYear() == year) {
								/*
								 * enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.
								 * getCode(), LocalDate.of(year, LocalDate.now().getMonthValue(),
								 * 1).toString());
								 */
								enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
										LocalDate.now().toString());
								enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode(),
										LocalDate.now().toString());
							} else {
								if (Validators.attributeValidater("vehicle_state", queryStringMap)
										&& queryStringMap.get("vehicle_state").get(0).equalsIgnoreCase("newcar")
										|| queryStringMap.get("vehicle_state").get(0).equalsIgnoreCase("newbike")) {
									enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
											LocalDate.now().toString());
									if(LocalDate.now().getYear() > year){
										enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode(),
												LocalDate.of(year, Month.DECEMBER, 1).toString());
									}else{
										enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode(),
												LocalDate.now().toString());
									}
								}else{
									enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
											LocalDate.of(year, LocalDate.now().getMonthValue(), 1).toString());
									enquiryBuilder.addEnquiryAttribute(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode(),
											LocalDate.of(year, LocalDate.now().getMonthValue(), 1).toString());
								}
							}
						}
						setCategoryId(mandatQP, mandatHP, enquiryBuilder);
						enquiryBuilder.setVisitorId(CookieUtils.getCookieValue(Context.current().request(), SessionTrackingParams.VISITOR_ID.getValue()));
						enquiryBuilder.setUserId(session().getOrDefault(SessionTrackingParams.USER_ID.getValue(), ""));
						EnquiryRequest enquiryRequest = enquiryBuilder.build();
						return JsonUtils.toJson(enquiryRequest);
					}
					ObjectNode errorJson = JsonUtils.newJsonObject();
					errorJson.put("error", "Not a valid vehicle code");
					errorJson.set("vehicle_code_response", vehicleCodeJson);
					return errorJson;
				}, httpExecutionContext.current()).thenComposeAsync(enquiryRequestJson -> {
					if (enquiryRequestJson != null && enquiryRequestJson.has("error")) {
						return CompletableFuture.completedFuture(enquiryRequestJson);
					}
					((ObjectNode) enquiryRequestJson).put("mispBookingId", mispBookingId);
					((ObjectNode) enquiryRequestJson).put("referrerCode", referrerCode);

					return procurementServ.createEnquiry(mandatQP, mandatHP, enquiryRequestJson);
				}, httpExecutionContext.current()).thenApplyAsync(enquiryResponseJson -> {
					ObjectNode respJson = JsonUtils.newJsonObject();
					if (enquiryResponseJson != null && enquiryResponseJson.has(JsonUtils.JSON_ARRAY_ITEMS_KEY)
							&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).has(0)
							&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).has("enquiryId")) {
						String enquiryId = enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0)
								.get("enquiryId").asText();
						respJson.put("enquiry_id", enquiryId);
						if (mandatQP.get(VEHICLE_TYPE).get(0) != null && mandatQP.get(VEHICLE_TYPE).get(0).equals("car")) {
							   cookieMap.put(SessionTrackingParams.CAR_ENQUIRY_ID.getValue(), enquiryId);
						} else {
							 cookieMap.put(SessionTrackingParams.BIKE_ENQUIRY_ID.getValue(), enquiryId);
						}							
						try {
							CookieUtils.setCookieValue(cookieMap, response(), request());
						} catch (Exception e) {
							e.printStackTrace();
						}
						session().put("home", "motor");
					}
					return ok(respJson);
				}, httpExecutionContext.current());
	}

	private void attributeValueAssigner(String attributeField, String field, EnquiryRequestBuilder enquiryBuilder,
			Map<String, List<String>> queryStringMap) {
		if (Validators.attributeValidater(field, queryStringMap)) {
			if (field.equals("regNo[]")) {
				enquiryBuilder.addEnquiryAttribute(attributeField, queryStringMap.get(field).toString().toUpperCase());
			} else {
				enquiryBuilder.addEnquiryAttribute(attributeField, queryStringMap.get(field).get(0));
			}
		}
	}

	private void setCategoryId(Map<String, List<String>> mandatQP, Map<String, List<String>> mandatHP,
			EnquiryRequestBuilder enquiryBuilder) {
		Map<String, List<String>> queryMap = new LinkedHashMap<>();
		queryMap.putAll(mandatQP);
		JsonNode categoryId = null;

		if (mandatQP.get(VEHICLE_TYPE).get(0) != null && mandatQP.get(VEHICLE_TYPE).get(0).equals("car")) {
			categoryId = procurementServ.getCategoryDetailsForCar(queryMap, mandatHP).toCompletableFuture().join();
		} else {
			categoryId = procurementServ.getCategoryDetailsForBike(queryMap, mandatHP).toCompletableFuture().join();
		}

		if (anyCollectionQueryResponseValidator.isValidAnyCollectionQueryResp().test(categoryId)) {
			JsonNode catIdJson = anyCollectionQueryResponseValidator.anyCollectionQueryGetDataWithIndex(categoryId, 0);
			enquiryBuilder.setCategoryId(catIdJson.get("_id").asText());
			
		}

	}

	public CompletionStage<Result> motorPolicy(String enquiryId) {
		if (StringUtils.isNumeric(enquiryId)) {
			return CompletableFuture.supplyAsync(() -> {
				return notFound();
			});
		}
		ObjectNode userEnteredData = Json.newObject();
		userEnteredData.put("enquiry_id", enquiryId);
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (Validation.queryValidator("proposalId").test(queryStringMap)) {
			userEnteredData.put("proposal_id", queryStringMap.get("proposalId").get(0));
		}
		if (queryStringMap.containsKey("enquiry_id") && queryStringMap.get("enquiry_id") != null
				&& queryStringMap.containsKey("proposal_id") && queryStringMap.get("proposal_id") != null) {
			userEnteredData.put("proposal_id", queryStringMap.get("proposal_id").get(0));
			EnquiryRequest enquiryRequest = enquiryAttributeBuilder(queryStringMap.get("enquiry_id").get(0), mandatQP,
					mandatHP);
			JsonNode enquiryResponseJson = JsonUtils.newJsonObject();
			try {
				enquiryResponseJson = procurementServ
						.createEnquiry(mandatQP, mandatHP, JsonUtils.toJson(enquiryRequest)).toCompletableFuture()
						.get();
				if (enquiryResponseJson != null && enquiryResponseJson.has(JsonUtils.JSON_ARRAY_ITEMS_KEY)
						&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).has(0)
						&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).has("enquiryId")) {
					enquiryId = enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).get("enquiryId")
							.asText();
				}
			} catch (Exception e) {
				return CompletableFuture
						.completedFuture(internalServerError(JsonUtils.getJsonResponseInternalServerError(e)));

			}
		}

		ObjectNode enquiryAttributes = (ObjectNode) getEnquiryAttributes(enquiryId, mandatQP, mandatHP);
		activeEntryTypeOfCall(enquiryAttributes);
		userEnteredData.setAll(enquiryAttributes);
		JsonNode errMsg = carInsuranceErrors();
		userEnteredData.setAll((ObjectNode) getRestrictionForCarInsurance(enquiryId, mandatQP, mandatHP));
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));

		Map<String, String> mapToPage = new HashMap<>();
		if(queryStringMap.containsKey("e")) {
			queryStringMap.get("e").forEach(ele->{
				mapToPage.put("error",ele);
			});
		}
		ObjectNode newResp = Json.newObject();

		return procurementServ.getEnquiry(enquiryId, queryMap, mandatHP).thenApplyAsync(enquiryResponseJson -> {

			/* get misp booking */
			if (JsonUtils.isValidField(enquiryResponseJson, "mispBookingId")) {
				ObjectNode mispBooking = (ObjectNode) mispService
						.getMispBooking(enquiryResponseJson.get("mispBookingId").asText()).toCompletableFuture().join();
				if (mispBooking.get("engineNumber") != null) {
					mispBooking.put("engineNumberLast7Character",
							subString(mispBooking.get("engineNumber").asText(), LAST_7_CHARACTER));
				}
				if (mispBooking.get("chassisNumber") != null) {
					mispBooking.put("chassisNumberLast7Character",
							subString(mispBooking.get("chassisNumber").asText(), LAST_7_CHARACTER));
				}
				newResp.set("mispBooking", mispBooking);
			}

			// get vehicleType
			JsonNode categoryJson = null;
			try {
				categoryJson = procurementServ
						.getCategoryById(enquiryResponseJson.get("categoryId").asText(), queryMap, mandatHP)
						.toCompletableFuture().get();

				if (categoryJson.get("error") == null) {
					newResp.set(VEHICLE_TYPE, Json.toJson(categoryJson.get("code").asText().toLowerCase()));
				}

			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (JsonUtils.isValidField(enquiryResponseJson, "categoryId")) {
				ObjectNode glossaryValues = getGlossaryValues(enquiryResponseJson.get("categoryId").asText(), mandatQP,
						mandatHP);
				userEnteredData.set("glossaryValues", glossaryValues);
			}
			if (queryStringMap.size() > 0 && queryStringMap.containsKey("code")
					&& queryStringMap.get("code").size() > 0) {
				userEnteredData.put("code", queryStringMap.get("code").get(0));
				ObjectNode productResp = JsonUtils.newJsonObject();
				enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(product -> {
					if (product.get("productCode").asText().equals(queryStringMap.get("code").get(0))) {
						JsonNode respJson = JsonUtils.newJsonObject().setAll((ObjectNode) JsonUtils.newJsonObject()
								.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, JsonUtils.newJsonArray().add(product)));
						productResp.setAll((ObjectNode) respJson);
					}
				});
				settingYearFromRegistration(userEnteredData);
				return ok(com.arka.consumerportal.procurementweb.views.html.motor_policy.render(productResp, propUtils,
						request(), session(), userEnteredData, errMsg, mapToPage, JsonUtils.toJson(newResp)))
								.withHeader("Cache-Control", "no-store");
			} else {
				settingYearFromRegistration(userEnteredData);
				return ok(com.arka.consumerportal.procurementweb.views.html.motor_policy.render(enquiryResponseJson,
						propUtils, request(), session(), userEnteredData, errMsg, mapToPage, JsonUtils.toJson(newResp)))
								.withHeader("Cache-Control", "no-store");
			}
		}, httpExecutionContext.current());
	}

	private void settingYearFromRegistration(ObjectNode userEnteredData) {
		if (JsonUtils.isValidField(userEnteredData, "items")) {
			userEnteredData.get("items").spliterator().forEachRemaining(obj -> {
				if (JsonUtils.isValidField(obj, "code") && JsonUtils.isValidField(obj, "value")) {
					String code = obj.get(VendorQuoteConstants.CODE.value()).asText();
					if (code.equalsIgnoreCase(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode())) {
						LocalDate endofCentury = LocalDate.parse(obj.get("value").asText());
						LocalDate now = LocalDate.now();
						Period diff = Period.between(endofCentury, now);
						userEnteredData.put("differenceInYears", diff.getYears());
					}
				}
			});
		}
	}

	// updateMotorPolicy
	public CompletionStage<Result> updateMotorPolicy(String enquiryId) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		JsonNode dataJson = request().body().asJson();
		ObjectNode dataJsonWithQuery = JsonUtils.newJsonObject();
		ArrayNode dataJsonArrayQuery = JsonUtils.newJsonArray();

		// misp fields
		if (JsonUtils.isValidField(dataJson, "is_misp") && ("true").equals(dataJson.get("is_misp").asText())) {
			dataJsonWithQuery.put(CarAttributeCode.AC_IS_MISP.getCode(),
					dataJson.get("is_misp").asBoolean());
			dataJsonWithQuery.put(CarAttributeCode.AC_IS_OTP_VERIFIED.getCode(), true);
			if (JsonUtils.isValidField(dataJson, "engine_number")) {
				dataJsonWithQuery.put(CarAttributeCode.AC_ENGINE_NUMBER.getCode(),
						dataJson.get("engine_number").asText());
			}
			if (JsonUtils.isValidField(dataJson, "chassis_number")) {
				dataJsonWithQuery.put(CarAttributeCode.AC_CHASSIS_NUMBER.getCode(),
						dataJson.get("chassis_number").asText());
			}
			if (JsonUtils.isValidField(dataJson, "email_id")) {
				dataJsonWithQuery.put(CarAttributeCode.AI_EMAILID.getCode(), dataJson.get("email_id").asText());
			}
			if (JsonUtils.isValidField(dataJson, "mobile_number")) {
				dataJsonWithQuery.put(CarAttributeCode.AI_MOBILE_NUMBER.getCode(),
						dataJson.get("mobile_number").asText());
			}
		}

		if (JsonUtils.isValidField(dataJson, "car_manufacture_date")) {
			dataJsonWithQuery.put(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode(),
					LocalDate.parse(dataJson.get("car_manufacture_date").asText(),DateTimeFormatter.ofPattern("d/M/yyyy")).toString());
			dataJsonWithQuery.put(CarAttributeCode.AC_NEW_CARMODEL_VALUE_FILLED.getCode(),"false");
		}

		if (JsonUtils.isValidField(dataJson, "car_policyStart_month")) {
			int date = dataJson.get("car_policyStart_date").asInt();
			int month = dataJson.get("car_policyStart_month").asInt();
			dataJsonWithQuery.put(CarAttributeCode.AC_POLICY_START_DATE.getCode(),
					LocalDate.of(LocalDate.now().getYear(), month, date).toString());
			dataJsonWithQuery.put(CarAttributeCode.AC_NEW_CARMODEL_VALUE_FILLED.getCode(),"false");
		}
		
		if (JsonUtils.isValidField(dataJson, "reg_date")) {
			dataJsonWithQuery.put(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
					LocalDate.parse(dataJson.get("reg_date").asText(),DateTimeFormatter.ofPattern("d/M/yyyy")).toString());
			dataJsonWithQuery.put(CarAttributeCode.AC_NEW_CARMODEL_VALUE_FILLED.getCode(),"false");
		}

		if (dataJsonWithQuery.size() == 0 && dataJson.size() > 0) {
			dataJsonWithQuery = (ObjectNode) dataJson;
		}
		AtomicBoolean newCarModelValueFilled = new AtomicBoolean();
		dataJsonWithQuery.fields().forEachRemaining(e -> {
			if (e.getKey().equals("AC-DATE_OF_REGISTRATION")
					|| e.getKey().equals("AC-PREVIOUS_POLICY_EXPIRED_ON")
					|| e.getKey().equals("AC-COVER-POLICY_EXPIRED_BEFORE_90_DAYS")
					|| e.getKey().equals("AC-CLAIMED")
					|| e.getKey().equals("AC-NCB_PERCENT")) {
				newCarModelValueFilled.set(true);
			}
		});
		if(newCarModelValueFilled.get()){
			if(!dataJsonWithQuery.has(CarAttributeCode.AC_NEW_CARMODEL_VALUE_FILLED.getCode())){
				dataJsonWithQuery.put(CarAttributeCode.AC_NEW_CARMODEL_VALUE_FILLED.getCode(),"false");
			}
		}
		
		dataJsonWithQuery.fields().forEachRemaining(e -> {
			if (e.getKey() != "undefined") {
				ObjectNode convertUpdateFormat = JsonUtils.newJsonObject();
				convertUpdateFormat.put("categoryAttributeCode", e.getKey());
				convertUpdateFormat.set("value", e.getValue());
				dataJsonArrayQuery.add(convertUpdateFormat);
			}
		});
		
		ObjectNode patchJson = Json.newObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.ADD.value());
		patchJson.put(UpdateNames.PATH.value(), "/enquiryAttributes");
		patchJson.set(UpdateNames.VALUE.value(), dataJsonArrayQuery);
		
		return procurementServ.updateEnquiry(enquiryId, mandatQP, mandatHP, patchJson).thenApplyAsync(resp -> {
			return ok();
		});
	}

	public WebSocket socket() {
		Request request =Context.current().request();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		Map<String, List<String>> headerMap = new HashMap<>();
		if (session().containsKey(SessionTrackingParams.SESSION_ID.getValue())
				&& session().get(SessionTrackingParams.SESSION_ID.getValue()) != null) {
			headerMap.put(HeaderParams.SESSION_ID.getValue(),
					Arrays.asList(session().get(SessionTrackingParams.SESSION_ID.getValue())));
		}
		if(CookieUtils.isContainKey(request, SessionTrackingParams.VISITOR_ID.getValue())
				&& CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue()) != null) {
			headerMap.put(HeaderParams.VISITOR_ID.getValue(),
					Arrays.asList(CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue())));
		}
		return WebSocket.Json.accept(reqHeader -> {
			return Flow.fromFunction(planReqJson -> {
				Map<String, List<String>> queryString = new HashMap<>();
				queryString.put("source", Arrays.asList("correlationid"));
				queryString.putAll(mandatQP);
				return sessionServ.generateUniqueId(queryString, mandatHP, null).thenApplyAsync(sessionResp -> {
					headerMap.put(HeaderParams.CORRELATION_ID.getValue(),
							Arrays.asList(sessionResp.get("correlationId").asText()));
					headerMap.putAll(mandatHP);
					return procurementServ.fetchPlan(mandatQP, headerMap, planReqJson).toCompletableFuture().join();

				}).toCompletableFuture().get();
			});
		});
	}

	private JsonNode carInsuranceErrors() {
		ObjectNode errorJson = JsonUtils.newJsonObject();
		Arrays.asList(ProcurementErrors.values()).forEach(error -> {
			errorJson.put(error.value().toString(), propUtils.get(error.value().toString()));
		});
		return errorJson;
	}

	private ObjectNode getGlossaryValues(String categoryId, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		ObjectNode glossaryValues = JsonUtils.newJsonObject();
		try {
			Map<String, List<String>> mandatQPDup = mandatQP;
			if (mandatQPDup.containsKey("motor_type")) {
				mandatQPDup.remove("motor_type");
			}
			glossaryValues.setAll((ObjectNode) procurementServ.getAttrDescription(categoryId, mandatQPDup, mandatHP)
					.toCompletableFuture().join());
		} catch (ArkaRunTimeException e) {
			throw new ArkaRunTimeException(e);
		}
		LOG.debug("json for glosaary values --------------------" + glossaryValues);
		return glossaryValues;
	}

	private JsonNode getEnquiryAttributes(String enquiryId, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		ObjectNode userEnteredData = JsonUtils.newJsonObject();
		JsonNode getEnquiryAttributesWithCode = JsonUtils.newJsonObject();

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.CODE.value()));

		getEnquiryAttributesWithCode = procurementServ.getEnquiryAttributes(enquiryId, queryMap, mandatHP)
				.toCompletableFuture().join();
		if (JsonUtils.isValidField(getEnquiryAttributesWithCode, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			userEnteredData.setAll((ObjectNode) getEnquiryAttributesWithCode);
			getEnquiryAttributesWithCode.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(e -> {
				if (JsonUtils.isValidField(e, "code") && e.get("code").asText().equals("AC-VEHCODE")) {
					String vehicleId = e.get("value").asText();
					try {
						Map<String, List<String>> queryString = new LinkedHashMap<>();
						// queryString.put("_id", Arrays.asList(vehicleId));
						queryString.put("code", Arrays.asList(vehicleId));
						queryString.putAll(mandatQP);
						JsonNode makeModelJson = procurementServ.getVehicleDetailsForCarInsurance(queryString, mandatHP)
								.toCompletableFuture().join();
						JsonNode makeModel = anyCollectionQueryResponseValidator
								.anyCollectionQueryGetData(makeModelJson);
						if (makeModel.size() > 0) {
							userEnteredData.set("makeModel", makeModel.get(0));
						}
					} catch (ArkaRunTimeException f) {
						throw new ArkaRunTimeException(f);
					}
				}
				if (JsonUtils.isValidField(e, "code")
						&& e.get("code").asText().equals(CarAttributeCode.AC_RTOCODE.getCode())) {
					String rtoCode = e.get("value").asText();
					try {
						Map<String, List<String>> queryStr = new LinkedHashMap<>();
						queryStr.put("code", Arrays.asList(rtoCode));
						queryStr.putAll(mandatQP);
						JsonNode rtoJson = procurementServ.getRtoDetailsForCarInsurance(queryStr, mandatHP)
								.toCompletableFuture().join();
						JsonNode rtoModel = anyCollectionQueryResponseValidator.anyCollectionQueryGetData(rtoJson);
						if (rtoModel.size() > 0) {
							userEnteredData.set("rtoModel", rtoModel.get(0));
						}
					} catch (ArkaRunTimeException g) {
						throw new ArkaRunTimeException(g);
					}
				}
			});
		}

		return userEnteredData;
	}

	private JsonNode getRestrictionForCarInsurance(String enquiryId, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {

		ObjectNode userEnteredData = JsonUtils.newJsonObject();
		JsonNode getLimitForPassengerCover = procurementServ.getRestrictionForNcbAttributes(mandatQP, mandatHP)
				.toCompletableFuture().join();
		if (anyCollectionQueryResponseValidator.isValidAnyCollectionQueryResp().test(getLimitForPassengerCover)) {
			anyCollectionQueryResponseValidator.anyCollectionQueryGetData(getLimitForPassengerCover).forEach(e -> {
				userEnteredData.set(e.get("code").asText(),
						JsonUtils.newJsonObject().set("constraint_value", e.get("constraint_value")));
			});

		}
		return userEnteredData;
	}

	public CompletionStage<Result> createEnquiryWithExistingEnquiry(String enquiryId) {
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Request request = request();
		Session session = session();
		session.put("enquiry_id", enquiryId);
		ObjectNode userEnteredData = Json.newObject();
		userEnteredData.put("enquiry_id", enquiryId);

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		EnquiryRequest enquiryRequest = enquiryAttributeBuilder(enquiryId, mandatQP, mandatHP);
		return procurementServ.createEnquiry(mandatQP, mandatHP, JsonUtils.toJson(enquiryRequest))
				.thenApplyAsync(enquiryResponseJson -> {
					ObjectNode respJson = JsonUtils.newJsonObject();
					if (enquiryResponseJson != null && enquiryResponseJson.has(JsonUtils.JSON_ARRAY_ITEMS_KEY)
							&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).has(0)
							&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).has("enquiryId")) {
						respJson.put("enquiry_id", enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0)
								.get("enquiryId").asText());
					}
					return ok(respJson);
				}, httpExecutionContext.current()).thenCompose(resp -> {
					if (JsonUtils.isValidField(JsonUtils.toJson(resp), "categoryId")) {
						ObjectNode glossaryValues = getGlossaryValues(JsonUtils.toJson(resp).get("categoryId").asText(),
								mandatQP, mandatHP);
						userEnteredData.set("glossaryValues", glossaryValues);
					}
					userEnteredData.setAll((ObjectNode) getEnquiryAttributes(enquiryId, mandatQP, mandatHP));
					JsonNode errMsg = carInsuranceErrors();
					userEnteredData.setAll((ObjectNode) getRestrictionForCarInsurance(enquiryId, mandatQP, mandatHP));
					Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
					queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));

					Map<String, String> mapToPage = new HashMap<>();
					ObjectNode newResp = Json.newObject();

					return procurementServ.getEnquiry(enquiryId, queryMap, mandatHP)
							.thenApplyAsync(enquiryResponseJson -> {
								if (queryStringMap.size() > 0 && queryStringMap.containsKey("code")
										&& queryStringMap.get("code").size() > 0) {
									ObjectNode productResp = JsonUtils.newJsonObject();
									enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(product -> {
										if (product.get("productCode").asText()
												.equals(queryStringMap.get("code").get(0))) {
											JsonNode respJson = JsonUtils.newJsonObject()
													.setAll((ObjectNode) JsonUtils.newJsonObject().set(
															JsonUtils.JSON_ARRAY_ITEMS_KEY,
															JsonUtils.newJsonArray().add(product)));
											productResp.setAll((ObjectNode) respJson);
										}
									});
									return ok(com.arka.consumerportal.procurementweb.views.html.motor_policy.render(
											productResp, propUtils, request, session, userEnteredData, errMsg,
											mapToPage, JsonUtils.toJson(newResp)));
								} else {
									return ok(com.arka.consumerportal.procurementweb.views.html.motor_policy.render(
											enquiryResponseJson, propUtils, request, session, userEnteredData, errMsg,
											mapToPage, JsonUtils.toJson(newResp)));
								}

							}, httpExecutionContext.current());
				});
	}

	public CompletionStage<Result> renewCarInsuranceWithPreviousProduct(String enquiryId, String productCode) {
		Request request = request();
		Session session = session();
		session.put("enquiry_id", enquiryId);
		ObjectNode userEnteredData = Json.newObject();
		userEnteredData.put("enquiry_id", enquiryId);

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		JsonNode errMsg = carInsuranceErrors();
		EnquiryRequest enquiryRequest = enquiryAttributeBuilder(enquiryId, mandatQP, mandatHP);

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));

		Map<String, String> mapToPage = new HashMap<>();
		ObjectNode newResp = Json.newObject();

		return procurementServ.createEnquiry(mandatQP, mandatHP, JsonUtils.toJson(enquiryRequest))
				.thenApplyAsync(enquiryResponseJson -> {
					ObjectNode respJson = JsonUtils.newJsonObject();
					if (JsonUtils.isValidField(enquiryResponseJson, "categoryId")) {
						ObjectNode glossaryValues = getGlossaryValues(enquiryResponseJson.get("categoryId").asText(),
								mandatQP, mandatHP);
						userEnteredData.set("glossaryValues", glossaryValues);
					}
					if (enquiryResponseJson != null && enquiryResponseJson.has(JsonUtils.JSON_ARRAY_ITEMS_KEY)
							&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).has(0)
							&& enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).has("enquiryId")) {
						enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(product -> {
							if (product.get("productCode").asText().equals(productCode)) {
								respJson.setAll((ObjectNode) JsonUtils.newJsonObject()
										.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, JsonUtils.newJsonArray().add(product)));
							}
						});
						return ok(com.arka.consumerportal.procurementweb.views.html.motor_policy.render(respJson,
								propUtils, request, session, userEnteredData, errMsg, mapToPage,
								JsonUtils.toJson(newResp)));
					}
					return ok(respJson);
				}, httpExecutionContext.current());
	}

	private EnquiryRequest enquiryAttributeBuilder(String enquiryId, Map<String, List<String>> mandatQP,
			Map<String, List<String>> mandatHP) {
		EnquiryRequestBuilder enquiryBuilder = new EnquiryRequestBuilder();
		JsonNode enquiryAttributes = getEnquiryAttributes(enquiryId, mandatQP, mandatHP);

		if (JsonUtils.isValidField(enquiryAttributes, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			enquiryAttributes.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(attr -> {
				if (JsonUtils.isValidField(attr, "code") && JsonUtils.isValidField(attr, "value")) {
					enquiryBuilder.addEnquiryAttribute(attr.get("code").asText(), attr.get("value").asText());
				}
			});
		}
		setCategoryId(mandatQP, mandatHP, enquiryBuilder);
		enquiryBuilder.setVisitorId(CookieUtils.getCookieValue(Context.current().request(), SessionTrackingParams.VISITOR_ID.getValue()));
		enquiryBuilder.setUserId("1");
		EnquiryRequest enquiryRequest = enquiryBuilder.build();
		return enquiryRequest;
	}

	public CompletionStage<Result> renewCarInsurance(String enquiryId) {
		Session session = session();
		session.put("enquiry_id", enquiryId);
		ObjectNode userEnteredData = Json.newObject();
		userEnteredData.put("enquiry_id", enquiryId);

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		EnquiryRequest enquiryRequest = enquiryAttributeBuilder(enquiryId, mandatQP, mandatHP);
		return procurementServ.createEnquiry(mandatQP, mandatHP, JsonUtils.toJson(enquiryRequest))
				.thenApplyAsync(enquiryResponseJson -> {
					if (JsonUtils.isValidField(enquiryResponseJson, "enquiryId")) {
						ObjectNode enqJson = Json.newObject();
						enqJson.put("enquiryId", enquiryResponseJson.get("enquiryId").asText());
						enqJson.put("type", "car");
						return ok(enqJson);
					}
					return ok();
				}, httpExecutionContext.current());
	}

	private void activeEntryTypeOfCall(ObjectNode enquiryAttributes) {
		ObjectNode classDefiner = JsonUtils.newJsonObject();
		if (JsonUtils.isValidField(enquiryAttributes, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			enquiryAttributes.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(e -> {
				if (JsonUtils.isValidField(e, "code") && JsonUtils.isValidField(e, "value")) {
					classDefiner.put(e.get("code").asText(), e.get("value").asText());
				}
			});
		}
	}

	private Predicate<JsonNode> isValidVehileCodeResp() {
		return (vehicleCodeJson) -> JsonUtils.isValidField(vehicleCodeJson, "vehicle_code")
				&& JsonUtils.isValidField(vehicleCodeJson.get("vehicle_code"), JsonUtils.JSON_ARRAY_ITEMS_KEY)
				&& vehicleCodeJson.get("vehicle_code").get(JsonUtils.JSON_ARRAY_ITEMS_KEY).has(0);
	}

	private String getMispBookingId(Map<String, String[]> queryParameter) {
		if (ArrayUtils.isNotEmpty(queryParameter.get(MISP_BOOKING_ID))) {
			return queryParameter.get(MISP_BOOKING_ID)[0];
		}
		return null;
	}

	private String getReferrerCode(Map<String, String[]> queryParameter) {
		if (ArrayUtils.isNotEmpty(queryParameter.get(REFERRER_CODE))) {
			return queryParameter.get(REFERRER_CODE)[0];
		}
		return null;
	}

	private String subString(String value, int length) {
		return (value.length() > length) ? value.substring(value.length() - length) : value;
	}

	public CompletionStage<Result> emailQuotes() {

		ObjectNode emailJsonReq = (ObjectNode) request().body().asJson();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		if (!JsonUtils.isValidField(emailJsonReq, "enquiryId") || !JsonUtils.isValidField(emailJsonReq, "emailId")) {
			ObjectNode errorJson = Json.newObject();
			errorJson.put("error", "Invalid input");
			return CompletableFuture.completedFuture(badRequest(errorJson));
		}

		// TODO: Ganesh: Please check and ensure that the whole chain (web->serv->web)
		// is of asynchronous callback
		return procurementServ.emailQuotes(mandatQP, mandatHP, emailJsonReq).thenApplyAsync(enquiryResponseJson -> {
			ObjectNode successJson = Json.newObject();
			successJson.put("status", "success");
			return ok(successJson);

		}, httpExecutionContext.current());

	}

	public CompletionStage<Result> updateMispBookingStatus(String bookingId)
			throws InterruptedException, ExecutionException {
		JsonNode booking = request().body().asJson();
		return mispService.updateBookingStatus(bookingId, booking).thenApplyAsync(enquiryResponseJson -> {
			return noContent();
		}, httpExecutionContext.current());
	}
}
