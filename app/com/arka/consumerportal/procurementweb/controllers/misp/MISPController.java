package com.arka.consumerportal.procurementweb.controllers.misp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.services.SessionServ;
import com.arka.common.services.UserServ;
import com.arka.common.user.constants.UserAuthField;
import com.arka.common.utils.CookieUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.ws.service.WsServiceUtils;
import com.arka.consumerportal.procurementweb.constants.ProcurementErrors;
import com.arka.consumerportal.procurementweb.controllers.MotorController;
import com.arka.consumerportal.procurementweb.models.misp.services.SigninService;
import com.arka.consumerportal.procurementweb.util.ErrorUtil;
import com.arka.consumerportal.procurementweb.util.ProcurementUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Http.Session;
import play.mvc.Result;
import play.mvc.Results;

public class MISPController extends MotorController {

	private static final Logger logger = LoggerFactory.getLogger(MotorController.class);

	@Inject
	FormFactory formfactory;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	FormFactory formFactory;

	@Inject
	private PropUtils propUtils;

	@Inject
	private SigninService signinService;

	@Inject
	private UserServ userServ;

	@Inject
	SessionServ sessionServ;

	@Inject
	private WsServiceUtils wsUtils;

	@Inject
	HttpExecutionContext httpExecutionContext;
	
	@Inject
	ProcurementUtils procurementUtils;
	

	public CompletionStage<Result> homePage() {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		String userId = session(SessionTrackingParams.USER_ID.getValue());

		if (userId == null) {
			return toMispLandingPage();
		}
		JsonNode mispUser = null;
		HashMap<String, List<String>> queryMap = new HashMap<>();
		try {
			queryMap.put("user_id", Arrays.asList(userId));
			mispUser = userServ.getAllMispUsers(queryMap, new HashMap<>()).toCompletableFuture().get();
			if (mispUser.get("list") == null || mispUser.get("list").size() < 1
					|| mispUser.get("list").get(0).get("misp") == null) {
				return toMispLandingPage();
			}	
			mispUser = mispUser.get("list").get(0).get("misp");

			System.out.println("MispUser: " + Json.prettyPrint(mispUser));

		} catch (InterruptedException | ExecutionException e) {
			// TODO throw error
			e.printStackTrace();
			toCompletionStage(internalServerError());
		}

		if (mispUser.get("id") != null) {
			session("mispId", mispUser.get("id").asText());
		}

		ObjectNode mispInfo = Json.newObject();
		Map<String, List<String>> queryStringMap = new HashMap<>();
		if (mispUser.get("vehicleCategory").asInt() == 1) {
			mispInfo.put("vehicleType", "car");
			queryStringMap.put("vehicle_type", Arrays.asList("CAR"));
		} else {
			mispInfo.put("vehicleType", "bike");
			queryStringMap.put("vehicle_type", Arrays.asList("BIKE"));
		}
		ArrayNode rtoResp = JsonUtils.newJsonArray();
		Map<String, List<String>> queryString = new HashMap<>();
		queryString.put("user_id", Arrays.asList(session("uid")));

		if (null != mispUser && JsonUtils.isValidField(mispUser, "referrer")) {
			mispInfo.put("referrerCode", mispUser.get("referrer").get("referrerCode").asText());

			if (JsonUtils.isValidField(mispUser, "logoId")) {

				Map<String, List<String>> queryparameter = new HashMap<>();
				Map<String, List<String>> headerparameter = new HashMap<>();
				headerparameter.put("Authorization", Arrays.asList(
						"Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MzQ2NjIzNTI5NzgyNzc4NjM1IiwiYnVzaW5lc3NQcm9maWxlIjp7IjU2MzMwNjI2OTI4OTg2MDYyNDgiOjE1fSwidXNlclR5cGUiOiJCVVNJTkVTUyIsImV4cCI6MTIzNDU2OTM5NTkzNTQzMH0.Oi418CCWkMArBXza8Bap4dcwjM70VCfRoP5W6sNRXdA"));
				queryparameter.put("url_type", Arrays.asList("PERMANENT"));
				// System.out.println(propUtils.get("arka.policy.service.host"));

				try {
					// TODO: DANGER DANGER
					/*JsonNode jsonNode = wsUtils
							.doGet("http://52.220.5.209:11007/v1/content/files/" + mispUser.get("logoId").asText(),
									queryparameter, headerparameter)
							.toCompletableFuture().get();
					if (null != jsonNode && JsonUtils.isValidField(jsonNode, "fileURL")) {
						mispInfo.put("logoUrl", jsonNode.get("fileURL").asText());
					}*/

					if (System.getenv("CONTENT_SERV_PROTOCOL") != null 
							&& System.getenv("CONTENT_SERV_IP") != null 
							&& System.getenv("CONTENT_SERV_PORT") != null) {
						String contentServUrl = System.getenv("CONTENT_SERV_PROTOCOL").toLowerCase() + "://" 
								+ System.getenv("CONTENT_SERV_IP") + ":" + System.getenv("CONTENT_SERV_PORT");
						JsonNode jsonNode = wsUtils.doGet(contentServUrl + "/v1/content/files/" + mispUser.get("logoId").asText(),
										queryparameter, headerparameter).toCompletableFuture().get();
						if (null != jsonNode && JsonUtils.isValidField(jsonNode, "fileURL")) {
							mispInfo.put("logoUrl", jsonNode.get("fileURL").asText());
						}
					}

				} catch (InterruptedException | ExecutionException e) {
					// toCompletionStage(internalServerError());
					logger.error("Unable to fetch logo ...");
					// Ignore silently. Not important
				}
			}
		}

		ObjectNode motorMakeNode = Json.newObject();
		ObjectNode motorItems = Json.newObject();
		motorMakeNode.set("popular_motor_make",
				motorItems.set("items", Json.toJson(Arrays.asList(mispUser.get("oemBrand").asText().toUpperCase()))));
		String enquiryId = "";
		Map<String,String> enquiryAttributeMap = new HashMap<>();
		if(CookieUtils.isContainKey(request(), SessionTrackingParams.CAR_ENQUIRY_ID.getValue())){
			enquiryId = CookieUtils.getCookieValue(request(), SessionTrackingParams.CAR_ENQUIRY_ID.getValue());
			procurementUtils.getEnquiryAttribute(mandatQP, mandatHP, enquiryId,enquiryAttributeMap);
		}
		return this.landingPage(queryStringMap, rtoResp).thenApplyAsync(makeModelJson -> {
			return ok(com.arka.consumerportal.procurementweb.views.html.misp.misp_home.render(motorMakeNode, propUtils,
					ErrorUtil.getProcurementErrors(), enquiryAttributeMap, rtoResp, mispInfo));
		}, httpExecutionContext.current());
	}

	private CompletionStage<Result> toMispLandingPage() {
		return toCompletionStage(Results.redirect(
				com.arka.consumerportal.procurementweb.controllers.misp.routes.MISPController.mispLandingPage()));
	}

	public CompletionStage<Result> mispLandingPage() {
		Request request =Context.current().request();
		String visitorId = CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue());
		sessionClearBuild();
		session(SessionTrackingParams.VISITOR_ID.getValue(), visitorId);
		Session session = session();
		return CompletableFuture.completedFuture(
				ok(com.arka.consumerportal.procurementweb.views.html.misp.loginPage.render(propUtils, session)));
	}

	public CompletionStage<Result> mispLogin() {

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		ArrayNode errMsg = Json.newArray();
		Arrays.asList(ProcurementErrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		System.out.println(formfactory.form().bindFromRequest().data());
		if (!validate()) {
			return CompletableFuture.completedFuture(Results.redirect("/motor/misp"));
		} else {

			Map<String, String> formData = formfactory.form().bindFromRequest().data();
			formData.put("source", "login");
			formData.put("mobOrMail", formData.get("userName"));
			System.out.println(Json.toJson(formData));

			return signinService.signIn(mandatQP, mandatHP, JsonUtils.toJson(formData))
					.thenComposeAsync(responseJson -> {
						System.out.println("user session : " + Json.prettyPrint(responseJson));
						if (hasUserSession(responseJson)) {
							return setUserSession(responseJson, formData.get("userName")).thenApplyAsync((obj) -> {
								return Results.redirect("/motor/misp-home");
							}, httpExecutionContext.current());
						} else if (isUserActive(responseJson)) {
							return CompletableFuture.supplyAsync(() -> Results.redirect("/motor/misp"),
									httpExecutionContext.current());
						} else {
							return CompletableFuture.supplyAsync(() -> Results.redirect("/motor/misp"),
									httpExecutionContext.current());
						}
					}, httpExecutionContext.current());
		}
	}

	private boolean hasUserSession(JsonNode responseJson) {
		return JsonUtils.isValidField(responseJson, "sessionId") && JsonUtils.isValidField(responseJson, "status")
				&& responseJson.get("status").asText().equalsIgnoreCase("Active");
	}

	private boolean validate() {
		DynamicForm requestData = formfactory.form().bindFromRequest();

		System.out.println("userName validate : " + requestData);

		String userName = (requestData.get("userName") != null && requestData.get("userName").trim().length() > 0)
				? requestData.get("userName") : "";
		String password = (requestData.get("password") != null && requestData.get("password").trim().length() > 0)
				? requestData.get("password") : "";
		if (userName != null && password != null && userName.trim().length() > 0 && password.length() > 0) {
			// if (Validation.isEmailId(userName) ||
			// Validation.isMobileNumber(userName)) {
			// return true;
			// } else {
			// return false;
			// }
			return true;
		} else {
			return false;
		}
	}

	private CompletionStage<Void> setUserSession(JsonNode responseJson, String userName) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (hasUserSession(responseJson)) {
			session("uid", responseJson.get("id").asText());

			session("sid", responseJson.get("sessionId").asText());
		}
		if (JsonUtils.isValidField(responseJson, SessionTrackingParams.AUTH_TOKEN.getValue())) {
			session(SessionTrackingParams.AUTH_TOKEN.getValue(),
					responseJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText());
			mandatHP.put(SessionTrackingParams.AUTH_TOKEN.getValue(),
					Arrays.asList(responseJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText()));
		}
		if (JsonUtils.isValidField(responseJson, UserAuthField.REFRESH_TOKEN.value())) {
			session(UserAuthField.REFRESH_TOKEN.value(),
					responseJson.get(UserAuthField.REFRESH_TOKEN.value()).asText());
			mandatHP.put(UserAuthField.REFRESH_TOKEN.value(),
					Arrays.asList(responseJson.get(UserAuthField.REFRESH_TOKEN.value()).asText()));

		}
		mandatQP.put("source", Arrays.asList("getUser"));
		mandatQP.put("userId", Arrays.asList(responseJson.get("id").asText()));
		return sessionServ.getUser(mandatQP, mandatHP, Json.newObject()).thenAcceptAsync(userJson -> {
			if (JsonUtils.isValidField(userJson, "loginNames")) {
				userJson.get("loginNames").elements().forEachRemaining(acc -> {
					if (userName.equals(acc.get("loginName").asText())) {
						session("userType", acc.get("type").asText());
						session("userName", acc.get("loginName").asText());

					}
				});
			}
		}, httpExecutionContext.current());

	}

	private boolean isUserActive(JsonNode respJson) {
		return JsonUtils.isValidField(respJson, "status")
				&& "Inactive".equalsIgnoreCase(respJson.get("status").asText());
	}

	private void sessionClearBuild() {
		session().clear();
		session("cookieDisabled", "false");
	}

	private CompletionStage<Result> toCompletionStage(Result result) {
		return CompletableFuture.supplyAsync(() -> result, httpExecutionContext.current());
	}

}
