package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.services.ProcurementServ;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.procurementweb.constants.Checkouterrors;
import com.arka.consumerportal.procurementweb.services.ProposalService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

public class TravelCheckoutController extends Controller {

	@Inject
	ProposalService proposalService;

	@Inject
	private PropUtils proputils;

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	FormFactory formFactory;

	@Inject
	private HttpExecutionContext executionContext;
	
	@Inject
	PropUtils propUtils;

	@Inject
	MandatoryParams mandatoryParams;

	public CompletionStage<Result> travelBuyPlan(String productId, String quotekey, String enquiryId) {
		ObjectNode detailsJson = Json.newObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		detailsJson.put("enquiryId", enquiryId);
		detailsJson.put("productId", productId);
		detailsJson.put("quoteKey", quotekey);
		JsonNode enquiryJson = null;
		try {
			enquiryJson = procurementServ.getEnquiry(enquiryId, mandatQP, mandatHP).toCompletableFuture().get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(enquiryJson != null) {
			detailsJson.put("categoryId", enquiryJson.get("categoryId").asText());
		}
		
		return proposalService.buyPlan(mandatQP, mandatHP, detailsJson).thenApplyAsync(resp -> {
		 String proposalId = resp.get("proposalId").asText();
			return redirect(com.arka.consumerportal.procurementweb.controllers.routes.TravelCheckoutController
					.travelCheckOutPage(proposalId).url());
		}, executionContext.current());
	}

	public CompletionStage<Result> travelCheckOutPage(String proposalId) {
		ArrayNode errMsg = Json.newArray();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Arrays.asList(Checkouterrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			return ok(com.arka.consumerportal.procurementweb.views.html.travel.travelcheckoutpage.render(resp,
					proputils, errMsg));
		}, executionContext.current());
	}

	public CompletionStage<Result> saveTravelProposal(String proposalId) {
		ObjectNode jsonInput = (ObjectNode) request().body().asJson();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		saveTravelAttributesInSession(jsonInput);
		ObjectNode patchJson = Json.newObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.REPLACE.value());
		patchJson.put(UpdateNames.PATH.value(), "/proposalAttributes");
		patchJson.set(UpdateNames.VALUE.value(), jsonInput);

		return proposalService.updateProposal(proposalId, mandatQP, mandatHP, patchJson).thenApplyAsync(resp -> {
			return ok(resp);
		}, executionContext.current());
	}

	private void saveTravelAttributesInSession(ObjectNode sessionJson) {
		sessionJson.fieldNames().forEachRemaining(action -> {
			session(action.toString(), sessionJson.get(action.toString()).asText());
		});
	}

	public CompletionStage<Result> travelCheckOutSummary(String proposalId) {
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			return ok(com.arka.consumerportal.procurementweb.views.html.travel.travelcheckoutsummary.render(resp,
					proputils));
		}, executionContext.current());
	}

	public CompletionStage<Result> getStatesForTravel() throws InterruptedException, ExecutionException {
		Map<String, List<String>> queryString = new HashMap<>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryString.put("cname", Arrays.asList("state"));
		queryString.put("pfield", Arrays.asList("state_name"));
		// queryString.put("nor", Arrays.asList("0"));
		queryString.put(CriteriaUtils.NO_OF_RECORDS.toString(), Arrays.asList(propUtils.get("nor.value")));
		queryString.putAll(mandatQP);
		JsonNode respJson = procurementServ.getStates(queryString, mandatHP).toCompletableFuture().get();
		return CompletableFuture.completedFuture(ok(respJson));
	}

	public Result updateUserIdInTravelProposal() {
		DynamicForm dynamicForm = formFactory.form().bindFromRequest();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		String propaslId = dynamicForm.get("proposal_id");
		String userId = dynamicForm.get("user_id");
		if (propaslId != null && !propaslId.trim().isEmpty() && userId != null && !userId.trim().isEmpty()) {
			ObjectNode detailsJson = Json.newObject();
			detailsJson.put("op", "replace");
			detailsJson.put("path", "/userId");
			detailsJson.put("value", userId);

			proposalService.updateProposal(propaslId, mandatQP, mandatHP, detailsJson);
		}
		return ok("ok");
	}

	public CompletionStage<Result> vendorPortal(String proposalId) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		mandatQP.putAll(queryMap);
		return proposalService.getProposalById(proposalId, mandatQP, mandatHP, null).thenApplyAsync(resp -> {
			return ok(com.arka.consumerportal.procurementweb.views.html.portalpage.render(JsonUtils.toJson(resp),
					proputils));
		}, executionContext.current());
	}
}
