/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.utils.PropUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class LoginPageController extends Controller {
	@Inject
	PropUtils propUtils;
	
		public CompletionStage<Result> LoginPage() {	
		ObjectNode responseJson = Json.newObject();
		//session("sid", responseJson.get("sessionId").asText());
		return CompletableFuture
				.completedFuture(ok(com.arka.consumerportal.procurementweb.views.html.loginPage
						.render(responseJson, propUtils)));
	}
	
	}
