package com.arka.consumerportal.procurementweb.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.elasticsearch.common.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.HeaderParams;
import com.arka.common.constants.RedisDB;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.insurance.constants.InsuranceAttributeCode;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PaymentFrequency;
import com.arka.common.policy.constants.PaymentMode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.DateConstants;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.procurement.constants.Source;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.services.PolicyService;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.ProfileService;
import com.arka.common.services.SessionServ;
import com.arka.common.services.UserServ;
import com.arka.common.services.VendorIntegrationServ;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.CookieUtils;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.EncodingUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.arka.consumerportal.procurementweb.constants.CheckoutSessionKeys;
import com.arka.consumerportal.procurementweb.constants.Checkouterrors;
import com.arka.consumerportal.procurementweb.constants.ProcurementErrors;
import com.arka.consumerportal.procurementweb.services.MispService;
import com.arka.consumerportal.procurementweb.services.ProposalService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.google.inject.Inject;

import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.Session;
import play.mvc.Result;

public class MotorCheckoutController extends Controller {

	private static final int LAST_7_CHARACTER = 7;
	private static final String VEHICLE_TYPE = "vehicleType";

	private static final Logger logger = LoggerFactory.getLogger(MotorCheckoutController.class);

	@Inject
	ProposalService proposalService;

	@Inject
	FormFactory formFactory;

	@Inject
	private HttpExecutionContext executionContext;

	@Inject
	private PropUtils proputils;

	@Inject
	private ProfileService profileService;

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	private VendorIntegrationServ vendorServ;

	@Inject
	private MispService mispService;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	private SessionServ sessionServ;

	@Inject
	private PropUtils propUtils;

	@Inject
	private UserServ userserv;

	@Inject
	private PolicyService policyService;

	@Inject
	private AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	@Inject
	private CacheService cache;

	public CompletionStage<Result> buyplan() {
		ObjectNode requestJson = (ObjectNode) request().body().asJson();
		if (JsonUtils.isValidField(requestJson, "enquiryId") && JsonUtils.isValidField(requestJson, "quoteKey")
				&& JsonUtils.isValidField(requestJson, "productId")) {
			Map<String, List<String>> oldProposalMap = CriteriaUtils.queryStringArrayToList(request().queryString());
			Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
			ObjectNode detailsJson = JsonUtils.newJsonObject();
			detailsJson.put("enquiryId", requestJson.get("enquiryId").asText());
			detailsJson.put("productId", requestJson.get("productId").asText());
			detailsJson.put("quoteKey", requestJson.get("quoteKey").asText());
			JsonNode enquiryJson = procurementServ.getEnquiry(requestJson.get("enquiryId").asText(), mandatQP, mandatHP)
					.toCompletableFuture().join();
			if (enquiryJson != null && JsonUtils.isValidField(enquiryJson, "categoryId")) {
				detailsJson.put("categoryId", enquiryJson.get("categoryId").asText());
			}
			return proposalService.buyPlan(mandatQP, mandatHP, detailsJson).thenApplyAsync(resp -> {
				logger.debug("this is the response---------------" + resp);
				String previousPremiumAmoumt = null;
				if (JsonUtils.isValidField(requestJson, "premiumAmount")) {
					previousPremiumAmoumt = requestJson.get("premiumAmount").asText();
				}
				String proposalId = "";
				if (JsonUtils.isValidField(resp, "proposalId")) {
					proposalId = resp.get("proposalId").asText();
				}
				if (session().containsKey("KEY")) {
					session().remove("KEY");
				}
				if (oldProposalMap.containsKey("oldproposal_id") && oldProposalMap.get("oldproposal_id") != null
						&& oldProposalMap.get("oldproposal_id").get(0) != null) {
					session().put("SOURCE", "CAR_RENEW");
					session().put("OLDPROPOSALID", oldProposalMap.get("oldproposal_id").get(0).toString());
				} else {
					if (session().containsKey("SOURCE")) {
						session().remove("SOURCE");
						if (session().containsKey("OLDPROPOSALID")) {
							session().remove("OLDPROPOSALID");
						}
					}
				}
				if (StringUtils.isNotEmpty(proposalId)) {
					if (StringUtils.isNotEmpty(previousPremiumAmoumt)) {
						ArrayNode savingattributes = JsonUtils.newJsonArray();
						ObjectNode updateJson = JsonUtils.newJsonObject();
						updateJson.put("categoryAttributeCode", "AI-GENERATED_PREMIUM_AMOUNT");
						updateJson.put("value", previousPremiumAmoumt);
						ObjectNode patchJson = Json.newObject();
						patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.REPLACE.value());
						patchJson.put(UpdateNames.PATH.value(), "/proposalAttributes");
						patchJson.set(UpdateNames.VALUE.value(), savingattributes.add(updateJson));
						JsonNode updateresponse = proposalService
								.updateProposal(proposalId, mandatQP, mandatHP, patchJson).toCompletableFuture().join();
						return ok(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
								.checkOutPage(proposalId).url());
					} else {
						return ok(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
								.checkOutPage(proposalId).url());
					}
				} else {
					return ok(com.arka.consumerportal.procurementweb.controllers.routes.MotorInsuranceQuoteController
							.motorPolicy(requestJson.get("enquiryId").asText()).url() + "?e=server.error");
				}
			}, executionContext.current());
		} else {
			return CompletableFuture.completedFuture(internalServerError());
		}
	}

	public CompletionStage<Result> checkOutPage(String proposalId) {

		ArrayNode errMsg = Json.newArray();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		Map<String, String> mapToPage = new HashMap<>();
		Arrays.asList(Checkouterrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));

		/* logic for users logined user */
		if (session().containsKey("uid") && session().get("uid") != null) {
			session().put("LOGGEDINUSER", "true");
		}

		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			if (resp.get("progress").asText().equalsIgnoreCase("PURCHASED")) {
				return ok(com.arka.consumerportal.procurementweb.views.html.purchased_proposal_error.render(propUtils,
						resp.get("enquiryId").asText(), resp.get("categoryCode").asText()));
			}

			ObjectNode newResp = (ObjectNode) resp;

			String vehicleType = getVehicleType(mandatHP, queryMap, newResp);
			/* get misp booking */
			if (JsonUtils.isValidField(newResp, "mispBookingId")) {
				ObjectNode mispBooking = (ObjectNode) mispService.getMispBooking(newResp.get("mispBookingId").asText())
						.toCompletableFuture().join();
				newResp.set("mispBooking", mispBooking);
			}

			if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())
					&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.INPUT_QUOTE.value()), 0)) {
				resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).forEach(element -> {

					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_ENGINE_NUMBER.getCode())) {
						mapToPage.put(addMotorKey(vehicleType) + "AC-ENGINE_NUMBER",
								subString(element.get("value").asText(), LAST_7_CHARACTER));
					}

					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_CHASSIS_NUMBER.getCode())) {
						mapToPage.put(addMotorKey(vehicleType) + "AC-CHASSIS_NUMBER",
								subString(element.get("value").asText(), LAST_7_CHARACTER));
					}

					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AI_EMAILID.getCode())) {
						mapToPage.put(addMotorKey(vehicleType) + CarAttributeCode.AI_EMAILID.getCode(),
								element.get("value").asText());
					}

					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AI_MOBILE_NUMBER.getCode())) {
						mapToPage.put(addMotorKey(vehicleType) + CarAttributeCode.AI_MOBILE_NUMBER.getCode(),
								element.get("value").asText());
					}

					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_IS_MISP.getCode())) {
						newResp.put("isMisp", element.get("value").asText());
					}
				});
			}

			/* logic for adandon Transaction */
			if (session().containsKey("SOURCE") && StringUtils.isNotEmpty(session().get("SOURCE"))
					&& session().get("SOURCE").toString().equalsIgnoreCase("CAR_ABANDONTRANSACTION")) {
				mapToPage.putAll(sessionforAbondonTransaction(newResp, vehicleType));
				mapToPage.put("VARDISABLED", "REPLACEDBYABANDONTRANSACTION");
			} else {
				if (JsonUtils.isValidField(newResp, VendorQuoteConstants.PROPOSAL_PARAMS.value())
						&& JsonUtils.isValidIndex(newResp.get(VendorQuoteConstants.PROPOSAL_PARAMS.value()), 0)) {

					mapToPage.putAll(sessionforAbondonTransaction(newResp, vehicleType));
					if (session().containsKey("KEY") && StringUtils.isNotEmpty(session().get("KEY"))) {
						if (session().get("KEY").toString().equalsIgnoreCase("RENEWFROMSUMMARY")
								&& session().containsKey("OLDPROPOSALID")
								&& StringUtils.isNotEmpty(session().get("OLDPROPOSALID"))) {
							session().put("SOURCE", "CAR_RENEW");
							mapToPage.put("VARDISABLED", "CAR_RENEWFROMSUMMARY");
						}
					} else {
						mapToPage.put("VARDISABLED", "REPLACEDBYPROPOSAL");
					}
				}
			}

			/* policy renew logic */
			if (session().containsKey("SOURCE") && StringUtils.isNotEmpty(session().get("SOURCE"))) {
				if (session().get("SOURCE").toString().equalsIgnoreCase("CAR_RENEW")
						&& session().containsKey("OLDPROPOSALID")
						&& StringUtils.isNotEmpty(session().get("OLDPROPOSALID"))) {
					if (mapToPage.containsKey("VARDISABLED") && mapToPage.get("VARDISABLED") != null
							&& mapToPage.get("VARDISABLED").equalsIgnoreCase("CAR_RENEWFROMSUMMARY")) {
						mapToPage.put("VARDISABLED", "REPLACEFORRENEWALDATA");
					} else {
						try {
							String oldId = session().get("OLDPROPOSALID").toString();
							ObjectNode oldJson = (ObjectNode) proposalService
									.getProposalById(oldId, queryMap, mandatHP, null).toCompletableFuture().join();
							mapToPage.putAll(sessionforAbondonTransaction(oldJson, vehicleType));
							String nomineeAgeKey = addMotorKey(vehicleType)
									+ InsuranceAttributeCode.AI_NOMINEE_AGE.getCode();
							if (mapToPage.containsKey(nomineeAgeKey)
									&& StringUtils.isNumeric(mapToPage.get(nomineeAgeKey))) {
								Integer parseInt = Integer.parseInt(mapToPage.get(
										addMotorKey(vehicleType) + InsuranceAttributeCode.AI_NOMINEE_AGE.getCode()))
										+ 1;
								mapToPage.put(nomineeAgeKey, parseInt.toString());
							}
							if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())
									&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.INPUT_QUOTE.value()), 0)) {
								resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).forEach(element -> {
									if (element.get("code").asText().equalsIgnoreCase("AC-PREV_POLICY_NUMBER")) {
										if (JsonUtils.isValidField(element, "value")
												&& StringUtils.isNotBlank("value")) {
											mapToPage.put(addMotorKey(vehicleType) + "AC-PREV_POLICY_NUMBER",
													element.get("value").asText());
										}
									}
									if (element.get("code").asText().equalsIgnoreCase("AC-PREVIOUS_POLICY_INSURER")) {
										if (JsonUtils.isValidField(element, "value")
												&& StringUtils.isNotBlank("value")) {
											mapToPage.put(addMotorKey(vehicleType) + "AC-PREVIOUS_POLICY_INSURER",
													element.get("value").asText());
										}
									}
								});
							}
							mapToPage.put("VARDISABLED", "REPLACEFORRENEWALDATA");
						} catch (RuntimeException e) {
							throw new ArkaRunTimeException(e);
						}
					}
				}
			}

			/* logic for users logined user */
			if (!JsonUtils.isValidField(newResp, "mispBookingId") && !JsonUtils.isValidField(newResp, "referrerCode")) {
				if (session().containsKey("LOGGEDINUSER") && session().get("LOGGEDINUSER").equalsIgnoreCase("true")
						&& session().containsKey("uid") && session().get("uid") != null) {
					ObjectNode getUser = JsonUtils.newJsonObject();
					getUser.put("source", "getuser");
					mandatHP.put(HeaderParams.USER_ID.getValue(), Arrays.asList(session().get("uid")));
					ObjectNode userResponse = (ObjectNode) sessionServ.getUser(mandatQP, mandatHP, getUser)
							.toCompletableFuture().join();
					if (mapToPage.containsKey("VARDISABLED")
							&& (mapToPage.get("VARDISABLED").equalsIgnoreCase("REPLACEDBYABANDONTRANSACTION"))) {
						mapToPage.putAll(userDataForOtherFlow(userResponse, vehicleType));
					} else {
						mapToPage.putAll(userDataToMap(userResponse, vehicleType));
					}
				} else {
					if (session().containsKey("LOGGEDINUSER")) {
						session().remove("LOGGEDINUSER");
					}
				}
			}
			if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())
					&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.INPUT_QUOTE.value()), 0)) {
				resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).forEach(element -> {
					if (element.get("code").asText()
							.equalsIgnoreCase(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode())) {
						if (JsonUtils.isValidField(element, "value") && StringUtils.isNotBlank("value")) {
							newResp.put(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
									formatDate(element.get("value").asText()));
						}
					}
					if (element.get("code").asText()
							.equalsIgnoreCase(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode())) {
						if (JsonUtils.isValidField(element, "value") && StringUtils.isNotBlank("value")) {
							newResp.put(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(),
									formatDate(element.get("value").asText()));
						}
					}
					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_VEHCODE.getCode())) {
						String vehicleId = element.get("value").asText();
						try {
							Map<String, List<String>> queryString = new LinkedHashMap<>();
							queryString.put("code", Arrays.asList(vehicleId));
							JsonNode makeModelJson = procurementServ
									.getVehicleDetailsForCarInsurance(queryString, mandatHP).toCompletableFuture()
									.join();
							JsonNode makeModel = anyCollectionQueryResponseValidator
									.anyCollectionQueryGetData(makeModelJson);
							if (makeModel.size() > 0) {
								newResp.put(CarAttributeCode.AC_MAKE.getCode(), makeModel.get(0).get("make").asText());
								newResp.put(CarAttributeCode.AC_FUEL_TYPE.getCode(),
										makeModel.get(0).get("fuel_type").asText());
								newResp.put(CarAttributeCode.AC_MODEL.getCode(),
										makeModel.get(0).get("model").asText());
								newResp.put(CarAttributeCode.AC_VARIANT.getCode(),
										makeModel.get(0).get("variant").asText());
							}
						} catch (ArkaRunTimeException f) {
							throw new ArkaRunTimeException(f);
						}
					}
					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_RTOCODE.getCode())) {
						if (JsonUtils.isValidField(element, "value") && StringUtils.isNotBlank("value")) {
							JsonNode rtodetail = procurementServ
									.getRtoDetailById(element.get("value").asText(), mandatQP, mandatHP)
									.toCompletableFuture().join();
							if (JsonUtils.isValidField(rtodetail, "state")) {
								newResp.put("state", rtodetail.get("state").asText());
							}
							if (JsonUtils.isValidField(rtodetail, "city")) {
								newResp.put("city", rtodetail.get("city").asText());
							}

						}
					}
				});
			}

			Map<String, List<String>> relationshipqueryString = new HashMap<>();
			relationshipqueryString.put("code",
					Arrays.asList(InsuranceAttributeCode.AI_NOMINEE_RELATIONSHIP.getCode()));
			relationshipqueryString.put("vendorproductId", Arrays.asList(resp.get("productId").asText()));
			// relationshipqueryString.putAll(mandatQP);
			JsonNode relationshiprespJson = JsonUtils.newJsonObject();
			try {
				relationshiprespJson = procurementServ.getRestrictionForAttributes(relationshipqueryString, mandatHP)
						.toCompletableFuture().join();
			} catch (RuntimeException e) {
				throw new ArkaRunTimeException(e);
			}
			newResp.set("relationships", relationshiprespJson);

			// get previous policy Insurer
			Map<String, List<String>> prevPolicyqueryString = new HashMap<>();
			prevPolicyqueryString.put("code", Arrays.asList(InsuranceAttributeCode.AI_PREV_POLICY_INSURER.getCode()));
			prevPolicyqueryString.put("vendorproductId", Arrays.asList(resp.get("productId").asText()));
			JsonNode prevpolicyJson = JsonUtils.newJsonObject();
			try {
				prevpolicyJson = procurementServ.getRestrictionForAttributes(prevPolicyqueryString, mandatHP)
						.toCompletableFuture().join();
			} catch (RuntimeException e) {
				throw new ArkaRunTimeException(e);
			}
			newResp.set("prevPolicyInsurer", prevpolicyJson);

			// get loan provider
			Map<String, List<String>> loanProviderqueryString = new HashMap<>();
			loanProviderqueryString.put("code", Arrays.asList(InsuranceAttributeCode.AI_LOAN_PROVIDER.getCode()));
			loanProviderqueryString.put("vendorproductId", Arrays.asList(resp.get("productId").asText()));
			JsonNode loanProviderJson = JsonUtils.newJsonObject();
			try {
				loanProviderJson = procurementServ.getRestrictionForAttributes(loanProviderqueryString, mandatHP)
						.toCompletableFuture().join();
			} catch (RuntimeException e) {
				throw new ArkaRunTimeException(e);
			}
			newResp.set("loanProviderJson", loanProviderJson);

			// get occupation
			Map<String, List<String>> occupation = new HashMap<>();
			occupation.put("code", Arrays.asList(CarAttributeCode.AC_OCCUPATION.getCode()));
			occupation.put("vendorproductId", Arrays.asList(resp.get("productId").asText()));
			JsonNode occupationJson = JsonUtils.newJsonArray();
			try {
				occupationJson = procurementServ.getRestrictionForAttributes(occupation, mandatHP).toCompletableFuture()
						.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new ArkaRunTimeException(e);
			}
			newResp.set("occupationList", occupationJson);
			if (JsonUtils.isValidField(resp, "productId")) {
				mandatHP.put("iscode_tobeadded", Arrays.asList("true"));
				JsonNode product = procurementServ.getProductById(resp.get("productId").asText(), mandatHP, mandatHP)
						.toCompletableFuture().join();
				mandatHP.remove("iscode_tobeadded");
				if (JsonUtils.isValidField(product, "code")) {
					newResp.set("productCode", product.get("code"));
					newResp.set("productJson", product);
				}
			}

			JsonNode aadharValue = cache
					.getValueWithKeyPrefix(VendorQuoteConstants.REQUEST_PROPOSAL.value() + "." + proposalId + "."
							+ InsuranceAttributeCode.AI_AADHARCARD.getCode(), RedisDB.PURCHASE.getValue())
					.toCompletableFuture().join();
			if (JsonUtils.isValidField(aadharValue, "keys")) {
				aadharValue.get("keys").forEach(ele -> {
					JsonNode aadharJson = JsonUtils.toJson(ele.get(VendorQuoteConstants.REQUEST_PROPOSAL.value() + "."
							+ proposalId + "." + InsuranceAttributeCode.AI_AADHARCARD.getCode()).asText());
					if (JsonUtils.isValidField(aadharJson, InsuranceAttributeCode.AI_AADHARCARD.getCode())
							&& JsonUtils.isValidField(aadharJson, VendorQuoteConstants.VISITOR_RESPONSE_ID.value())
							&& CookieUtils.isContainKey(request(), SessionTrackingParams.VISITOR_ID.getValue())
							&& aadharJson.get(VendorQuoteConstants.VISITOR_RESPONSE_ID.value()).asText()
									.equalsIgnoreCase(CookieUtils.getCookieValue(request(),
											SessionTrackingParams.VISITOR_ID.getValue()))) {
						mapToPage.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_AADHARCARD.getCode(),
								CryptUtils.decryptData(
										aadharJson.get(InsuranceAttributeCode.AI_AADHARCARD.getCode()).asText()));
					}
				});
			}
			return ok(com.arka.consumerportal.procurementweb.views.html.checkoutpage.render(JsonUtils.toJson(newResp),
					proputils, errMsg, mapToPage, addMotorKey(vehicleType)));
		}, executionContext.current());
	}

	private String addMotorKey(String vehicleType) {

		if ("car".equalsIgnoreCase(vehicleType)) {
			return CheckoutSessionKeys.CAR_KEY.getCode();
		} else {
			return CheckoutSessionKeys.BIKE_KEY.getCode();
		}
	}

	private JsonNode insuranceErrorsList() {
		ObjectNode errorJson = JsonUtils.newJsonObject();
		Arrays.asList(ProcurementErrors.values()).forEach(error -> {
			errorJson.put(error.value().toString(), propUtils.get(error.value().toString()));
		});
		return errorJson;
	}

	public CompletionStage<Result> abandonTransaction(String proposalId) {
		ArrayNode errMsg = Json.newArray();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Arrays.asList(Checkouterrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		if (session().containsKey("KEY")) {
			session().remove("KEY");
		}
		if (session().containsKey("SOURCE")) {
			session().remove("SOURCE");
			if (session().containsKey("OLDPROPOSALID")) {
				session().remove("OLDPROPOSALID");
			}
		}
		session().put("SOURCE", "CAR_ABANDONTRANSACTION");
		// /* logic for users logined user */
		// if (session().containsKey("uid") && session().get("uid") != null) {
		// session().put("LOGGEDINUSER", "true");
		// }
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));

		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
					.checkOutPage(proposalId).url());
		}, executionContext.current());
	}

	private String formatDate(String startDate) {
		return new SimpleDateFormat("dd-MM-yyyy").format(EncodingUtils.getDateFromString(startDate, "yyyy-MM-dd"))
				.toString();
	}

	public CompletionStage<Result> refetchPlan(String proposalId) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		ObjectNode detailsJson = Json.newObject();
		JsonNode proposalResponse = proposalService.getProposalById(proposalId, queryMap, mandatHP, null)
				.toCompletableFuture().join();
		detailsJson.put("enquiryId", proposalResponse.get("enquiryId").asText());
		detailsJson.put("productId", proposalResponse.get("productId").asText());
		detailsJson.put("proposalId", proposalResponse.get("proposalId").asText());
		detailsJson.put("quoteKey", proposalResponse.get("quoteKey").asText());
		return procurementServ.fetchPlan(mandatQP, mandatHP, detailsJson).thenApplyAsync(resp -> {
			return ok(JsonUtils.toJson(resp));
		});
	}

	public CompletionStage<Result> checkOutSummary(String proposalId) {
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));

		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {

			if (resp.get("progress").asText().equalsIgnoreCase("PURCHASED")) {
				return ok(com.arka.consumerportal.procurementweb.views.html.purchased_proposal_error.render(propUtils,
						resp.get("enquiryId").asText(), resp.get("categoryCode").asText()));
			}

			ObjectNode newResp = (ObjectNode) resp;

			String vehicleType = getVehicleType(mandatHP, queryMap, newResp);
			newResp.set("vehicleType", Json.toJson(vehicleType));

			if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())
					&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.INPUT_QUOTE.value()), 0)) {
				resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).forEach(element -> {
					if (element.get("code").asText()
							.equalsIgnoreCase(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode())) {
						newResp.put(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode(),
								formatDate(element.get("value").asText()));
					}
					if (element.get("code").asText()
							.equalsIgnoreCase(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode())) {
						newResp.put(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(),
								formatDate(element.get("value").asText()));
					}

					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_IS_OTP_VERIFIED.getCode())) {
						newResp.put("isOtpVerified", element.get("value").asText());
					}
				});
			}
			if (session().containsKey("KEY")) {
				session().remove("KEY");
			}
			if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())
					&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.INPUT_QUOTE.value()), 0)) {
				resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).forEach(element -> {
					if (element.get("code").asText().equalsIgnoreCase(CarAttributeCode.AC_VEHCODE.getCode())) {
						String vehicleId = element.get("value").asText();
						try {
							Map<String, List<String>> queryString = new LinkedHashMap<>();
							queryString.put("code", Arrays.asList(vehicleId));
							JsonNode makeModelJson = procurementServ
									.getVehicleDetailsForCarInsurance(queryString, mandatHP).toCompletableFuture()
									.join();
							JsonNode makeModel = anyCollectionQueryResponseValidator
									.anyCollectionQueryGetData(makeModelJson);
							if (makeModel.size() > 0) {
								newResp.put(CarAttributeCode.AC_MAKE.getCode(), makeModel.get(0).get("make").asText());
								newResp.put(CarAttributeCode.AC_FUEL_TYPE.getCode(),
										makeModel.get(0).get("fuel_type").asText());
								newResp.put(CarAttributeCode.AC_MODEL.getCode(),
										makeModel.get(0).get("model").asText());
								newResp.put(CarAttributeCode.AC_VARIANT.getCode(),
										makeModel.get(0).get("variant").asText());
							}
						} catch (ArkaRunTimeException f) {
							throw new ArkaRunTimeException(f);
						}
					}
				});
			}
			if (session().containsKey("SOURCE") && StringUtils.isNotEmpty(session().get("SOURCE"))
					&& session().get("SOURCE").toString().equalsIgnoreCase("CAR_RENEW")
					&& session().containsKey("OLDPROPOSALID")
					&& StringUtils.isNotEmpty(session().get("OLDPROPOSALID"))) {
				if (session().containsKey("OLDPROPOSALID")) {
					session().put("KEY", "RENEWFROMSUMMARY");
				}
			}
			JsonNode product = null;
			if (JsonUtils.isValidField(resp, "productId")) {
				product = procurementServ.getProductById(resp.get("productId").asText(), mandatHP, mandatHP)
						.toCompletableFuture().join();
				if (JsonUtils.isValidField(product, "code")) {
					newResp.set("productCode", product.get("code"));
				}
			}

			newResp.get("add_on_covers").forEach(action -> {
				System.out.println(action);
			});
			String visitorId = mandatHP.get("x-visitor-id").get(0);
			String userId = session().get(SessionTrackingParams.USER_ID.getValue()) != null
					? session().get(SessionTrackingParams.USER_ID.getValue()).toString()
					: null;
					
			JsonNode otpVerified = session(SessionTrackingParams.SESSION_ID.getValue()) != null
					? cache.getValueWithKeyPrefix("checkout" + "." + session(SessionTrackingParams.SESSION_ID.getValue()) + "." + "otpVerified",
							RedisDB.PURCHASE.getValue()).toCompletableFuture().join()
					: null;

			if ((JsonUtils.isValidField(otpVerified, "keys") && otpVerified.get("keys").get(0) != null
					&& otpVerified.get("keys").get(0).get("checkout" + "." + session(SessionTrackingParams.SESSION_ID.getValue()) + "." + "otpVerified")
							.asBoolean())
					|| (newResp.get("isOtpVerified") != null && newResp.get("isOtpVerified").asBoolean())
					|| userId != null) {
				newResp.set("otpVerified", Json.toJson(true));
			} else {
				newResp.set("otpVerified", Json.toJson(false));
			}

			return ok(com.arka.consumerportal.procurementweb.views.html.checkoutsummarypage
					.render(JsonUtils.toJson(newResp), proputils));
		}, executionContext.current());
	}

	private String getVehicleType(Map<String, List<String>> mandatHP, Map<String, List<String>> queryMap,
			ObjectNode newResp) {
		JsonNode categoryJson = null;
		try {
			categoryJson = procurementServ.getCategoryById(newResp.get("categoryId").asText(), queryMap, mandatHP)
					.toCompletableFuture().get();

			if (categoryJson.get("error") == null) {
				newResp.set(VEHICLE_TYPE, Json.toJson(categoryJson.get("code").asText().toLowerCase()));
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return categoryJson.get("code").asText().toLowerCase();
	}

	public CompletionStage<Result> validateProposal(String proposalId) {
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {

			if (resp.get("progress").asText().equalsIgnoreCase("PURCHASED")) {
				ObjectNode response = JsonNodeFactory.instance.objectNode();
				response.put("error", "proposal already purchased");
				return ok(response);
			}

			JsonNode obj = proposalService.verifyProposal(proposalId, queryMap, mandatHP, resp).toCompletableFuture()
					.join();
			return ok(obj);
		}, executionContext.current());
	}

	public CompletionStage<Result> vendorPortal(String proposalId) {
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			return ok(com.arka.consumerportal.procurementweb.views.html.portalpage.render(JsonUtils.toJson(resp),
					proputils));
		}, executionContext.current());
	}

	public CompletionStage<Result> productPurchased(String proposalId) {
		Session session = session();
		ObjectNode detailsJson = Json.newObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		return proposalService.purchaseProposal(proposalId, mandatQP, mandatHP, detailsJson).thenApplyAsync(resp -> {
			System.out.println("purchased reponse---------" + resp);
			String redisDB = RedisDB.PURCHASE.getValue();
			if (JsonUtils.isValidField(resp, JsonUtils.ERROR)) {
				System.out.println("PURCHASE UNSUCCESSFUL ERROR === > " + redisDB);
				((ObjectNode) resp).set("errorKeyJson", insuranceErrorsList());
				cache.getOrElse("errorJson" + "." + proposalId, redisDB, () -> JsonUtils.stringify(resp));
				return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
						.cancelled(proposalId, "CANCELLED"));
			} else {
				System.out.println("its here--------");
				session.put(SessionTrackingParams.PROPOSAL_ID.getValue(), resp.get("proposalId").asText());
				cache.getOrElse(PolicyFields.CATEGORY_ID.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.CATEGORY_ID.getCode()).asText());
				cache.getOrElse(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.CATEGORY_CODE.getCode()).asText());
				cache.getOrElse(PolicyFields.VENDOR_ID.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.VENDOR_ID.getCode()).asText());
				cache.getOrElse(PolicyFields.USER_ID.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.USER_ID.getCode()).asText());
				cache.getOrElse(PolicyFields.PRODUCT_ID.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.PRODUCT_ID.getCode()).asText());
				cache.getOrElse(PolicyFields.PROPOSAL_ID.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.PROPOSAL_ID.getCode()).asText());
				cache.getOrElse(PolicyFields.SUM_ASSURED.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.SUM_ASSURED.getCode()).asText());
				cache.getOrElse(PolicyFields.PREMIUM.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.PREMIUM.getCode()).asText());
				cache.getOrElse(PolicyFields.POLICY_HOLDER_NAME.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.POLICY_HOLDER_NAME.getCode()).asText());
				cache.getOrElse(PolicyFields.VENDOR_CODE.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.VENDOR_CODE.getCode()).asText());
				cache.getOrElse(PolicyFields.PRODUCT_CODE.getCode() + "." + proposalId, redisDB,
						() -> resp.get(PolicyFields.PRODUCT_CODE.getCode()).asText());
				cache.getOrElse(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId, redisDB,
						() -> resp.get("policyNumber").asText());
				if (resp.has(PolicyFields.OLD_POLICY_ID.getCode())) {
					cache.getOrElse(PolicyFields.OLD_POLICY_ID.getCode() + "." + proposalId, redisDB,
							() -> resp.get(PolicyFields.OLD_POLICY_ID.getCode()).asText());
				}
				if (resp.has(PolicyFields.PREV_POLICY_NUMBER.getCode())) {
					cache.getOrElse(PolicyFields.PREV_POLICY_NUMBER.getCode() + "." + proposalId, redisDB,
							() -> resp.get(PolicyFields.PREV_POLICY_NUMBER.getCode()).asText());
				}
				// misp
				if (resp.get("referrerCode") != null) {
					session.put(PolicyFields.REFERRER_CODE.getCode(), resp.get("referrerCode").asText());
				}
				if (resp.get("mispBookingId") != null) {
					session.put(PolicyFields.MISP_BOOKING_ID.getCode(), resp.get("mispBookingId").asText());
					JsonNode mispBookingJson = null;
					JsonNode mispJson = null;
					try {
						mispBookingJson = policyService
								.getMispBooking(resp.get("mispBookingId").asText(), mandatQP, mandatHP)
								.toCompletableFuture().get();
						mispJson = userserv.getReferrerById(mispBookingJson.get("mispId").asText(), mandatQP, mandatHP)
								.toCompletableFuture().get();

						// Change Booking status to PAYMENT_PENDING
						ObjectNode booking = Json.newObject();
						booking.put("op", "replace");
						booking.put("path", "/status");
						booking.put("value", "PAYMENT_PENDING");
						mispService.updateBookingStatus(resp.get("mispBookingId").asText(), booking)
								.toCompletableFuture().get();
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
					session.put("mispId", (mispJson.get("id").asText()));
				}
			}
			return ok(com.arka.consumerportal.procurementweb.views.html.payments.vendorpage
					.render(JsonUtils.toJson(resp), proputils));
		}, executionContext.current());

	}

	public CompletionStage<Result> policyPurchased(String proposalId) {
		return CompletableFuture.supplyAsync(() -> {
			ObjectNode policyJson = JsonUtils.newJsonObject();
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(),
					getCacheValue(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId));
			System.out.println(getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId));
			return ok(com.arka.consumerportal.procurementweb.views.html.purchased.render(propUtils, policyJson,
					getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId)));
		}, executionContext.current());
	}

	public CompletionStage<Result> productSuccess() {
		Session session = session();
		session().forEach((k, v) -> System.out.println(k + " " + v));
		DynamicForm df = formFactory.form().bindFromRequest();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		ObjectNode policyJson = Json.newObject();
		String proposalId = session.get(SessionTrackingParams.PROPOSAL_ID.getValue());
		session.replace(SessionTrackingParams.PROPOSAL_ID.getValue(), proposalId);
		policyJson.put(PolicyFields.CATEGORY_ID.getCode(),
				getCacheValue(PolicyFields.CATEGORY_ID.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.CATEGORY_CODE.getCode(),
				getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.VENDOR_ID.getCode(),
				getCacheValue(PolicyFields.VENDOR_ID.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.USER_ID.getCode(),
				getCacheValue(PolicyFields.USER_ID.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.PRODUCT_ID.getCode(),
				getCacheValue(PolicyFields.PRODUCT_ID.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.PROPOSAL_ID.getCode(),
				getCacheValue(PolicyFields.PROPOSAL_ID.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.SUM_ASSURED.getCode(),
				getCacheValue(PolicyFields.SUM_ASSURED.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.PREMIUM.getCode(),
				getCacheValue(PolicyFields.PREMIUM.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(),
				getCacheValue(PolicyFields.POLICY_HOLDER_NAME.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.OLD_POLICY_ID.getCode(),
				getCacheValue(PolicyFields.OLD_POLICY_ID.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.PREV_POLICY_NUMBER.getCode(),
				getCacheValue(PolicyFields.PREV_POLICY_NUMBER.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.VENDOR_CODE.getCode(),
				getCacheValue(PolicyFields.VENDOR_CODE.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.PRODUCT_CODE.getCode(),
				getCacheValue(PolicyFields.PRODUCT_CODE.getCode() + "." + proposalId));
		policyJson.put(PolicyFields.PAYMENT_FREQUENCY.getCode(), PaymentFrequency.ANNUAL.name());
		policyJson.put(PolicyFields.PAYMENT_ID.getCode(), RandomStringUtils.randomAlphanumeric(10));
		policyJson.put(PolicyFields.PAYMENT_MODE.getCode(), PaymentMode.ONLINE.name());

		// misp
		if (session.get("mispBookingId") != null) {
			policyJson.put(PolicyFields.MISP_BOOKING_ID.getCode(), session.get("mispBookingId"));
		}
		if (session.get("referrerCode") != null) {
			policyJson.put(PolicyFields.REFERRER_CODE.getCode(), session.get("referrerCode"));
		}

		java.util.Date date = new java.util.Date();
		java.util.Date expiryDate = DateUtils.addYears(date, 1);
		SimpleDateFormat formatter = new SimpleDateFormat(DateConstants.DB_DATE_FORMAT.value());
		String dateStr = formatter.format(date);
		String expiryDateStr = formatter.format(expiryDate);
		policyJson.put(PolicyFields.ISSUED_TIME.getCode(), dateStr);
		policyJson.put(PolicyFields.COMMENCEMENT_DATE.getCode(), dateStr);
		policyJson.put(PolicyFields.EXPIRY_DATE.getCode(), expiryDateStr);
		String vendorCode = getCacheValue(PolicyFields.VENDOR_CODE.getCode() + "." + proposalId);

		if (vendorCode.equalsIgnoreCase("RELIGARE")) {
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), df.data().get("policyNumber"));
		} else if (vendorCode.equalsIgnoreCase("RS")) {
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), df.data().get("policyNO"));
		} else if (vendorCode.equalsIgnoreCase("ITGI")) {
			String itgiResponse = df.get("ITGIResponse");
			String[] itgiResponseArray = itgiResponse.split("[|]");
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), itgiResponseArray[2]);
		} else if (vendorCode.equalsIgnoreCase("USGI")) {
			String usgiResponse = df.get("MSG");
			String[] usgiResponseArray = usgiResponse.split("[|]");
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), usgiResponseArray[3]);
		} else if (vendorCode.equalsIgnoreCase("BAGIC")) {
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), df.data().get("policyref"));
		} else if (vendorCode.equalsIgnoreCase("HDFC_ERGO")) {
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), df.data().get("PolicyNo"));
		} else if (vendorCode.equalsIgnoreCase("LVGI")) {
			policyJson.put(PolicyFields.POLICY_NUMBER.getCode(),
					getCacheValue(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId));
			cache.inValidateCacheWithExactKey(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId,
					RedisDB.PURCHASE.getValue());
		}
		String categoryCode = getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId);
		clearPurchaseProposalCache(proposalId, RedisDB.PURCHASE.getValue());
		if (JsonUtils.isValidField(policyJson, PolicyFields.POLICY_NUMBER.getCode())
				&& Strings.isNullOrEmpty(policyJson.get(PolicyFields.POLICY_NUMBER.getCode()).asText())
				&& policyJson.get(PolicyFields.POLICY_NUMBER.getCode()).asText().equals("0")) {
			ObjectNode policyErrorJson = Json.newObject();
			policyErrorJson.put(PolicyFields.CATEGORY_CODE.getCode(), categoryCode);
			policyErrorJson.put(JsonUtils.ERROR, ProcurementErrors.SUBSYSTEM_ERROR.value());
			((ObjectNode) policyErrorJson).set("errorKeyJson", insuranceErrorsList());
			cache.getOrElse("errorJson" + "." + proposalId, RedisDB.PURCHASE.getValue(),
					() -> JsonUtils.stringify(policyErrorJson));
			return CompletableFuture.completedFuture(
					redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
							.cancelled(proposalId, "TERMINATED_BY_ERROR")));
		}
		if (StringUtils.isNotEmpty(getCacheValue(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId))) {
			policyJson.put("vendor_policy_number",
					getCacheValue(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId));
		}
		return proposalService.completePurchase(policyJson, mandatQP, mandatHP).thenApplyAsync(resp -> {

			System.out.println("//////////// resp" + resp);

			if (JsonUtils.isValidField(resp, JsonUtils.ERROR)) {
				if (JsonUtils.isValidField(resp.get(JsonUtils.ERROR), "policy_number")) {
					ObjectNode policyRespJson = JsonUtils.newJsonObject();
					policyRespJson.put(PolicyFields.POLICY_NUMBER.getCode(),
							getCacheValue(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId));
					policyRespJson.put("error", "policy.creation");
					return ok(com.arka.consumerportal.procurementweb.views.html.purchased.render(propUtils,
							policyRespJson, getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId)));
				} else {
					((ObjectNode) resp).put(PolicyFields.CATEGORY_CODE.getCode(),
							getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId));
					((ObjectNode) resp).set("errorKeyJson", insuranceErrorsList());
					cache.getOrElse("errorJson" + "." + proposalId, RedisDB.PURCHASE.getValue(),
							() -> JsonUtils.stringify(resp));
					return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
							.cancelled(proposalId, "TERMINATED_BY_ERROR"));
				}
			} else {
				if (session.get("mispId") != null) {
					Map<String, String[]> queryString = request().queryString();
					Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
					map.putAll(mandatQP);
					map.put(Source.SRC.value(), Arrays.asList(Source.GARAGE_LIST.value()));
					map.put("vendorCode", Arrays.asList(vendorCode));
					JsonNode garages = null;
					try {
						garages = procurementServ.getGarages(map, mandatHP).toCompletableFuture().get();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}

					JsonNode referrerDetails = null;
					try {
						referrerDetails = userserv.getReferrerById(session.get("mispId"), mandatQP, mandatHP)
								.toCompletableFuture().get();
						// Change Booking status to POLICY_PURCHASED
						if (session.get("mispBookingId") != null) {
							ObjectNode booking = Json.newObject();
							booking.put("op", "replace");
							booking.put("path", "/status");
							booking.put("value", "POLICY_PURCHASED");
							mispService.updateBookingStatus(session.get("mispBookingId"), booking).toCompletableFuture()
									.get();
						}

					} catch (Exception e) {
						// TODO exception
					}
					if (JsonUtils.isValidField(garages, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
						ObjectNode data = Json.newObject();
						data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, garages.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
						data.set("referrerDetails", referrerDetails);
						data.put("vendorCode", vendorCode);
						data.put("policyId", resp.get("id").asText());
						return ok(com.arka.consumerportal.procurementweb.views.html.misp.misppurchased.render(data,
								session, propUtils));

					} else {
						if (JsonUtils.isValidField(policyJson, PolicyFields.POLICY_NUMBER.getCode())) {
							cache.getOrElse(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId,
									RedisDB.PURCHASE.getValue(),
									() -> policyJson.get(PolicyFields.POLICY_NUMBER.getCode()).asText());
						}
						// Change Booking status to PAYMENT_FAILED
						if (session.get("mispBookingId") != null) {
							ObjectNode booking = Json.newObject();
							booking.put("op", "replace");
							booking.put("path", "/status");
							booking.put("value", "PAYMENT_FAILED");

							try {
								mispService.updateBookingStatus(session.get("mispBookingId"), booking)
										.toCompletableFuture().get();
							} catch (InterruptedException | ExecutionException e) {
								e.printStackTrace();
							}
						}
						return redirect(
								com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
										.policyPurchased(proposalId));

					}
				} else {
					if (JsonUtils.isValidField(policyJson, PolicyFields.POLICY_NUMBER.getCode())) {
						cache.getOrElse(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId,
								RedisDB.PURCHASE.getValue(),
								() -> policyJson.get(PolicyFields.POLICY_NUMBER.getCode()).asText());
					}

					if (vendorCode.equalsIgnoreCase("BAGIC")) {

						System.out.println("//////////// Inside bajaj" + resp);

						JsonNode cmsJson = vendorServ.policyPdf(mandatQP, mandatHP, policyJson).toCompletableFuture()
								.join();

						System.out.println("//////////// cmsJson" + cmsJson);

						if (JsonUtils.isValidField(cmsJson, PolicyFields.CMS_ID.getCode())) {

							String cmsId = cmsJson.get(PolicyFields.CMS_ID.getCode()).asText();
							if (cmsId != null && !cmsId.trim().isEmpty()) {
								ObjectNode detailsJson = Json.newObject();
								detailsJson.put("op", "replace");
								detailsJson.put("path", "/cms_id");
								detailsJson.put("value", cmsId);
								JsonNode respJson = policyService
										.patchPolicy(resp.get("id").asText(), mandatQP, mandatHP, detailsJson)
										.toCompletableFuture().join();

								System.out.println("//////////// respJson" + respJson);

								if (respJson != null) {
									if (respJson.size() != 0) {
										logger.debug("CMS ID " + cmsId + " not updated in policy table for policy "
												+ resp.get("id").asText());
									} else {
										logger.debug("CMS ID " + cmsId + " updated in policy table for policy "
												+ resp.get("id").asText());
									}
								}
							}

						}
					}
					return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
							.policyPurchased(proposalId));
				}

				// return
				// ok(com.arka.consumerportal.procurementweb.views.html.purchased.render(propUtils));
			}
		}, executionContext.current());

	}

	private void clearPurchaseProposalCache(String proposalId, String dbName) {
		cache.inValidateCacheWithExactKey(PolicyFields.CATEGORY_ID.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.VENDOR_ID.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.USER_ID.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.PRODUCT_ID.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.PROPOSAL_ID.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.SUM_ASSURED.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.PREMIUM.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.POLICY_HOLDER_NAME.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.OLD_POLICY_ID.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.PREV_POLICY_NUMBER.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.PRODUCT_CODE.getCode() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(PolicyFields.VENDOR_CODE.getCode() + "." + proposalId, dbName);
	}

	private String getCacheValue(String categoryAttribute) {
		return CacheService.getCacheKeyValue(
				cache.getValueWithKeyPrefix(CacheService.getCacheKey(categoryAttribute, null, null),
						RedisDB.PURCHASE.getValue()).toCompletableFuture().join(),
				CacheService.getCacheKey(categoryAttribute, null, null));
	}

	public CompletionStage<Result> purchased() {
		Session session = session();
		Map<String, String[]> queryString = request().queryString();
		String vendorCode = session.get("vendorCode");
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.GARAGE_LIST.value()));
		map.put("vendorCode", Arrays.asList(vendorCode));
		return procurementServ.getGarages(map, mandatHP).thenApplyAsync(response -> {
			JsonNode referrerDetails = null;
			try {
				referrerDetails = userserv.getReferrerById(session.get("mispId"), mandatQP, mandatHP)
						.toCompletableFuture().get();
			} catch (Exception e) {
			}
			if (JsonUtils.isValidField(response, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, response.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				data.put("vendorCode", session.get("vendorCode"));
				data.set("referrerDetails", referrerDetails);
				data.put("policyId", session.get("policyId"));
				// return ok();
				return ok(com.arka.consumerportal.procurementweb.views.html.purchased.render(propUtils,
						JsonUtils.newJsonObject(), ""));
			} else {
				return ok();
			}
		}, executionContext.current());
	}

	public Result cancelled(String proposalId, String status) {
		ObjectNode detailsJson = Json.newObject();
		detailsJson.put("op", "replace");
		detailsJson.put("path", "/progress");
		detailsJson.put("value", status);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		proposalService.updateProposal(proposalId, mandatQP, mandatHP, detailsJson);
		String errorStr = getCacheValue("errorJson" + "." + proposalId);
		cache.inValidateCacheWithExactKey("errorJson" + "." + proposalId, RedisDB.PURCHASE.getValue());
		JsonNode errorJson = JsonUtils.newJsonObject();
		if (StringUtils.isNotEmpty(errorStr)) {
			errorJson = JsonUtils.toJson(errorStr);
		}
		return ok(com.arka.consumerportal.procurementweb.views.html.cancelled.render(errorJson, proputils, proposalId,
				getCacheValue(PolicyFields.CATEGORY_CODE.getCode() + "." + proposalId)));
	}

	public Result updateUserIdInProposal() {
		DynamicForm dynamicForm = formFactory.form().bindFromRequest();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		String propaslId = dynamicForm.get("proposal_id");
		String userId = dynamicForm.get("user_id");
		if (propaslId != null && !propaslId.trim().isEmpty() && userId != null && !userId.trim().isEmpty()) {
			ObjectNode detailsJson = Json.newObject();
			detailsJson.put("op", "replace");
			detailsJson.put("path", "/userId");
			detailsJson.put("value", userId);
			JsonNode respJson = proposalService.updateProposal(propaslId, mandatQP, mandatHP, detailsJson)
					.toCompletableFuture().join();
			if (respJson != null) {
				if (respJson.size() == 0)
					return ok("ok");
				if (JsonUtils.isValidField(respJson, JsonUtils.ERROR)) {
					return internalServerError(respJson);
				}
			}
		}
		return notFound(JsonUtils.getJsonResponseForResourceNotFound());
	}

	public CompletionStage<Result> saveProposal(String proposalId) {
		ArrayNode jsonInput = (ArrayNode) request().body().asJson();
		ArrayNode savingattributes = JsonUtils.newJsonArray();
		List<String> deleteattributes = new ArrayList<String>();
		Map<String, List<String>> mandatQP = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		jsonInput.elements().forEachRemaining(action -> {
			if (action != null
					&& JsonUtils.isValidField(action, VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value())
					&& action.get(VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value()).asText()
							.equals(CarAttributeCode.AC_REGISTRATION_NUMBER.getCode())) {
				if (action.get("value") instanceof TextNode) {
				} else if (action.get("value") instanceof JsonNode) {
					ArrayNode resgistartionArr = (ArrayNode) action.get("value");
					if (resgistartionArr.size() > 0) {
						savingattributes.add(action);
					} else {
					}
				}
			}
			if (!JsonUtils.isValidField(action, "value") || StringUtils.isBlank(action.get("value").asText())) {
				if (JsonUtils.isValidField(action, VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value())) {
					deleteattributes
							.add(action.get(VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value()).asText());
				}
			} else {
				if (JsonUtils.isValidField(action, VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value())
						&& JsonUtils.isValidField(action, "value")
						&& action.get(VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value()).asText()
								.equalsIgnoreCase(InsuranceAttributeCode.AI_AADHARCARD.getCode())) {
					ObjectNode obj = JsonUtils.newJsonObject();
					obj.put(InsuranceAttributeCode.AI_AADHARCARD.getCode(),
							CryptUtils.encryptData(action.get("value").asText()));
					if (CookieUtils.isContainKey(request(), SessionTrackingParams.VISITOR_ID.getValue())) {
						obj.put(VendorQuoteConstants.VISITOR_RESPONSE_ID.value(),
								CookieUtils.getCookieValue(request(), SessionTrackingParams.VISITOR_ID.getValue()));
					}
					cache.getOrElse(
							CacheService.getCacheKey(
									VendorQuoteConstants.REQUEST_PROPOSAL.value() + "." + String.valueOf(proposalId),
									null, InsuranceAttributeCode.AI_AADHARCARD.getCode()),
							RedisDB.PURCHASE.getValue(), () -> {
								return JsonUtils.stringify(obj);
							});
				} else {
					savingattributes.add(action);
				}
			}

		});
		deleteProposalAttributes(proposalId, deleteattributes);
		ObjectNode patchJson = Json.newObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.REPLACE.value());
		patchJson.put(UpdateNames.PATH.value(), "/proposalAttributes");
		patchJson.set(UpdateNames.VALUE.value(), savingattributes);

		return proposalService.updateProposal(proposalId, mandatQP, mandatHP, patchJson).thenApplyAsync(resp -> {
			return ok(resp);
		}, executionContext.current());
	}

	private CompletionStage<Result> deleteProposalAttributes(String proposalId, List<String> attributes) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		mandatQP.put("code", attributes);
		return proposalService.deleteProposalAttribute(proposalId, mandatQP, mandatHP).thenApplyAsync(resp -> {
			return ok(resp);
		}, executionContext.current());

	}

	private Map<String, String> userDataForOtherFlow(ObjectNode response, String vehicleType) {
		Map<String, String> returnMap = new HashMap<>();
		if (JsonUtils.isValidField(response, "loginNames") && JsonUtils.isValidIndex(response.get("loginNames"), 0)) {
			response.get("loginNames").forEach(action -> {
				if (JsonUtils.isValidField(action, "type") && JsonUtils.isValidField(action, "loginName")) {
					if (action.get("type").asText().equalsIgnoreCase("EMAIL")) {
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_EMAIL.getCode(),
								action.get("loginName").asText());
					}
					if (action.get("type").asText().equalsIgnoreCase("MOBILE_NUMBER")) {
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_MOBILE_NUMBER.getCode(),
								action.get("loginName").asText());
					}
				}
			});
		}
		settingAddress(returnMap, response, vehicleType);
		return returnMap;
	}

	private Map<String, String> userDataToMap(ObjectNode response, String vehicleType) {
		Map<String, String> returnMap = new HashMap<>();
		if (session().containsKey("SOURCE") && StringUtils.isNotEmpty(session().get("SOURCE"))
				&& session().get("SOURCE").toString().equalsIgnoreCase("CAR_RENEW")
				&& session().containsKey("OLDPROPOSALID") && StringUtils.isNotEmpty(session().get("OLDPROPOSALID"))) {
			// its from renemwal");
		} else {
			if (JsonUtils.isValidField(response, "profile")) {
				if (JsonUtils.isValidField(response.get("profile"), "isCompany")
						&& response.get("profile").get("isCompany").asText().equalsIgnoreCase("false")) {
					if (JsonUtils.isValidField(response.get("profile"), "firstName")
							&& JsonUtils.isValidField(response.get("profile"), "lastName")) {
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_FIRST_NAME.getCode(),
								response.get("profile").get("firstName").asText());
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_LAST_NAME.getCode(),
								response.get("profile").get("lastName").asText());
					}
				} else {
					if (JsonUtils.isValidField(response.get("profile"), "isCompany")
							&& response.get("profile").get("isCompany").asText().equalsIgnoreCase("true")
							&& JsonUtils.isValidField(response.get("profile"), "companyName")
							&& JsonUtils.isValidField(response.get("profile"), "companyShortName")) {
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_COMPANY_NAME.getCode(),
								response.get("profile").get("companyName").asText());
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_SHORT_NAME.getCode(),
								response.get("profile").get("companyShortName").asText());
					}
				}
				if (JsonUtils.isValidField(response.get("profile"), "dob")) {
					returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_DOB.getCode(),
							response.get("profile").get("dob").asText());
				}
				if (JsonUtils.isValidField(response.get("profile"), "gender")) {
					returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_GENDER.getCode(),
							response.get("profile").get("gender").asText());
				}
			}
		}

		if (JsonUtils.isValidField(response, "loginNames") && JsonUtils.isValidIndex(response.get("loginNames"), 0)) {
			response.get("loginNames").forEach(action -> {
				if (JsonUtils.isValidField(action, "type") && JsonUtils.isValidField(action, "loginName")) {
					if (action.get("type").asText().equalsIgnoreCase("EMAIL")) {
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_EMAIL.getCode(),
								action.get("loginName").asText());
					}
					if (action.get("type").asText().equalsIgnoreCase("MOBILE_NUMBER")) {
						returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_MOBILE_NUMBER.getCode(),
								action.get("loginName").asText());
					}
				}
			});
		}
		settingAddress(returnMap, response, vehicleType);
		return returnMap;
	}

	private void settingAddress(Map<String, String> returnMap, ObjectNode response, String vehicleType) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (JsonUtils.isValidField(response, "user_address")
				&& JsonUtils.isValidField(response.get("user_address"), "place")
				&& JsonUtils.isValidField(response.get("user_address"), "city")
				&& JsonUtils.isValidField(response.get("user_address"), "cityCode")
				&& JsonUtils.isValidField(response.get("user_address"), "postalCode")
				&& JsonUtils.isValidField(response.get("user_address"), "countryCode")
				&& JsonUtils.isValidField(response.get("user_address"), "state")
				&& JsonUtils.isValidField(response.get("user_address"), "addressLine1")) {
			returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_PLACE.getCode(),
					response.get("user_address").get("place").asText());
			if (JsonUtils.isValidField(response.get("user_address"), "addressLine2")) {
				returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_ADDRESS_LINE_1.getCode(),
						response.get("user_address").get("addressLine1").asText());
				returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_ADDRESS_LINE_2.getCode(),
						response.get("user_address").get("addressLine2").asText());
			} else {
				returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_ADDRESS_LINE_1.getCode(),
						response.get("user_address").get("addressLine1").asText());
			}
			returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_COUNTRY_CODE.getCode(),
					response.get("user_address").get("countryCode").asText());
			returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_PINCODE.getCode(),
					response.get("user_address").get("postalCode").asText());
			returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_DISTRICT.getCode(),
					response.get("user_address").get("city").asText());
			returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_DISTRICT_CODE.getCode(),
					response.get("user_address").get("cityCode").asText());
			if (response.get("user_address").get("countryCode").asText().equalsIgnoreCase("IN")) {
				returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_COUNTRY.getCode(), "India");
			}
			if (JsonUtils.isValidField(response.get("user_address").get("state"), "id")) {
				returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_STATE_ID.getCode(),
						response.get("user_address").get("state").get("id").asText());
				mandatQP.put("country_code", Arrays.asList("IN"));
				String country_code = "IN";
				JsonNode stateNameRes = procurementServ
						.getStateById(response.get("user_address").get("state").get("id").asText(), country_code,
								mandatQP, mandatHP)
						.toCompletableFuture().join();
				if (JsonUtils.isValidField(stateNameRes, "state_name")) {
					returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_STATE.getCode(),
							stateNameRes.get("state_name").asText());
				}
				if (JsonUtils.isValidField(stateNameRes, "state_code")) {
					returnMap.put(addMotorKey(vehicleType) + InsuranceAttributeCode.AI_STATE_CODE.getCode(),
							stateNameRes.get("state_code").asText());
				}
			}
		}

	}

	private Map<String, String> sessionforAbondonTransaction(ObjectNode sessionJson, String vehicleType) {
		Map<String, String> returnMap = new HashMap<>();
		if (JsonUtils.isValidField(sessionJson, VendorQuoteConstants.PROPOSAL_PARAMS.value())
				&& sessionJson.get(VendorQuoteConstants.PROPOSAL_PARAMS.value()).size() >= 1) {
			ArrayNode values = (ArrayNode) sessionJson.get(VendorQuoteConstants.PROPOSAL_PARAMS.value());
			values.forEach(action -> {
				if (JsonUtils.isValidField(action, "code")) {
					if (action.get("code").equals(CarAttributeCode.AC_REGISTRATION_NUMBER.getCode())) {
						returnMap.put(addMotorKey(vehicleType) + action.get("code").asText(),
								Json.stringify(action.get("value")));
					} else {
						returnMap.put(addMotorKey(vehicleType) + action.get("code").asText(),
								action.get("value").asText());
					}
				}
			});
		}
		return returnMap;
	}

	private String subString(String value, int length) {
		return (value.length() > length) ? value.substring(value.length() - length) : value;
	}

	public CompletionStage<Result> getLoanProviders(String categoryId, String productId) {
		Map<String, List<String>> queryString = new HashMap<>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		ObjectNode detailsJson = Json.newObject();
		ArrayNode returnList = Json.newArray();
		queryString.putAll(mandatQP);
		queryString.put("code", Arrays.asList(InsuranceAttributeCode.AI_LOAN_PROVIDER.getCode()));
		JsonNode jsonNode = procurementServ.getRestrictionForAttributes(queryString, mandatHP).toCompletableFuture()
				.join();
		if (JsonUtils.isValidField(jsonNode, InsuranceAttributeCode.AI_LOAN_PROVIDER.getCode())) {
			if (JsonUtils.isValidField(jsonNode.get(InsuranceAttributeCode.AI_LOAN_PROVIDER.getCode()),
					"restriction")) {
				jsonNode.get(InsuranceAttributeCode.AI_LOAN_PROVIDER.getCode()).get("restriction").forEach(val -> {
					returnList.add(val);
				});
			}
		}
		return CompletableFuture.completedFuture(ok(detailsJson.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, returnList)));
	}

	public Result sampleReorderProduct() {
		return ok(com.arka.consumerportal.procurementweb.views.html.product_sort.render(proputils));
	}

	public CompletionStage<Result> profileCreate() {
		ObjectNode input = (ObjectNode) request().body().asJson();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (JsonUtils.isValidField(input, "userId")) {
			mandatHP.put(SessionTrackingParams.USER_ID.getValue(), Arrays.asList(input.get("userId").asText()));
		}
		logger.debug("creating profile : " + input);
		return profileService.manageUserAccount(mandatQP, mandatHP, input).thenApplyAsync(userProfileJson -> {
			return ok(userProfileJson);
		});
	}

	public CompletionStage<Result> updateAddress() {
		ObjectNode addressInput = (ObjectNode) request().body().asJson();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (JsonUtils.isValidField(addressInput, "userId")) {
			mandatHP.put(SessionTrackingParams.USER_ID.getValue(), Arrays.asList(addressInput.get("userId").asText()));
		}
		logger.debug("creating address : " + addressInput);
		return profileService.manageUserAccount(mandatQP, mandatHP, addressInput).thenApplyAsync(respJson -> {
			return ok(respJson);
		});
	}

	public CompletionStage<Result> getAllStates() {
		Map<String, List<String>> queryString = new HashMap<>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryString.put("cname", Arrays.asList("state"));
		queryString.put("pfield", Arrays.asList("state_name"));
		queryString.put("nor", Arrays.asList("0"));
		queryString.put(CriteriaUtils.NO_OF_RECORDS.toString(), Arrays.asList(propUtils.get("nor.value")));
		queryString.putAll(mandatQP);
		JsonNode respJson = JsonUtils.newJsonObject();
		try {
			respJson = procurementServ.getStates(queryString, mandatHP).toCompletableFuture().join();
		} catch (RuntimeException e) {
			throw new ArkaRunTimeException(e);
		}
		return CompletableFuture.completedFuture(ok(respJson));
	}

	public CompletionStage<Result> schedule(String productId, String quotekey, String enquiryId) {
		ObjectNode detailsJson = Json.newObject();
		detailsJson.put(VendorQuoteConstants.ENQUIRY_RESPONSE_ID.value(), enquiryId);
		detailsJson.put(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), productId);
		detailsJson.put(VendorQuoteConstants.QUOTE_KEY.value(), quotekey);
		return CompletableFuture.completedFuture(ok(com.arka.consumerportal.procurementweb.views.html.scheduleinspection
				.render(detailsJson, proputils, detailsJson)));
	}

	public CompletionStage<Result> scheduleInspection(String proposalId) {
		ArrayNode errMsg = Json.newArray();
		Arrays.asList(Checkouterrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));

		return proposalService.getProposalById(proposalId, queryMap, null, null).thenApplyAsync(resp -> {
			return ok(com.arka.consumerportal.procurementweb.views.html.scheduleinspection.render(resp, proputils,
					errMsg));
		}, executionContext.current());
	}

	public CompletionStage<Result> lvgiCreatePolicy() {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		Session session = session();
		ObjectNode reqJson = Json.newObject();

		String proposalId = session.get(SessionTrackingParams.PROPOSAL_ID.getValue());
		reqJson.put("PremiumAmount", getCacheValue("PremiumAmount" + "." + proposalId));
		reqJson.put("ProductCode", getCacheValue("ProductCode" + "." + proposalId));
		reqJson.put("QuotationNumber", getCacheValue("QuotationNumber" + "." + proposalId));
		reqJson.put("customerID", getCacheValue("customerID" + "." + proposalId));
		reqJson.put("PaymentSource", getCacheValue("PaymentSource" + "." + proposalId));
		reqJson.put("TransactionID", getCacheValue("TransactionID" + "." + proposalId));
		reqJson.put("TPSourceName", getCacheValue("TPSourceName" + "." + proposalId));
		reqJson.put("TPEmailID", getCacheValue("TPEmailID" + "." + proposalId));
		reqJson.put("SendEmailtoCustomer", getCacheValue("SendEmailtoCustomer" + "." + proposalId));
		reqJson.put(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_CODE.value(),
				getCacheValue(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_CODE.value() + "." + proposalId));
		reqJson.put(ProductFields.PRODUCT_CODE.value(),
				getCacheValue(ProductFields.PRODUCT_CODE.value() + "." + proposalId));

		invalidLvgiCache(proposalId);
		mandatQP.put("isLvgi", Arrays.asList("true"));
		return proposalService.purchaseProposal(proposalId, mandatQP, mandatHP, reqJson).thenApplyAsync(resp -> {
			if (JsonUtils.isValidField(resp, JsonUtils.ERROR)) {
				return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
						.cancelled(proposalId, "CANCELLED"));
			}
			if (JsonUtils.isValidField(resp, "PolicyNumber")) {
				cache.getOrElse(PolicyFields.POLICY_NUMBER.getCode() + "." + proposalId, RedisDB.PURCHASE.getValue(),
						() -> resp.get("PolicyNumber").asText());
			}

			return redirect(
					com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController.productSuccess());
		});
	}

	private void invalidLvgiCache(String proposalId) {
		String dbName = RedisDB.PURCHASE.getValue();
		cache.inValidateCacheWithExactKey("PremiumAmount" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("ProductCode" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("QuotationNumber" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("customerID" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("PaymentSource" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("TransactionID" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("TPSourceName" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("TPEmailID" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey("SendEmailtoCustomer" + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(ProductFields.PRODUCT_CODE.value() + "." + proposalId, dbName);
		cache.inValidateCacheWithExactKey(
				CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_CODE.value() + "." + proposalId, dbName);

	}

    public CompletionStage<Result> buyplanByEmail(String productId, String quotekey, String enquiryId) {
        ObjectNode detailsJson = Json.newObject();
        Map<String, List<String>> oldProposalMap = CriteriaUtils.queryStringArrayToList(request().queryString());
        Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
        Map<String, List<String>> mandatHP = mandatoryParams
                .getMandatoryHeaderFromArgs(Context.current().args.entrySet());

        detailsJson.put("enquiryId", enquiryId);
        detailsJson.put("productId", productId);
        detailsJson.put("quoteKey", quotekey);

        JsonNode enquiryJson = procurementServ.getEnquiry(enquiryId, mandatQP, mandatHP).toCompletableFuture().join();
        if (enquiryJson != null && JsonUtils.isValidField(enquiryJson, "categoryId")) {
            detailsJson.put("categoryId", enquiryJson.get("categoryId").asText());
        }
        return proposalService.buyPlan(mandatQP, mandatHP, detailsJson).thenApplyAsync(resp -> {
            logger.debug("this is the response---------------" + resp);
            String proposalId = "";
            if (JsonUtils.isValidField(resp, "proposalId")) {
                proposalId = resp.get("proposalId").asText();
            }
            if (session().containsKey("KEY")) {
                session().remove("KEY");
            }
            if (oldProposalMap.containsKey("oldproposal_id") && oldProposalMap.get("oldproposal_id") != null
                    && oldProposalMap.get("oldproposal_id").get(0) != null) {
                session().put("SOURCE", "CAR_RENEW");
                session().put("OLDPROPOSALID", oldProposalMap.get("oldproposal_id").get(0).toString());
            } else {
                if (session().containsKey("SOURCE")) {
                    session().remove("SOURCE");
                    if (session().containsKey("OLDPROPOSALID")) {
                        session().remove("OLDPROPOSALID");
                    }
                }
            }
            if (StringUtils.isNotEmpty(proposalId)) {
                return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorCheckoutController
                        .checkOutPage(proposalId).url());
            } else {
                return redirect(com.arka.consumerportal.procurementweb.controllers.routes.MotorInsuranceQuoteController
                        .motorPolicy(enquiryId).url()+"?e=server.error");
            }
        }, executionContext.current());
    }
}