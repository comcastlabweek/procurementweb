/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.Json;
import play.libs.concurrent.HttpExecution;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * @author arul.p
 *
 */
public class CookieController extends Controller {

	@Inject
	HttpExecution httpexe;

	public CompletionStage<Result> setCookie() {
		JsonNode sessionData = request().body().asJson();
		if (sessionData.has("source") && sessionData.get("source").asText().equals("map")) {
			session().put(sessionData.get("key").asText(), Json.stringify(sessionData.get("value")));
		} else {
			sessionData.fields().forEachRemaining(e -> {
				session().put(e.getKey(), e.getValue().asText());
			});
		}
		return CompletableFuture.completedFuture(ok(Json.toJson(session())));
	}
}