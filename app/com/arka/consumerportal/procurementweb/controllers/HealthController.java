package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.apache.commons.lang3.ArrayUtils;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.CategoryFields;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.procurementweb.controllers.enquirybuilder.HealthEnquiryFieldBuilder;
import com.arka.consumerportal.procurementweb.services.HealthService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

public class HealthController extends Controller {

	@Inject
	PropUtils propUtils;

	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	FormFactory formFactory;
	
	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	HealthEnquiryFieldBuilder healthValidator;

	@Inject
	HealthService healthService;

	@Inject
	ProcurementServ procurementServ;

	private static final String SUM_ASSURED = "sumAssured";
	public final static String HEALTH_INSURANCE_HOME = "health_insurance_home";

	public CompletionStage<Result> health() {
		ObjectNode responseJson = Json.newObject();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		responseJson.set("errorProperties", Json.toJson(healthService.getPropertiesOfHelathErrorMsg()));
		responseJson.set("healthRestictions", healthService.getHealthHomeRestrictions());
		responseJson.set("diseases", healthService.getHealthHomeDisease());
		responseJson.set("sumAssuredList", Json.toJson(healthService.getSumAssuredList()));
		session().put("home", "health");
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(CategoryFields.CATEGORY_CODE.value(), Arrays.asList(CategoryCode.HEALTH.name()));
		queryMap.put(CriteriaUtils.PFIELD.toString(),
				Arrays.asList(CategoryFields.CATEGORY_CODE.value(), CategoryFields.CATEGORY_STATUS.value()));
		JsonNode categoryJson = vendorProdMgmtServ.getCategories(queryMap, mandatHP).toCompletableFuture().join();
		responseJson.set("healthCategory", categoryJson);
		return CompletableFuture.completedFuture(ok(com.arka.consumerportal.procurementweb.views.html.health.health_home
				.render(responseJson, propUtils, healthValidator.getHealthCookie(Context.current().request()))));
	}

	public CompletionStage<Result> createEnquiry() {

		ObjectNode responseJson = Json.newObject();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> queryMapCategory = new LinkedHashMap<>();
		queryMapCategory.put("code", Arrays.asList(CategoryCode.HEALTH.toString()));
		JsonNode categoryListNode = procurementServ.getCategoryList(queryMapCategory, mandatHP).toCompletableFuture()
				.join();

		Map<String, String[]> asFormUrlEncodedMap = request().body().asFormUrlEncoded();
		DynamicForm df = formFactory.form().bindFromRequest();
		ObjectNode enquiryParams = Json.newObject();

		healthValidator.createHealthEnquiryObject(asFormUrlEncodedMap, df, enquiryParams, new LinkedHashMap<>(),
				new LinkedList<>(), Context.current());

		if (categoryListNode != null && categoryListNode.hasNonNull("items")
				&& categoryListNode.get("items").get(0) != null
				&& categoryListNode.get("items").get(0).hasNonNull("id")) {
			enquiryParams.put("categoryId", categoryListNode.get("items").get(0).get("id").asText());
		}
		if (session("vid") != null) {
			enquiryParams.put("visitorId", session().getOrDefault("vid", ""));
		}
		if (session("uid") != null) {
			enquiryParams.put("userId", session().getOrDefault("uid", ""));
		}
		JsonNode response = procurementServ.createEnquiry(mandatQP, mandatHP, enquiryParams).toCompletableFuture()
				.join();

		if (response.hasNonNull("enquiryId")) {
			resetHealthSessionBasedOnEnquiry(response.get("enquiryId").asText(), mandatHP, mandatQP);
			session().put("home", "health");
		}
		responseJson.set("response", response);
		
		return CompletableFuture.completedFuture(ok(responseJson));

	}

	private Map<String, String> resetHealthSessionBasedOnEnquiry(String enquiryId, Map<String, List<String>> mandatHP,
			Map<String, List<String>> mandatQP) {

		if (enquiryId != null) {
			Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
			queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.CODE.value()));

			JsonNode inputParamsJson = procurementServ.getEnquiryAttributes(enquiryId, queryMap, mandatHP)
					.toCompletableFuture().join();
			return healthValidator.resetHealthSessionVariables(inputParamsJson, new LinkedHashMap<>(),
					new LinkedList<>(), healthService.getNotIssuableDiseasesIds(healthService.getHealthHomeDisease()),
					Context.current());
		}
		return Collections.emptyMap();

	}

	public CompletionStage<Result> updateHealthEnquiry() {

		DynamicForm df = formFactory.form().bindFromRequest();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		String enquiryId = df.get("enquiryId");

		ObjectNode dataJsonWithQuery = JsonUtils.newJsonObject();
		dataJsonWithQuery.put("categoryAttributeCode", HealthAttributeCode.AH_SUM_ASSURED.getCode());
		dataJsonWithQuery.put("value", df.get("medicalCover"));
		ArrayNode dataJsonarr = Json.newArray();
		dataJsonarr.add(dataJsonWithQuery);

		session().put(SUM_ASSURED, df.get("medicalCover"));

		ObjectNode patchJson = Json.newObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.ADD.value());
		patchJson.put(UpdateNames.PATH.value(), "/enquiryAttributes");
		patchJson.set(UpdateNames.VALUE.value(), dataJsonarr);
		return CompletableFuture.completedFuture(ok(procurementServ
				.updateEnquiry(enquiryId, mandatQP, mandatHP, patchJson).toCompletableFuture().thenComposeAsync(s -> {
					Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
					queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));
					return procurementServ.getEnquiry(enquiryId, queryMap, mandatHP);
				}).join()));

	}

	public CompletionStage<Result> enquiryExcludeIncludeAttr() {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		Map<String, String[]> body = request().body().asFormUrlEncoded();
		String[] exclude_attributes = body.get("exclude_attributes[]");
		String[] include_attributes = body.get("include_attributes[]");
		String[] include_tags = body.get("include_tags[]");
		String[] exclude_tags = body.get("exclude_tags[]");

		DynamicForm df = formFactory.form().bindFromRequest();
		String enquiryId = df.get("enquiryId");

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.CODE.value()));
		JsonNode enquiryJson = procurementServ.getEnquiryAttributes(enquiryId, queryMap, mandatHP).toCompletableFuture()
				.join();
		JsonNode enquiryAttributes = null;
		if (JsonUtils.isValidField(enquiryJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			enquiryAttributes = enquiryJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY);
		}

		ArrayNode patchArrJson = Json.newArray();
		if (enquiryAttributes != null && enquiryAttributes.size() > 0) {

			enquiryAttributes.elements().forEachRemaining(enquiryAttribute -> {
				if (JsonUtils.isValidField(enquiryAttribute, "code")
						&& JsonUtils.isValidField(enquiryAttribute, "id")) {
					String attributeCode = enquiryAttribute.get("code").asText();
					String enquiryAttributeId = enquiryAttribute.get("id").asText();
					ArrayNode tagArr = (ArrayNode) enquiryAttribute.get("tag");
					if (attributePresentInList(exclude_attributes, attributeCode)) {
						addPatchObject(patchArrJson, enquiryAttributeId, true);
					}
					if (attributePresentInList(include_attributes, attributeCode)) {
						addPatchObject(patchArrJson, enquiryAttributeId, false);
					}

					if (tagArr != null && tagArr.size() > 0) {
						tagArr.elements().forEachRemaining(tag -> {
							String tagElement = tag.asText();
							if (attributePresentInList(exclude_tags, tagElement)) {
								addPatchObject(patchArrJson, enquiryAttributeId, true);
							}
							if (attributePresentInList(include_tags, tagElement)) {
								addPatchObject(patchArrJson, enquiryAttributeId, false);
							}
						});
					}
				}
			});
		} else {
			queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));
			JsonNode enquiryUpdateJson = procurementServ.getEnquiry(enquiryId, queryMap, mandatHP).toCompletableFuture()
					.join();
			return CompletableFuture.completedFuture(ok(enquiryUpdateJson));
		}

		ObjectNode excludePatchJson = Json.newObject();
		excludePatchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.REPLACE.value());
		excludePatchJson.put(UpdateNames.PATH.value(), "/enquiryAttributes");
		excludePatchJson.set(UpdateNames.VALUE.value(), patchArrJson);
		return CompletableFuture
				.completedFuture(ok(procurementServ.updateEnquiry(enquiryId, mandatQP, mandatHP, excludePatchJson)
						.toCompletableFuture().thenComposeAsync(s -> {
							queryMap.put(QueryConstants.FETCH_COLUMN.value(),
									Arrays.asList(QueryConstants.PRODUCT.value()));
							return procurementServ.getEnquiry(enquiryId, queryMap, mandatHP);
						}).join()));
	}

	private void addPatchObject(ArrayNode patchArrJson, String enquiryAttributeId, boolean inputExcluded) {
		ObjectNode patchJson = patchArrJson.addObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.REPLACE.value());
		patchJson.put(UpdateNames.PATH.value(), "/enquiryAttributes/" + enquiryAttributeId + "/inputExcluded");
		patchJson.put(UpdateNames.VALUE.value(), inputExcluded);

	}

	private boolean attributePresentInList(String[] exclude_attributes, String attributeCode) {

		if (ArrayUtils.isNotEmpty(exclude_attributes)) {
			return Arrays.asList(exclude_attributes).contains(attributeCode);
		}
		return false;
	}

	public CompletionStage<Result> products() {

		String enquiryId = request().getQueryString("enquiryId");

		ObjectNode responseJson = Json.newObject();
		responseJson.set("sumAssuredList", Json.toJson(healthService.getSumAssuredList()));

		if (request().queryString().containsKey("code")) {
			responseJson.put("selectedProduct", request().getQueryString("code"));
		}
		if (request().queryString().containsKey("proposalId")) {
			responseJson.put("oldproposal_id", request().getQueryString("proposalId"));
		}
		if (request().queryString().containsKey("code")) {
			responseJson.put("product_code", request().getQueryString("code"));
		}
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();

		// Reset session when abandoned transaction re fetch occure
		Map<String, String> cookies = resetHealthSessionBasedOnEnquiry(enquiryId, mandatHP, mandatQP);

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));
		JsonNode productListJson = procurementServ.getEnquiry(enquiryId, queryMap, mandatHP).toCompletableFuture()
				.join();
		String categoryId = "";
		if (JsonUtils.isValidField(productListJson, VendorQuoteConstants.CATEGORY_RESPONSE_ID.value())) {
			categoryId = productListJson.get(VendorQuoteConstants.CATEGORY_RESPONSE_ID.value()).asText();
		}
		responseJson.set("products", productListJson);

		Map<String, List<String>> quotesRestriction = new LinkedHashMap<>();
		if (StringUtils.isNotEmpty(categoryId)) {
			quotesRestriction.put(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_ID.value(),
					Arrays.asList(categoryId));
		}
		quotesRestriction.put("tags.name", Arrays.asList("filter"));
		quotesRestriction.putAll(mandatQP);
		JsonNode healthAttributeRestictionList = procurementServ.getRestrictionForAttributes(queryMap, mandatHP)
				.toCompletableFuture().join();
		responseJson.set("quotesRestictions", healthAttributeRestictionList);
		responseJson.set("cookies", Json.toJson(cookies));
		return CompletableFuture
				.completedFuture(ok(com.arka.consumerportal.procurementweb.views.html.health.health_quotes
						.render(responseJson, propUtils)));
	}

}
