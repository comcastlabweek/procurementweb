package com.arka.consumerportal.procurementweb.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.HeaderParams;
import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.insurance.constants.InsuranceAttributeCode;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.SessionServ;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.arka.consumerportal.procurementweb.constants.CheckoutSessionKeys;
import com.arka.consumerportal.procurementweb.constants.Checkouterrors;
import com.arka.consumerportal.procurementweb.services.ProposalService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

public class HealthCheckoutController extends Controller {

	private static final Logger logger = LoggerFactory.getLogger(HealthCheckoutController.class);

	@Inject
	ProposalService proposalService;

	@Inject
	private PropUtils proputils;

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	@Inject
	private HttpExecutionContext executionContext;

	@Inject
	PropUtils propUtils;

	@Inject
	SessionServ sessionServ;

	//private static String COOKIENAME = "HEALTH_INSURANCE";

	public CompletionStage<Result> healthBuyPlan(String productId, String quotekey, String enquiryId) {
		ObjectNode detailsJson = Json.newObject();
		Map<String, List<String>> oldProposalMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		detailsJson.put("enquiryId", enquiryId);
		detailsJson.put("productId", productId);
		detailsJson.put("quoteKey", quotekey);
		JsonNode enquiryJson = null;
		try {
			enquiryJson = procurementServ.getEnquiry(enquiryId, mandatQP, mandatHP).toCompletableFuture().get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(enquiryJson != null) {
			detailsJson.put("categoryId", enquiryJson.get("categoryId").asText());
		}
		
		if (session().containsKey("HEALTH_FLOW")) {
			session().remove("HEALTH_FLOW");
		}
		if (session().containsKey("HEALTH_FLOW")
				&& session().get("HEALTH_FLOW").equalsIgnoreCase("HEALTH_ABANDONTRANSACTION")) {
			session().remove("HEALTH_FLOW");
		}

		return proposalService.buyPlan(mandatQP, mandatHP, detailsJson).thenApplyAsync(resp -> {
			if (JsonUtils.isValidField(resp, "proposalId")) {
				String proposalId = resp.get("proposalId").asText();
				logger.debug("oldproposal map------------" + oldProposalMap);
				if (oldProposalMap.containsKey("oldproposal_id") && oldProposalMap.get("oldproposal_id") != null
						&& oldProposalMap.get("oldproposal_id").get(0) != null) {
					session().put("HEALTH_FLOW", "HEALTH_RENEW");
					session().put("OLDPROPOSALID_HEALTH", oldProposalMap.get("oldproposal_id").get(0).toString());
				} else {
					if (session().containsKey("HEALTH_FLOW")) {
						session().remove("HEALTH_FLOW");
						if (session().containsKey("OLDPROPOSALID_HEALTH")) {
							session().remove("OLDPROPOSALID_HEALTH");
						}
					}
				}
				return redirect(com.arka.consumerportal.procurementweb.controllers.routes.HealthCheckoutController
						.healthCheckOutPage(proposalId).url());
			} else {
				return internalServerError();
			}
		}, executionContext.current());
	}

	public CompletionStage<Result> healthCheckOutPage(String proposalId) {
		ObjectNode details = Json.newObject();
		Map<String, String> responseToPage = new HashMap<>();
		ArrayNode errMsg = Json.newArray();
		Arrays.asList(Checkouterrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});

		Map<String, List<String>> statesqueryString = new HashMap<>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		statesqueryString.put("cname", Arrays.asList("state"));
		statesqueryString.put("pfield", Arrays.asList("state_name"));
		statesqueryString.put(CriteriaUtils.NO_OF_RECORDS.toString(), Arrays.asList(propUtils.get("nor.value")));
		statesqueryString.putAll(mandatQP);
		JsonNode respJson = JsonUtils.newJsonObject();
		try {
			respJson = procurementServ.getStates(statesqueryString, mandatHP).toCompletableFuture().join();
		} catch (RuntimeException e) {
			throw new ArkaRunTimeException(e);
		}
		JsonNode stateResp = anyCollectionQueryResponseValidator.anyCollectionQueryGetData(respJson);
		if (stateResp.size() > 0) {
			details.set("states", stateResp);
		}
		Map<String, List<String>> relationshipqueryString = new HashMap<>();
		relationshipqueryString.put("code", Arrays.asList(InsuranceAttributeCode.AI_NOMINEE_RELATIONSHIP.getCode()));
		relationshipqueryString.putAll(mandatQP);
		JsonNode relationshiprespJson = JsonUtils.newJsonObject();
		try {
			relationshiprespJson = procurementServ.getRestrictionForAttributes(relationshipqueryString, mandatHP)
					.toCompletableFuture().join();
		} catch (RuntimeException e) {
			throw new ArkaRunTimeException(e);
		}
		details.set("relationships", relationshiprespJson);
		
		/*
		Map<String, List<String>> medicalHistory = new HashMap<>();
		medicalHistory.put("code", Arrays.asList(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()));
		JsonNode medicalQuestions = JsonUtils.newJsonArray();
		try {
			medicalQuestions = procurementServ.getRestrictionForAttributes(medicalHistory, mandatHP)
					.toCompletableFuture().join();
		} catch (RuntimeException e) {
			throw new ArkaRunTimeException(e);
		}
		
		
		if (JsonUtils.isValidField(medicalQuestions, HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode())
				&& JsonUtils.isValidField(medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()),
						"restriction")) {
			medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()).get("restriction")
					.get("constraint_value").forEach(questions -> {
						JsonNode medicalcollection = JsonUtils.newJsonArray();
						try {
							medicalcollection = procurementServ
									.getRestrictionForCollection(questions.get("collection").asText(), mandatQP,
											mandatHP)
									.toCompletableFuture().join();
						} catch (RuntimeException e) {
							throw new ArkaRunTimeException(e);
						}
						details.set("MedicalQuestions", medicalcollection);
					});
		}
		*/
		Map<String, List<String>> occupation = new HashMap<>();
		occupation.put("code", Arrays.asList(HealthAttributeCode.AI_OCCUPATION.getCode()));
		JsonNode occupationJson = JsonUtils.newJsonArray();
		try {
			occupationJson = procurementServ.getRestrictionForAttributes(occupation, mandatHP).toCompletableFuture()
					.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new ArkaRunTimeException(e);
		}
		details.set("occupationList", occupationJson);
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));
		
		/* setting login */
		if (session().containsKey("uid") && session().get("uid") != null) {
			session().put("LOGGEDINUSER", "true");
		}
		
		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {

			logger.debug("response from procurement serv : " + resp);
			details.set("response", resp);
			
			
			Map<String, List<String>> medicalHistory = new HashMap<>();
			medicalHistory.put("code", Arrays.asList(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()));
			JsonNode medicalQuestions = JsonUtils.newJsonArray();
			try {
				medicalQuestions = procurementServ.getRestrictionForAttributes(medicalHistory, mandatHP)
						.toCompletableFuture().join();
			} catch (RuntimeException e) {
				throw new ArkaRunTimeException(e);
			}
			
			
			if (JsonUtils.isValidField(medicalQuestions, HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode())
					&& JsonUtils.isValidField(medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()),
							"restriction")) {
				medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()).get("restriction")
						.get("constraint_value").forEach(questions -> {
							JsonNode medicalcollection = JsonUtils.newJsonArray();
							try {
								String productCode = "None";
								if(resp.get("productId") != null) {
									productCode = resp.get("productId").asText();
								}
								medicalcollection = procurementServ
										.getMedicalQuestions(questions.get("collection").asText(), productCode, mandatQP, mandatHP)
										.toCompletableFuture().join();
							} catch (RuntimeException e) {
								throw new ArkaRunTimeException(e);
							}
							details.set("MedicalQuestions", medicalcollection);
						});
			}
			
			
			/* logic for adandon Transaction*/
			if (session().containsKey("SOURCE")
					&& session().get("SOURCE").toString().equalsIgnoreCase("HEALTH_ABANDONTRANSACTION")) {
				if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())) {
					Map<String, String> cookie = new HashMap<>();
					resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).elements().forEachRemaining(action -> {
						if (action.get("code").asText().equalsIgnoreCase(InsuranceAttributeCode.AI_MARITAL.getCode())) {
							if (action.get("value") != null) {
								cookie.put(
										CheckoutSessionKeys.HEALTH_KEY.getCode()
												+ InsuranceAttributeCode.AI_MARITAL.getCode() + "self",
										action.get("value").asText());
								responseToPage.putAll(cookie);
							}
						}
					});
				}
				ObjectNode newResp = (ObjectNode) resp;
				responseToPage.clear();
				responseToPage.putAll(settingValuesFromProposalParams(newResp));
				responseToPage.put("VARDISABLED", "REPLACEDBYABANDONTRANSACTION");
			} else {
				if (JsonUtils.isValidField(resp, VendorQuoteConstants.PROPOSAL_PARAMS.value())
						&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.PROPOSAL_PARAMS.value()), 0)) {
					Map<String, String> data = new HashMap<>();
					if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())) {
						resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).elements().forEachRemaining(action -> {
							if (action.get("code").asText()
									.equalsIgnoreCase(InsuranceAttributeCode.AI_MARITAL.getCode())) {
								if (action.get("value") != null) {
									data.put(
											CheckoutSessionKeys.HEALTH_KEY.getCode()
													+ InsuranceAttributeCode.AI_MARITAL.getCode() + "self",
											action.get("value").asText());
								}
							}
						});
					}
					responseToPage.putAll(settingAttributesInSession(resp, data));
					responseToPage.put("VARDISABLED", "REPLACEDBYPROPOSAL");
				} else {
					if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())
							&& JsonUtils.isValidIndex(resp.get(VendorQuoteConstants.INPUT_QUOTE.value()), 0)) {
						Map<String, String> cookie = new HashMap<>();
						resp.get(VendorQuoteConstants.INPUT_QUOTE.value()).elements().forEachRemaining(action -> {
							if (JsonUtils.isValidField(action, "code") && JsonUtils.isValidField(action, "value")) {
								if (action.get("code").asText()
										.equalsIgnoreCase(InsuranceAttributeCode.AI_MARITAL.getCode())) {
									if (action.get("value") != null) {
										cookie.put(
												CheckoutSessionKeys.HEALTH_KEY.getCode()
														+ InsuranceAttributeCode.AI_MARITAL.getCode() + "self",
												action.get("value").asText());
									}
								}
								if (action.get("code").asText().equals("AH-PINCODE_SELF_OR_FAMILY")) {
									Map<String, List<String>> gettingData = new HashMap<>();
									gettingData.put("_id", Arrays.asList(action.get("value").asText()));
									ObjectNode postalCodeEntered = JsonUtils.newJsonObject();
									postalCodeEntered = (ObjectNode) procurementServ
											.getPostalCodes(gettingData, mandatHP).toCompletableFuture().join();
									responseToPage.putAll(settingPostalCodeInSession(postalCodeEntered, cookie));
									responseToPage.put("VARDISABLED", "USERGIVENDATA");
								} else {
									responseToPage
											.putAll(settingPostalCodeInSession(JsonUtils.newJsonObject(), cookie));
								}
							}
						});
					}
				}
			}

			// logic for building insured memebers
			if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())) {
				details.setAll(buildInsuredMembers(resp.get(VendorQuoteConstants.INPUT_QUOTE.value())));
			}

			/* logic for renewal flow */
			if (session().containsKey("HEALTH_FLOW") && StringUtils.isNotEmpty(session().get("HEALTH_FLOW"))) {
				if (session().get("HEALTH_FLOW").toString().equalsIgnoreCase("HEALTH_RENEW")
						&& session().containsKey("OLDPROPOSALID_HEALTH")
						&& StringUtils.isNotEmpty(session().get("OLDPROPOSALID_HEALTH"))) {
					String oldId = session().get("OLDPROPOSALID_HEALTH").toString();
					ObjectNode oldJson = (ObjectNode) proposalService.getProposalById(oldId, queryMap, mandatHP, null)
							.toCompletableFuture().join();
					logger.debug(
							"this is the map for renewal --------------" + settingValuesFromProposalParams(oldJson));
					responseToPage.putAll(settingValuesFromProposalParams(oldJson));
					String nomineeAgeKey = CheckoutSessionKeys.HEALTH_KEY.getCode()
							+ InsuranceAttributeCode.AI_NOMINEE_AGE.getCode();
					if (responseToPage.containsKey(nomineeAgeKey)
							&& StringUtils.isNumeric(responseToPage.get(nomineeAgeKey))) {
						Integer parseInt = Integer.parseInt(responseToPage.get(nomineeAgeKey)) + 1;
						responseToPage.put(nomineeAgeKey, parseInt.toString());
					}
					responseToPage.put("VARDISABLED", "REPLACEFORHEALTHRENEWALDATA");
				}
			}

			/* logic for users logined user */
			if (session().containsKey("LOGGEDINUSER") && session().get("LOGGEDINUSER").equalsIgnoreCase("true")
					&& session().containsKey("uid") && session().get("uid") != null) {
				ObjectNode getUser = JsonUtils.newJsonObject();
				getUser.put("source", "getuser");
				mandatHP.put(HeaderParams.USER_ID.getValue(), Arrays.asList(session().get("uid")));
				ObjectNode userResponse = (ObjectNode) sessionServ.getUser(mandatQP, mandatHP, getUser)
						.toCompletableFuture().join();
				if (responseToPage.containsKey("VARDISABLED")
						&& (responseToPage.get("VARDISABLED").equalsIgnoreCase("REPLACEDBYABANDONTRANSACTION")
								|| responseToPage.get("VARDISABLED").equalsIgnoreCase("REPLACEFORHEALTHRENEWALDATA"))) {
					responseToPage.putAll(userDataForOtherFlow(userResponse));
				} else {
					if (responseToPage.containsKey("VARDISABLED")
							&& responseToPage.get("VARDISABLED").equalsIgnoreCase("USERGIVENDATA")) {
						responseToPage.putAll(userGivenData(userResponse));
					} else {
						responseToPage.putAll(userDataToMap(userResponse));
					}
				}
			}

			return ok(com.arka.consumerportal.procurementweb.views.html.health.healthcheckoutpage.render(details,
					proputils, responseToPage, errMsg));
		}, executionContext.current());
	}

	private ObjectNode buildInsuredMembers(JsonNode inputQuoteParams) {
		ObjectNode insuredMembers = Json.newObject();
		inputQuoteParams.elements().forEachRemaining(inputQuoteParam -> {
			if (JsonUtils.isValidField(inputQuoteParam, "tag")) {
				ArrayNode tagListArr = (ArrayNode) inputQuoteParam.get("tag");
				logger.debug("this is the iteration" + tagListArr);
				if (JsonUtils.arrayContains(tagListArr, "self")) {
					if (!insuredMembers.hasNonNull("self")) {
						insuredMembers.putObject("self");
					}
					ObjectNode selfJson = (ObjectNode) insuredMembers.get("self");
					if (JsonUtils.arrayContains(tagListArr, "age")) {
						selfJson.put("age", inputQuoteParam.get("value").asText());
					}
					if (JsonUtils.arrayContains(tagListArr, "gender")) {
						selfJson.put("gender", inputQuoteParam.get("value").asText());
					}
				}
				if (JsonUtils.arrayContains(tagListArr, "spouse") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode spouseJson = insuredMembers.putObject("spouse");
					spouseJson.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "son_1") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode son1Json = insuredMembers.putObject("son 1");
					son1Json.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "son_2") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode son2Json = insuredMembers.putObject("son 2");
					son2Json.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "son_3") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode son3Json = insuredMembers.putObject("son 3");
					son3Json.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "daughter_1") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode daughter1Json = insuredMembers.putObject("daughter 1");
					daughter1Json.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "daughter_2") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode daughter2Json = insuredMembers.putObject("daughter 2");
					daughter2Json.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "daughter_3") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode daughter3Json = insuredMembers.putObject("daughter 3");
					daughter3Json.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "father") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode fatherJson = insuredMembers.putObject("father");
					fatherJson.put("age", inputQuoteParam.get("value").asText());
				}
				if (JsonUtils.arrayContains(tagListArr, "mother") && JsonUtils.arrayContains(tagListArr, "age")) {
					ObjectNode motherJson = insuredMembers.putObject("mother");
					motherJson.put("age", inputQuoteParam.get("value").asText());
				}
			}
		});
		return insuredMembers;
	}

	public CompletionStage<Result> healthCheckOutSummary(String proposalId) {
		ObjectNode details = Json.newObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		
		
		/*
		Map<String, List<String>> medicalHistory = new HashMap<>();
		medicalHistory.put("code", Arrays.asList(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()));
		JsonNode medicalQuestions = JsonUtils.newJsonArray();
		try {
			medicalQuestions = procurementServ.getRestrictionForAttributes(medicalHistory, mandatHP)
					.toCompletableFuture().join();
		} catch (RuntimeException e) {
			throw new ArkaRunTimeException(e);
		}
		
		
		
		if (JsonUtils.isValidField(medicalQuestions, HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode())
				&& JsonUtils.isValidField(medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()),
						"restriction")) {
			medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()).get("restriction")
					.get("constraint_value").forEach(questions -> {
						JsonNode medicalcollection = JsonUtils.newJsonArray();
						try {
							mandatQP.put();
							medicalcollection = procurementServ
									.getRestrictionForCollection(questions.get("collection").asText(), mandatQP,
											mandatHP)
									.toCompletableFuture().join();
						} catch (RuntimeException e) {
							throw new ArkaRunTimeException(e);
						}
						details.set("MedicalQuestions", medicalcollection);
					});
		}
		*/

		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));

		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			
			
			logger.debug("response from procurement serv proposalService: " + resp);
			
			
			Map<String, List<String>> medicalHistory = new HashMap<>();
			medicalHistory.put("code", Arrays.asList(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()));
			JsonNode medicalQuestions = JsonUtils.newJsonArray();
			try {
				medicalQuestions = procurementServ.getRestrictionForAttributes(medicalHistory, mandatHP)
						.toCompletableFuture().join();
			} catch (RuntimeException e) {
				throw new ArkaRunTimeException(e);
			}
			
			
			if (JsonUtils.isValidField(medicalQuestions, HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode())
					&& JsonUtils.isValidField(medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()),
							"restriction")) {
				medicalQuestions.get(HealthAttributeCode.AH_MEDICAL_QUESTIONS.getCode()).get("restriction")
						.get("constraint_value").forEach(questions -> {
							JsonNode medicalcollection = JsonUtils.newJsonArray();
							try {
								String productCode = "None";
								if(resp.get("productId") != null) {
									productCode = resp.get("productId").asText();
								}
								medicalcollection = procurementServ
										.getMedicalQuestions(questions.get("collection").asText(), productCode, mandatQP, mandatHP)
										.toCompletableFuture().join();
							} catch (RuntimeException e) {
								throw new ArkaRunTimeException(e);
							}
							details.set("MedicalQuestions", medicalcollection);
						});
			}
			
			
			
			if (JsonUtils.isValidField(resp, VendorQuoteConstants.INPUT_QUOTE.value())) {
				details.setAll(buildInsuredMembers(resp.get(VendorQuoteConstants.INPUT_QUOTE.value())));
			}
			return ok(com.arka.consumerportal.procurementweb.views.html.health.healthcheckoutsummary.render(resp,
					details, proputils));
		}, executionContext.current());
	}

	public CompletionStage<Result> healthAbandonTransaction(String proposalId) {
		ArrayNode errMsg = Json.newArray();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Arrays.asList(Checkouterrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.ALL.value()));

		return proposalService.getProposalById(proposalId, queryMap, mandatHP, null).thenApplyAsync(resp -> {
			session().put("HEALTH_FLOW", "HEALTH_ABANDONTRANSACTION");
			return redirect(com.arka.consumerportal.procurementweb.controllers.routes.HealthCheckoutController
					.healthCheckOutPage(proposalId).url());
		}, executionContext.current());
	}

	private Map<String, String> userDataForOtherFlow(ObjectNode response) {
		Map<String, String> returnMap = new HashMap<>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		if (JsonUtils.isValidField(response, "loginNames") && JsonUtils.isValidIndex(response.get("loginNames"), 0)) {
			response.get("loginNames").forEach(action -> {
				if (JsonUtils.isValidField(action, "type") && JsonUtils.isValidField(action, "loginName")) {
					if (action.get("type").asText().equalsIgnoreCase("EMAIL")) {
						returnMap.put(
								CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_EMAIL.getCode(),
								action.get("loginName").asText());
					}
					if (action.get("type").asText().equalsIgnoreCase("MOBILE_NUMBER")) {
						returnMap.put(
								CheckoutSessionKeys.HEALTH_KEY.getCode()
										+ InsuranceAttributeCode.AI_MOBILE_NUMBER.getCode(),
								action.get("loginName").asText());
					}
				}
			});
		}

		if (JsonUtils.isValidField(response, "user_address")
				&& JsonUtils.isValidField(response.get("user_address"), "place")
				&& JsonUtils.isValidField(response.get("user_address"), "city")
				&& JsonUtils.isValidField(response.get("user_address"), "postalCode")
				&& JsonUtils.isValidField(response.get("user_address"), "countryCode")
				&& JsonUtils.isValidField(response.get("user_address"), "state")
				&& JsonUtils.isValidField(response.get("user_address"), "addressLine1")) {
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_PLACE.getCode(),
					response.get("user_address").get("place").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_ADDRESS_LINE_1.getCode(),
					response.get("user_address").get("addressLine1").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_COUNTRY_CODE.getCode(),
					response.get("user_address").get("countryCode").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_PINCODE.getCode(),
					response.get("user_address").get("postalCode").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_DISTRICT.getCode(),
					response.get("user_address").get("city").asText());
			if (response.get("user_address").get("countryCode").asText().equalsIgnoreCase("IN")) {
				returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_COUNTRY.getCode(),
						"India");
			}
			if (JsonUtils.isValidField(response.get("user_address"), "addressLine2")) {
				returnMap.put(
						CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_ADDRESS_LINE_2.getCode(),
						response.get("user_address").get("addressLine2").asText());
			}
			if (JsonUtils.isValidField(response.get("user_address").get("state"), "id")) {
				returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE_ID.getCode(),
						response.get("user_address").get("state").get("id").asText());
				String countryCode="IN";
				JsonNode stateNameRes = procurementServ
						.getStateById(response.get("user_address").get("state").get("id").asText(), countryCode, mandatQP, mandatHP)
						.toCompletableFuture().join();
				if (JsonUtils.isValidField(stateNameRes, "state_name")) {
					returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE.getCode(),
							stateNameRes.get("state_name").asText());
				}
			}
		}
		return returnMap;
	}

	private Map<String, String> userGivenData(ObjectNode response) {
		Map<String, String> returnMap = new HashMap<>();
		if (JsonUtils.isValidField(response, "profile")) {
			if (JsonUtils.isValidField(response.get("profile"), "isCompany")
					&& response.get("profile").get("isCompany").asText().equalsIgnoreCase("false")) {
				if (JsonUtils.isValidField(response.get("profile"), "firstName")
						&& JsonUtils.isValidField(response.get("profile"), "lastName")) {
					returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode()
							+ InsuranceAttributeCode.AI_FIRST_NAME.getCode() + "self",
							response.get("profile").get("firstName").asText());
					returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode()
							+ InsuranceAttributeCode.AI_LAST_NAME.getCode() + "self",
							response.get("profile").get("lastName").asText());
				}
			}
			if (JsonUtils.isValidField(response.get("profile"), "dob")) {
				returnMap.put(
						CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_DOB.getCode() + "self",
						response.get("profile").get("dob").asText());
			}
			if (JsonUtils.isValidField(response.get("profile"), "gender")) {
				returnMap.put(
						CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_GENDER.getCode() + "self",
						response.get("profile").get("gender").asText());
			}
		}
		if (JsonUtils.isValidField(response, "loginNames") && JsonUtils.isValidIndex(response.get("loginNames"), 0)) {
			response.get("loginNames").forEach(action -> {
				if (JsonUtils.isValidField(action, "type") && JsonUtils.isValidField(action, "loginName")) {
					if (action.get("type").asText().equalsIgnoreCase("EMAIL")) {
						returnMap.put(
								CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_EMAIL.getCode(),
								action.get("loginName").asText());
					}
					if (action.get("type").asText().equalsIgnoreCase("MOBILE_NUMBER")) {
						returnMap.put(
								CheckoutSessionKeys.HEALTH_KEY.getCode()
										+ InsuranceAttributeCode.AI_MOBILE_NUMBER.getCode(),
								action.get("loginName").asText());
					}
				}
			});
		}
		return returnMap;
	}

	private Map<String, String> userDataToMap(ObjectNode response) {
		Map<String, String> returnMap = new HashMap<>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		if (JsonUtils.isValidField(response, "profile")) {
			if (JsonUtils.isValidField(response.get("profile"), "isCompany")
					&& response.get("profile").get("isCompany").asText().equalsIgnoreCase("false")) {
				if (JsonUtils.isValidField(response.get("profile"), "firstName")
						&& JsonUtils.isValidField(response.get("profile"), "lastName")) {
					returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode()
							+ InsuranceAttributeCode.AI_FIRST_NAME.getCode() + "self",
							response.get("profile").get("firstName").asText());
					returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode()
							+ InsuranceAttributeCode.AI_LAST_NAME.getCode() + "self",
							response.get("profile").get("lastName").asText());
				}
			}
			if (JsonUtils.isValidField(response.get("profile"), "dob")) {
				returnMap.put(
						CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_DOB.getCode() + "self",
						response.get("profile").get("dob").asText());
			}
			if (JsonUtils.isValidField(response.get("profile"), "gender")) {
				returnMap.put(
						CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_GENDER.getCode() + "self",
						response.get("profile").get("gender").asText());
			}
		}
		if (JsonUtils.isValidField(response, "loginNames") && JsonUtils.isValidIndex(response.get("loginNames"), 0)) {
			response.get("loginNames").forEach(action -> {
				if (JsonUtils.isValidField(action, "type") && JsonUtils.isValidField(action, "loginName")) {
					if (action.get("type").asText().equalsIgnoreCase("EMAIL")) {
						returnMap.put(
								CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_EMAIL.getCode(),
								action.get("loginName").asText());
					}
					if (action.get("type").asText().equalsIgnoreCase("MOBILE_NUMBER")) {
						returnMap.put(
								CheckoutSessionKeys.HEALTH_KEY.getCode()
										+ InsuranceAttributeCode.AI_MOBILE_NUMBER.getCode(),
								action.get("loginName").asText());
					}
				}
			});
		}
		if (JsonUtils.isValidField(response, "user_address")
				&& JsonUtils.isValidField(response.get("user_address"), "place")
				&& JsonUtils.isValidField(response.get("user_address"), "city")
				&& JsonUtils.isValidField(response.get("user_address"), "postalCode")
				&& JsonUtils.isValidField(response.get("user_address"), "countryCode")
				&& JsonUtils.isValidField(response.get("user_address"), "state")
				&& JsonUtils.isValidField(response.get("user_address"), "addressLine1")) {
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_PLACE.getCode(),
					response.get("user_address").get("place").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_ADDRESS_LINE_1.getCode(),
					response.get("user_address").get("addressLine1").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_COUNTRY_CODE.getCode(),
					response.get("user_address").get("countryCode").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_PINCODE.getCode(),
					response.get("user_address").get("postalCode").asText());
			returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_DISTRICT.getCode(),
					response.get("user_address").get("city").asText());
			if (response.get("user_address").get("countryCode").asText().equalsIgnoreCase("IN")) {
				returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_COUNTRY.getCode(),
						"India");
			}
			if (JsonUtils.isValidField(response.get("user_address"), "addressLine2")) {
				returnMap.put(
						CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_ADDRESS_LINE_2.getCode(),
						response.get("user_address").get("addressLine2").asText());
			}
			if (JsonUtils.isValidField(response.get("user_address").get("state"), "id")) {
				returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE_ID.getCode(),
						response.get("user_address").get("state").get("id").asText());
				String countryCode="IN";
				JsonNode stateNameRes = procurementServ
						.getStateById(response.get("user_address").get("state").get("id").asText(), countryCode, mandatQP, mandatHP)
						.toCompletableFuture().join();
				if (JsonUtils.isValidField(stateNameRes, "state_name")) {
					returnMap.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE.getCode(),
							stateNameRes.get("state_name").asText());
				}
			}
		}
		return returnMap;
	}

	public CompletionStage<Result> healthSaveProposal(String proposalId) {
		ArrayNode jsonInput = (ArrayNode) request().body().asJson();
		ArrayNode savingattributes = JsonUtils.newJsonArray();
		List<String> deleteattributes = new ArrayList<String>();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		jsonInput.elements().forEachRemaining(action -> {
			if (JsonUtils.isValidField(action, "value") && StringUtils.isBlank(action.get("value").asText())) {
				if (JsonUtils.isValidField(action, VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value())) {
					deleteattributes
							.add(action.get(VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value()).asText());
				}
			} else {
				savingattributes.add(action);
			}
		});
		deleteProposalAttributes(proposalId, deleteattributes);
		ObjectNode patchJson = Json.newObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.REPLACE.value());
		patchJson.put(UpdateNames.PATH.value(), "/proposalAttributes");
		patchJson.set(UpdateNames.VALUE.value(), savingattributes);

		return proposalService.updateProposal(proposalId, mandatQP, mandatHP, patchJson).thenApplyAsync(resp -> {
			return ok(resp);
		}, executionContext.current());
	}

	private CompletionStage<Result> deleteProposalAttributes(String proposalId, List<String> attributes) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		mandatQP.put("code", attributes);
		return proposalService.deleteProposalAttribute(proposalId, mandatQP, mandatHP).thenApplyAsync(resp -> {
			return ok(resp);
		}, executionContext.current());

	}

	private Map<String, String> settingValuesFromProposalParams(ObjectNode sessionJson) {
		Map<String, String> cookie = new HashMap<>();
		if (JsonUtils.isValidField(sessionJson, VendorQuoteConstants.PROPOSAL_PARAMS.value())
				&& sessionJson.get(VendorQuoteConstants.PROPOSAL_PARAMS.value()).size() >= 1) {
			ArrayNode values = (ArrayNode) sessionJson.get(VendorQuoteConstants.PROPOSAL_PARAMS.value());
			values.forEach(action -> {
				if (JsonUtils.isValidField(action, "code")
						&& action.get("code").asText().equals("AH-MEDICAL_QUESTIONS")) {
					logger.debug("do not store it in session");
				} else {
					if (JsonUtils.isValidField(action, "code") && JsonUtils.isValidField(action, "value")) {
						if (action.has("tag") && action.hasNonNull("tag") && action.get("tag").size() > 0) {
							action.get("tag").forEach(ac -> {
								if (JsonUtils.isValidField(action, VendorQuoteConstants.ADDITIONAL_VALUE.value())) {
									cookie.put(
											CheckoutSessionKeys.HEALTH_KEY.getCode() + action.get("code").asText() + "_"
													+ action.get("value").asText() + ac.asText(),
											action.get(VendorQuoteConstants.ADDITIONAL_VALUE.value()).asText());
								} else {
									cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + action.get("code").asText()
											+ ac.asText(), action.get("value").asText());
								}
							});
						} else {
							if (JsonUtils.isValidField(action, VendorQuoteConstants.ADDITIONAL_VALUE.value())) {
								cookie.put(
										CheckoutSessionKeys.HEALTH_KEY.getCode() + action.get("code").asText() + "_"
												+ action.get("value").asText(),
										action.get(VendorQuoteConstants.ADDITIONAL_VALUE.value()).asText());
							} else {
								cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + action.get("code").asText(),
										action.get("value").asText());
							}
						}
					}
				}
			});
		}
		return cookie;
	}

	private Map<String, String> settingPostalCodeInSession(ObjectNode data, Map<String, String> marital) {
		Map<String, String> cookie = new HashMap<>();
		if (!marital.isEmpty() && marital.containsKey(
				CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_MARITAL.getCode() + "self")) {
			cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_MARITAL.getCode() + "self",
					marital.get(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_MARITAL.getCode()
							+ "self"));
		}
		if (JsonUtils.isValidField(data, "items") && JsonUtils.isValidIndex(data.get("items"), 0)) {
			data.get("items").elements().forEachRemaining(city -> {
				if (JsonUtils.isValidField(city, "_id") && JsonUtils.isValidField(city, "place")
						&& JsonUtils.isValidField(city, "postal_code") && JsonUtils.isValidField(city, "district")
						&& JsonUtils.isValidField(city, "state") && JsonUtils.isValidField(city, "state_id")
						&& JsonUtils.isValidField(city, "country_name")
						&& JsonUtils.isValidField(city, "country_code")) {
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE.getCode(),
							city.get("state").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE_ID.getCode(),
							city.get("state_id").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_PLACE.getCode(),
							city.get("place").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_DISTRICT.getCode(),
							city.get("district").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_COUNTRY.getCode(),
							city.get("country_name").asText());
					cookie.put(
							CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_COUNTRY_CODE.getCode(),
							city.get("country_code").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_PINCODE.getCode(),
							city.get("postal_code").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_DISTRICT_CODE.getCode(),
							city.get("district_code").asText());
					cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_STATE_CODE.getCode(),
							city.get("state_code").asText());
				}
				;
			});
		}
		return cookie;
	}

	private Map<String, String> settingAttributesInSession(JsonNode resp, Map<String, String> marital) {
		Map<String, String> cookie = new HashMap<>();
		if (!marital.isEmpty() && marital.containsKey(
				CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_MARITAL.getCode() + "self")) {
			cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_MARITAL.getCode() + "self",
					marital.get(CheckoutSessionKeys.HEALTH_KEY.getCode() + InsuranceAttributeCode.AI_MARITAL.getCode()
							+ "self"));
		}
		ArrayNode sessionJson = (ArrayNode) resp.get(VendorQuoteConstants.PROPOSAL_PARAMS.value());
		if (JsonUtils.isValidIndex(sessionJson, 0)) {
			sessionJson.forEach(action -> {
				if (JsonUtils.isValidField(action, VendorQuoteConstants.PROPOSAL_PARAMS.value()) && action
						.get(VendorQuoteConstants.CODE.value()).asText().equalsIgnoreCase("AH-MEDICAL_QUESTIONS")) {
					logger.debug("these are not to be stored in session",
							action.get(VendorQuoteConstants.CODE.value()).asText());
				} else {
					if (action.has(VendorQuoteConstants.CODE.value())
							&& action.hasNonNull(VendorQuoteConstants.CODE.value()) && action.has("value")) {
						if (action.has("tag") && action.hasNonNull("tag") && action.get("tag").size() > 0) {
							action.get("tag").forEach(ac -> {

								if (JsonUtils.isValidField(action, VendorQuoteConstants.ADDITIONAL_VALUE.value())) {
									cookie.put(
											CheckoutSessionKeys.HEALTH_KEY.getCode()
													+ action.get(VendorQuoteConstants.CODE.value()).asText() + "_"
													+ action.get("value").asText() + ac.asText(),
											action.get(VendorQuoteConstants.ADDITIONAL_VALUE.value()).asText());
								} else {
									cookie.put(CheckoutSessionKeys.HEALTH_KEY.getCode()
											+ action.get(VendorQuoteConstants.CODE.value()).asText() + ac.asText(),
											action.get("value").asText());
								}
							});
						} else {
							if (JsonUtils.isValidField(action, VendorQuoteConstants.ADDITIONAL_VALUE.value())) {
								cookie.put(
										CheckoutSessionKeys.HEALTH_KEY.getCode()
												+ action.get(VendorQuoteConstants.CODE.value()).asText() + "_"
												+ action.get("value").asText(),
										action.get(VendorQuoteConstants.ADDITIONAL_VALUE.value()).asText());
							} else {
								cookie.put(
										CheckoutSessionKeys.HEALTH_KEY.getCode()
												+ action.get(VendorQuoteConstants.CODE.value()).asText(),
										action.get("value").asText());
							}
						}
					}
				}
			});
		}
		return cookie;
	}

}
