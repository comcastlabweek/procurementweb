package com.arka.consumerportal.procurementweb.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.arka.common.constants.HeaderParams;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.productmgmt.constants.CategoryFields;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.SessionServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.travel.constants.TravelAttributeCode;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.CookieUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.Validation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Result;
import play.mvc.WebSocket;

public class TravelController extends Controller {

	@Inject
	PropUtils propUtils;

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	ActorSystem actorSystem;

	@Inject
	Materializer materializer;

	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	SessionServ sessionServ;

	@Inject
	FormFactory formFactory;

	private static final Integer INITIAL_MEDICAL_COVER = 50000;

	public CompletionStage<Result> travel() {

		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryString = new LinkedHashMap<>();
		queryString.put(CategoryFields.CATEGORY_CODE.value(), Arrays.asList(CategoryCode.TRAVEL.name()));
		queryString.put(CriteriaUtils.PFIELD.toString(),
				Arrays.asList(CategoryFields.CATEGORY_CODE.value(), CategoryFields.CATEGORY_STATUS.value()));
		session().put("home", "travel");
		JsonNode responseJson = vendorProdMgmtServ.getCategories(queryString, mandatHP).toCompletableFuture().join();
		return CompletableFuture.completedFuture(ok(
				com.arka.consumerportal.procurementweb.views.html.travel.travel_home.render(responseJson, propUtils)));

	}

	public Map<String, List<String>> travelHomeRestriction() {
		Map<String, List<String>> queryString = new LinkedHashMap<>();
		queryString.put("code", Arrays.asList(TravelAttributeCode.ATR_TRAVEL_START_DATE.getCode(),
				TravelAttributeCode.ATR_TRAVEL_END_DATE.getCode(), TravelAttributeCode.ATR_TRAVEL_END_DATE.getCode(),
				TravelAttributeCode.ATR_IS_TRIP_STARTING_FROM_INDIA.getCode(),
				TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode(),
				TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode(),
				TravelAttributeCode.ATR_TRAVELLER_DESTINATION.getCode(),
				TravelAttributeCode.ATR_TRAVEL_INSURANCE_TYPE.getCode(), TravelAttributeCode.ATR_SELF_AGE.getCode(),
				TravelAttributeCode.ATR_SPOUSE_AGE.getCode(), TravelAttributeCode.ATR_FATHER_AGE.getCode(),
				TravelAttributeCode.ATR_MOTHER_AGE.getCode(), TravelAttributeCode.ATR_SON_AGE.getCode(),
				TravelAttributeCode.ATR_DAUGHTER_AGE.getCode(), TravelAttributeCode.ATR_IS_ANNUAL_MULTI_TRIP.getCode(),
				TravelAttributeCode.ATR_TRIP_FREQUENCY.getCode()

		));
		return queryString;

	}

	public Map<String, List<String>> travelPreExistingRestriction() {
		Map<String, List<String>> queryString = new LinkedHashMap<>();
		// queryString.put("code",
		// Arrays.asList(TravelAttributeCode.ATR_PRE_EXISITING_DISEASES.getCode()));
		return queryString;
	}

	public CompletionStage<Result> createEnquiry() {

		ObjectNode enquiryRequestJson = Json.newObject();
		ObjectNode responseJson = Json.newObject();
		Map<String, String[]> asFormUrlEncodedMap = request().body().asFormUrlEncoded();
		DynamicForm df = formFactory.form().bindFromRequest();

		List<String> countries = new ArrayList<>();
		List<String> sonsAge = new ArrayList<>();
		List<String> daughtersAge = new ArrayList<>();
		List<String> travellersAge = new ArrayList<>();
		List<String> studentsAge = new ArrayList<>();

		String[] sonsAgeArray = asFormUrlEncodedMap.get("sonAge");
		String[] daughtersAgeArray = asFormUrlEncodedMap.get("daughterAge");
		String[] travellersAgeArray = asFormUrlEncodedMap.get("travellerAge");
		String[] studentsAgeArray = asFormUrlEncodedMap.get("studentAge");

		String[] countriesArray = asFormUrlEncodedMap.get("visitingCountries");
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryMapCategory = new LinkedHashMap<>();
		queryMapCategory.put("code", Arrays.asList(CategoryCode.TRAVEL.toString()));
		JsonNode categoryListNode = procurementServ.getCategoryList(queryMapCategory, mandatHP).toCompletableFuture()
				.join();

		if (countriesArray != null) {
			countries = Arrays.asList(countriesArray);
		}
		if (sonsAgeArray != null) {
			sonsAge = Arrays.asList(sonsAgeArray);
		}
		if (daughtersAgeArray != null) {
			daughtersAge = Arrays.asList(daughtersAgeArray);
		}
		if (travellersAgeArray != null) {
			travellersAge = Arrays.asList(travellersAgeArray);
		}

		if (studentsAgeArray != null) {
			studentsAge = Arrays.asList(studentsAgeArray);
		}

		createRequestJson(enquiryRequestJson, df.data(), countries, sonsAge, daughtersAge, travellersAge, studentsAge);

		if (categoryListNode != null && categoryListNode.hasNonNull("items")
				&& categoryListNode.get("items").get(0) != null
				&& categoryListNode.get("items").get(0).hasNonNull("id")) {
			enquiryRequestJson.put("categoryId", categoryListNode.get("items").get(0).get("id").asText());
		}

		if (session("vid") != null) {
			enquiryRequestJson.put("visitorId", session("vid"));
		}

		/* TODO only for logged in user */
		// enquiryRequestJson.put("userId", "1");

		responseJson.set("restrictions", procurementServ
				.getRestrictionForAttributes(travelPreExistingRestriction(), mandatHP).toCompletableFuture().join());
		responseJson.set("products",
				procurementServ.createEnquiry(mandatQP, mandatHP, enquiryRequestJson).toCompletableFuture().join());

		return CompletableFuture
				.completedFuture(ok(com.arka.consumerportal.procurementweb.views.html.travel.travel_quotes
						.render(responseJson, propUtils)));
	}

	private ObjectNode createRequestJson(ObjectNode enquiryRequestJson, Map<String, String> data,
			List<String> countries, List<String> sonsAge, List<String> daughtersAge, List<String> travellersAge,
			List<String> studentsAge) {

		ArrayNode enquiryAttribute = enquiryRequestJson.putArray("enquiryAttributes");

		if (data.containsKey("tripStartDate") && !Validation.isNullOrEmpty(data.get("tripStartDate").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVEL_START_DATE.getCode(),
					data.get("tripStartDate").toString()));
			session().put("tripStartDate", data.get("tripStartDate").toString());
		} else {

		}
		if (data.containsKey("tripEndDate") && !Validation.isNullOrEmpty(data.get("tripEndDate").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVEL_END_DATE.getCode(),
					data.get("tripEndDate").toString()));
			session().put("tripEndDate", data.get("tripEndDate").toString());
		} else {

		}
		if (data.containsKey("isResidenOfIndia")
				&& !Validation.isNullOrEmpty(data.get("isResidenOfIndia").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_IS_RESIDENT_OF_INDIA.getCode(), "true"));
			session().put("isResidenOfIndia", data.get("isResidenOfIndia").toString());
		} else {

		}
		/*
		 * if(data.containsKey("preExistingDisease") &&
		 * !Validation.isNullOrEmpty(data.get("preExistingDisease").toString()))
		 * { enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.
		 * ATR_PRE_EXISITING_DISEASES_OTHERS.getCode(), "cold"));
		 * 
		 * }else{
		 * 
		 * }
		 */
		if (data.containsKey("isTripFromIndia") && !Validation.isNullOrEmpty(data.get("isTripFromIndia").toString())) {
			enquiryAttribute
					.add(getEnquiryAttribute(TravelAttributeCode.ATR_IS_TRIP_STARTING_FROM_INDIA.getCode(), "true"));
			session().put("isTripFromIndia", data.get("isTripFromIndia").toString());
		} else {

		}
		if (data.containsKey("medicalCover") && !Validation.isNullOrEmpty(data.get("medicalCover").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode(),
					INITIAL_MEDICAL_COVER.toString()));
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode(), "$"));
		} else {

		}
		if (data.containsKey("visitingCountries")
				&& !Validation.isNullOrEmpty(data.get("visitingCountries").toString())) {

			if (countries.size() > 0) {
				Map<String, String> contriesMap = new LinkedHashMap<>();
				for (String c : countries) {
					enquiryAttribute
							.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVELLER_DESTINATION.getCode(), c));
					contriesMap.put(c, c);
				}
				session().put("visitingCountries", Json.toJson(contriesMap).toString());
			}
		} else {

		}
		if (data.containsKey("insuranceType") && !Validation.isNullOrEmpty(data.get("insuranceType").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_TYPE.getCode(),
					data.get("insuranceType").toString().toUpperCase()));
			session().put("insuranceType", data.get("insuranceType").toString());
		} else {

		}
		if (data.containsKey("individualAge") && !Validation.isNullOrEmpty(data.get("individualAge").toString())) {
			enquiryAttribute.add(
					getEnquiryAttribute(TravelAttributeCode.ATR_SELF_AGE.getCode(), data.get("selfAge").toString()));
			session().put("individualAge", data.get("individualAge").toString());
		} else {

		}
		if (data.containsKey("selfAge") && !Validation.isNullOrEmpty(data.get("selfAge").toString())) {
			enquiryAttribute.add(
					getEnquiryAttribute(TravelAttributeCode.ATR_SELF_AGE.getCode(), data.get("selfAge").toString()));
			session().put("selfAge", data.get("selfAge").toString());
		} else {

		}
		if (data.containsKey("spouseAge") && !Validation.isNullOrEmpty(data.get("spouseAge").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_SPOUSE_AGE.getCode(),
					data.get("spouseAge").toString()));
			session().put("spouseAge", data.get("spouseAge").toString());
		} else {

		}
		if (data.containsKey("fatherAge") && !Validation.isNullOrEmpty(data.get("fatherAge").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_FATHER_AGE.getCode(),
					data.get("fatherAge").toString()));

			session().put("fatherAge", data.get("fatherAge").toString());
		} else {
		}
		if (data.containsKey("motherAge") && !Validation.isNullOrEmpty(data.get("motherAge").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_MOTHER_AGE.getCode(),
					data.get("motherAge").toString()));

			session().put("motherAge", data.get("motherAge").toString());
		} else {

		}
		if (data.containsKey("sonAge") && !Validation.isNullOrEmpty(data.get("sonAge").toString())) {

			if (sonsAge.size() > 0) {
				ArrayNode ageNode = Json.newArray();

				for (String age : sonsAge) {
					enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_SON_AGE.getCode(), age));
					ageNode.add(age);
				}
				session().put("sonsAge", ageNode.toString());
			}
		} else {

		}
		if (data.containsKey("daughterAge") && !Validation.isNullOrEmpty(data.get("daughterAge").toString())) {

			if (daughtersAge.size() > 0) {
				ArrayNode ageNode = Json.newArray();
				for (String age : daughtersAge) {
					enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_DAUGHTER_AGE.getCode(), age));
					ageNode.add(age);
				}
				session().put("daughtersAge", ageNode.toString());
			}
		} else {

		}

		if (data.containsKey("travellerAge") && !Validation.isNullOrEmpty(data.get("travellerAge").toString())) {

			if (travellersAge.size() > 0) {
				ArrayNode ageNode = Json.newArray();
				for (String age : travellersAge) {
					enquiryAttribute
							.add(getEnquiryAttribute(TravelAttributeCode.ATR_GROUP_TRAVELLER_AGE.getCode(), age));
					ageNode.add(age);
				}
				session().put("travellersAge", ageNode.toString());
			}
		} else {

		}

		if (data.containsKey("studentAge") && !Validation.isNullOrEmpty(data.get("studentAge").toString())) {

			if (studentsAge.size() > 0) {
				ArrayNode ageNode = Json.newArray();
				for (String age : studentsAge) {
					enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_STUDENT_AGE.getCode(), age));
					ageNode.add(age);
				}
				session().put("studentsAge", ageNode.toString());
			}
		} else {

		}
		if (data.containsKey("isAnualPlanMultiTrip")
				&& !Validation.isNullOrEmpty(data.get("isAnualPlanMultiTrip").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_IS_ANNUAL_MULTI_TRIP.getCode(), "true"));
			session().put("isAnualPlanMultiTrip", data.get("isAnualPlanMultiTrip").toString());
		} else {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_IS_ANNUAL_MULTI_TRIP.getCode(), "false"));
		}

		if (data.containsKey("tripFrequency") && !Validation.isNullOrEmpty(data.get("tripFrequency").toString())) {
			enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRIP_FREQUENCY.getCode(),
					data.get("tripFrequency").toString()));

			session().put("tripFrequency", data.get("tripFrequency").toString());
		} else {

		}
		enquiryAttribute.add(getEnquiryAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode(), "$"));
		return enquiryRequestJson;
	}

	public CompletionStage<Result> quotes() {

		ObjectNode enquiryRequestJson = Json.newObject();

		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		return CompletableFuture.completedFuture(
				ok(com.arka.consumerportal.procurementweb.views.html.travel.travel_quotes.render(procurementServ
						.createEnquiry(mandatQP, mandatHP, enquiryRequestJson).toCompletableFuture().join(),
						propUtils)));

	}

	public CompletionStage<Result> updateTravelEnquiry() {
		DynamicForm df = formFactory.form().bindFromRequest();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		String enquiryId = df.get("enquiryId");

		ArrayNode dataJsonArrayQuery = JsonUtils.newJsonArray();
		ObjectNode dataJsonWithQuery = JsonUtils.newJsonObject();
		dataJsonWithQuery.put(TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode(), df.get("medicalCover"));
		dataJsonArrayQuery.add(dataJsonWithQuery);

		ObjectNode patchJson = Json.newObject();
		patchJson.put(UpdateNames.OP.value(), JsonUpdateOperations.ADD.value());
		patchJson.put(UpdateNames.PATH.value(), "/enquiryAttributes");
		patchJson.set(UpdateNames.VALUE.value(), dataJsonArrayQuery);

		return CompletableFuture.completedFuture(ok(procurementServ
				.updateEnquiry(enquiryId, mandatQP, mandatHP, patchJson).toCompletableFuture().thenComposeAsync(s -> {
					Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
					queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.PRODUCT.value()));

					return procurementServ.getEnquiry(enquiryId, mandatQP, mandatHP);
				}).join()));
	}

	private JsonNode getEnquiryAttribute(String code, String value) {
		ObjectNode enquiryAttribute = Json.newObject();
		enquiryAttribute.put("categoryAttributeCode", code);
		enquiryAttribute.put("value", value);
		return enquiryAttribute;
	}

	public CompletionStage<Result> fetchPlan() {
		ObjectNode responseJson = Json.newObject();
		return CompletableFuture.completedFuture(ok(responseJson));
	}

	public WebSocket socket() {
		Request request = Context.current().request();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> headerMap = new HashMap<>();
		if (session().containsKey(SessionTrackingParams.SESSION_ID.getValue())
				&& session().get(SessionTrackingParams.SESSION_ID.getValue()) != null) {
			headerMap.put(HeaderParams.SESSION_ID.getValue(),
					Arrays.asList(session().get(SessionTrackingParams.SESSION_ID.getValue())));
		}
		if (CookieUtils.isContainKey(request, SessionTrackingParams.VISITOR_ID.getValue())
				&& CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue()) != null) {
			headerMap.put(HeaderParams.VISITOR_ID.getValue(),
					Arrays.asList(CookieUtils.getCookieValue(request, SessionTrackingParams.VISITOR_ID.getValue())));
		}
		return WebSocket.Json.accept(reqHeader -> {
			return Flow.fromFunction(planReqJson -> {
				Map<String, List<String>> queryString = new HashMap<>();
				queryString.put("source", Arrays.asList("correlationid"));
				queryString.putAll(mandatQP);
				return sessionServ.generateUniqueId(queryString, mandatHP, null).thenApplyAsync(sessionResp -> {
					headerMap.put(HeaderParams.CORRELATION_ID.getValue(),
							Arrays.asList(sessionResp.get("correlationId").asText()));
					headerMap.putAll(mandatHP);
					return procurementServ.fetchPlan(mandatQP, headerMap, planReqJson).toCompletableFuture().join();

				}).toCompletableFuture().get();
			});
		});
	}

}
