/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.arka.common.utils.JsonUtils;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * @author arul.p
 *
 */
public class SortProductController extends Controller {
	
	
	private final static String ASCENDING_ORDER_KEY="ASC";
	private final static String DESCENDING_ORDER_KEY="DESC";
	private final static String BESTMATCH_KEY="BEST_MATCH";
	
	private final static String ORDER_KEY="OREDER_KEY";
	private final static String PRODUCT_ARRAY="PRODUCT_ARRAY";
	private final static String SORTFIELD_NAME="SORTFIELD_NAME";
	
	
	private final static String IDV="idv";
	private final static String PREMIUM="premium";
	
	
	public CompletionStage<Result> sortProduct(){
		
		
		JsonNode productJson=request().body().asJson();
		List<ObjectNode> sortAscending=new ArrayList<>();
		if(JsonUtils.isValidField(productJson, ORDER_KEY) && JsonUtils.isValidField(productJson, PRODUCT_ARRAY)){
			
			
			List<ObjectNode> productList=getProductList(productJson);
			
			if(productJson.get(ORDER_KEY).asText().equals(ASCENDING_ORDER_KEY)){
				sortAscending = sortAscending(productList,productJson.get(SORTFIELD_NAME).asText());
			}else if(productJson.get(ORDER_KEY).asText().equals(DESCENDING_ORDER_KEY)){
				sortAscending = sortDescending(productList,productJson.get(SORTFIELD_NAME).asText());
			}else if(productJson.get(ORDER_KEY).asText().equals(BESTMATCH_KEY)){
				sortAscending = bestMatchForCar(productList,productJson.get(SORTFIELD_NAME).asText());
			}
		}
		
		return CompletableFuture.completedFuture(ok(JsonUtils.toJson(sortAscending)));
	}
	
	
	private List<ObjectNode> sortAscending(List<ObjectNode> productList,String key){
		productList.sort((JsonNode a, JsonNode b) -> {
			double valA=0;
			double valB=0;
	         try {
	        	 if(JsonUtils.isValidField(a, key) && JsonUtils.isValidField(b, key)){
	        		 
	        		 valA = a.get(key).asDouble();
		             valB = b.get(key).asDouble();
	        	 }
	            
	         } 
	         catch (Exception e) {
	             throw new ArkaRunTimeException(e);
	         }
	         
	         return Double.compare(valA, valB);
		});
		
		return productList;
	}
	
	private List<ObjectNode> sortDescending(List<ObjectNode> productList,String key){
		Collections.reverse(sortAscending(productList, key));
		return productList;
	}
	
	private List<ObjectNode> bestMatchForCar(List<ObjectNode> productList,String key){
		
		
		double idvDeviation = getAverage(productList,IDV);
		double premiumDeviation = getAverage(productList,PREMIUM);
		
		productList.parallelStream().forEach(product->{
			if(JsonUtils.isValidField(product, IDV) && JsonUtils.isValidField(product, PREMIUM)){
				try{
					
					double idv=product.get(IDV).asDouble();
					double premium=product.get(PREMIUM).asDouble();
					double idvDevPer=(product.get(PREMIUM).asDouble()/product.get(IDV).asDouble())*100.0;
					double idvDev=idv-idvDeviation;
					double idvDevVal=Math.sqrt(idvDev*idvDev);
					double premDev=premium-premiumDeviation;
					double premDevVal=Math.sqrt(premDev*premDev);
					
					product.put("idvDevPer", idvDevPer);
					product.put("idvDevVal", idvDevVal);
					product.put("premDevVal", premDevVal);
					
				}catch(Exception e){
					throw new ArkaRunTimeException(e);
				}
			}
		});
		
		sortingAscWithRank(productList, "idvDevPer", "idvDevPerRank");
		sortingAscWithRank(productList, "idvDevVal", "idvDevValRank");
		sortingAscWithRank(productList, "premDevVal", "premDevValRank");
		
		productList.parallelStream().forEach(product->{
			
			double overAllRankAverage=((50.0/100.0)*product.get("idvDevPerRank").asDouble())
						+((20.0/100.0)*product.get("idvDevValRank").asDouble())
						+((30.0/100.0)*product.get("premDevValRank").asDouble());
			product.put("overAllRankAverage",overAllRankAverage);
		});
		
		
		sortingAscWithRank(productList, "overAllRankAverage", "overAllRankAverageRank");
		
		return productList;
	}
	
	
	private List<ObjectNode> sortingAscWithRank(List<ObjectNode> productList,String key,String rankKey){
		 double prevsortBasedKey=0;
		 int rankNo=0;
		 int sameValCounter=0;
		 
		 for (ObjectNode product : sortAscending(productList, key))
		 {
		   if (Double.compare(product.get(key).asDouble(), prevsortBasedKey)==0)
		   {
			  sameValCounter ++;
		   }else{
			   rankNo++;
			   if(sameValCounter!=0){
				   rankNo +=sameValCounter;
				   sameValCounter=0;
			   }
			   
		   }
		   product.put(rankKey, rankNo);
		   prevsortBasedKey = product.get(key).asDouble();
		 }
		 
		return productList;
	}
	
	private Double getAverage(List<ObjectNode> productList,String averageKey){
		
		OptionalDouble average = productList
	            .stream()
	            .mapToDouble(a -> {
	            	return a.get(averageKey).asInt();
	            }).average();
		
		return average.getAsDouble();
	} 
	
	private List<ObjectNode> getProductList(JsonNode productJson){
		
		List<ObjectNode> productList=new ArrayList<>();
		
		if(JsonUtils.isValidField(productJson, PRODUCT_ARRAY) && JsonUtils.isValidIndex(productJson.get(PRODUCT_ARRAY), 0)){
			productJson.get(PRODUCT_ARRAY).forEach(product->{
				productList.add((ObjectNode)product);
			});
		}
		
		return productList;
	}
	
	

}
