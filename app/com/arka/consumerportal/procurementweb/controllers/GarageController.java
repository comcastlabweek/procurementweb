/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.procurement.constants.Source;
import com.arka.common.services.PolicyService;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.UserServ;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.procurementweb.services.VendorService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

public class GarageController extends Controller {

	@Inject
	private ProcurementServ procurementServ;
	
	@Inject
	private VendorService vendorService;

	@Inject
	private UserServ userService;
	
	@Inject
	private PropUtils propUtils;

	@Inject
	PolicyService policyService;
	
	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	MandatoryParams mandatoryParams;

	public CompletionStage<Result> getGarageList() {
		Map<String, String[]> queryString = request().queryString();
		String vendorCode = request().getQueryString("vendorCode");
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.GARAGE_LIST.value()));
		map.put("vendorCode", Arrays.asList(vendorCode));
		return procurementServ.getGarages(map, mandatHP).thenApplyAsync(response -> {
			if (JsonUtils.isValidField(response, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, response.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				data.put("vendorCode", vendorCode);
				return ok(com.arka.consumerportal.procurementweb.views.html.garage.garage_list.render(data, propUtils));
			} else {
				return ok(errorMsg());
			}
		}, httpExecutionContext.current());
	}
	
	public CompletionStage<Result> viewGarageDetails(String policyNumber) throws InterruptedException, ExecutionException {
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.GARAGE_LIST.value()));
		mandatQP.put("policyNumber", Arrays.asList(policyNumber));
		JsonNode policy = policyService.getPoliciesByField(mandatQP, mandatHP).toCompletableFuture().get();
		//JsonNode policy = policyService.getPolicyByNumber(policyNumber, mandatQP, mandatHP).toCompletableFuture().get();
		JsonNode vendor = vendorService.getVendor(policy.get("vendorId").asText(), mandatQP, mandatHP).toCompletableFuture().get();
		
		if(vendor != null && vendor.get("error") == null) {	
			map.put("vendorCode", Arrays.asList(vendor.get("code").asText()));
			return procurementServ.getGarages(map, mandatHP).thenApplyAsync(response -> {
				if (JsonUtils.isValidField(response, JsonUtils.JSON_ARRAY_ITEMS_KEY) && response.get("items").size() > 0) {
					ObjectNode data = Json.newObject();
					
					JsonNode referrerDetails = null;
					JsonNode mispBookingJson = null;
					try {
						mispBookingJson = policyService.getMispBooking(policy.get("mispBookingId").asText(), mandatQP, mandatHP).toCompletableFuture().get();
						referrerDetails = userService.getReferrerById(mispBookingJson.get("mispId").asText(),mandatQP,mandatHP).toCompletableFuture().get();
					} catch (Exception e) {
						System.out.println("error " + e);
					}
					
					boolean isMisp = false;
					if(policy.get("referrerCode") != null) {
						isMisp = true;
					}
					data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, response.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
					data.put("vendorCode", vendor.get("code").asText());
					data.put("policyId", policy.get("id").asText());
					data.put("isMisp", isMisp);
					data.set("referrerDetails", referrerDetails);
					
					return ok(com.arka.consumerportal.procurementweb.views.html.garage.garage_list_popup.render(data, propUtils));
				} else {
					return redirect("/myaccount");
				}
			}, httpExecutionContext.current());
		}
		return null;
	}

	public Result changeGarage() throws InterruptedException, ExecutionException {
		Map<String, String[]> queryString = request().queryString();
		
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		
		String policyId = queryString.get("policyId")[0];
		String garageCode = queryString.get("garageCode")[0];
		
		ObjectNode detailsJson = Json.newObject();
		detailsJson.put("op", "replace");
		detailsJson.put("path", "/service_provider_id");
		detailsJson.put("value", garageCode);
		
		JsonNode policyResp = policyService.patchPolicy(policyId, mandatQP, mandatHP, detailsJson).toCompletableFuture().get();
		
		if(policyResp.get("error") == null) {
			return redirect("/myaccount");
		}
		return null;
	}
	
	public CompletionStage<Result> getGarageCity() {
		Map<String, String[]> queryString = request().queryString();
		String vendorCode = request().getQueryString("vendorCode");
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.putAll(mandatQP);
		map.put(Source.SRC.value(), Arrays.asList(Source.CITY.value()));
		return procurementServ.getGarages(map, mandatHP).thenApplyAsync(resp -> {
			if (JsonUtils.isValidField(resp, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, resp.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				data.put("vendorCode", vendorCode);
				return ok(data);
			} else {
				return ok(errorMsg());
			}
		});
	}
	public Result setPreferredGarage() {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		JsonNode jsonNode = request().body().asJson();
		if( jsonNode.get("policyId") != null && jsonNode.get("garageCode") != null){
			ObjectNode detailsJson = Json.newObject();
			detailsJson.put("op", "replace");
			detailsJson.put("path", "/service_provider_id");
			detailsJson.put("value", jsonNode.get("garageCode").asText());
			try {
				JsonNode policyResp = policyService.patchPolicy(jsonNode.get("policyId").asText(), mandatQP, mandatHP, detailsJson).toCompletableFuture().get();
				return ok();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}


		}
		return ok(Json.toJson("{}"));

	}

	public CompletionStage<Result> getCityDetails() {
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.put(Source.SRC.value(), Arrays.asList(Source.CITY_DETAILS.value()));
		map.putAll(mandatQP);
		return procurementServ.getGarages(map, mandatHP).thenApplyAsync(resp -> {
			if (JsonUtils.isValidField(resp, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, resp.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				return ok(data);
			} else {
				return ok(errorMsg());
			}
		});
	}

	public CompletionStage<Result> getPostalDetails() {
		Map<String, String[]> queryString = request().queryString();
		Map<String, List<String>> map = CriteriaUtils.queryStringArrayToList(queryString);
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		map.put(Source.SRC.value(), Arrays.asList(Source.POSTALCODE.value()));
		map.putAll(mandatQP);
		return procurementServ.getGarages(map, mandatHP).thenApplyAsync(resp -> {
			if (JsonUtils.isValidField(resp, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				ObjectNode data = Json.newObject();
				data.set(JsonUtils.JSON_ARRAY_ITEMS_KEY, resp.get(JsonUtils.JSON_ARRAY_ITEMS_KEY));
				return ok(data);
			} else {
				return ok(errorMsg());
			}
		});
	}

	private JsonNode errorMsg() {
		ObjectNode error = Json.newObject();
		error.put("error", "Invalid data");
		return error;
	}
}
