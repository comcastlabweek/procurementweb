/**
 * 
 */
package com.arka.consumerportal.procurementweb.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.logging.constants.PayloadDataKeyEvent;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.SessionServ;
import com.arka.common.utils.CookieUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.procurementweb.constants.ProcurementErrors;
import com.arka.consumerportal.procurementweb.util.ProcurementUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Results;

/**
 * @author himagirinathan
 *
 */
public class MotorInsuranceController extends Controller {

	@Inject
	FormFactory formfactory;

	@Inject
	private ProcurementServ procurementServ;

	@Inject
	private PropUtils propUtils;

	@Inject
	AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	@Inject
	HttpExecutionContext httpExecutionContext;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	SessionServ sessionServ;

	@Inject
	HttpExecutionContext executionContext;
	
	@Inject
	ProcurementUtils procurementUtils;
	

	public CompletionStage<Result> carInsuranceLandingPage() {

		if (session("mispId") != null) {
			return toCompletionStage(
					redirect(com.arka.consumerportal.procurementweb.controllers.misp.routes.MISPController.homePage()));
		}
		Map<String, List<String>> queryStringMap = new HashMap<>();
		queryStringMap.put("vehicle_type", Arrays.asList("CAR"));
		ObjectNode payLoadData = Json.newObject();
		payLoadData.set(PayloadDataKeyEvent.SERVICE_INPUT_PARAMS.name(), Json.toJson(queryStringMap));
		ArrayNode errMsg = Json.newArray();
		Arrays.asList(ProcurementErrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		return procurementServ.getVehicleMake(queryStringMap, mandatHP).thenApplyAsync(makeModelJson -> {
			JsonNode rtoData = JsonUtils.newJsonObject();
			ArrayNode rtoResp = JsonUtils.newJsonArray();
			payLoadData.put(PayloadDataKeyEvent.MESSAGE.name(), PayloadDataKeyEvent.HTTP_STATUS_CODE_OK.name());
			Map<String, List<String>> queryString = new HashMap<>();
			queryString.put("nor", Arrays.asList("20"));
			queryString.put("pfield", Arrays.asList("city"));
			queryString.put("distinct", Arrays.asList("distinct"));
			rtoData = procurementServ.getRtoDetailsForCarInsurance(queryString, mandatHP).toCompletableFuture().join();
			if (anyCollectionQueryResponseValidator.isValidAnyCollectionQueryResp().test(rtoData)) {
				rtoResp = (ArrayNode) anyCollectionQueryResponseValidator.anyCollectionQueryGetData(rtoData);
			}
			session().put("home", "car");
			String enquiryId = "";
			Map<String,String> enquiryAttributeMap = new HashMap<>();
			if(CookieUtils.isContainKey(request(), SessionTrackingParams.CAR_ENQUIRY_ID.getValue())){
				enquiryId = CookieUtils.getCookieValue(request(), SessionTrackingParams.CAR_ENQUIRY_ID.getValue());
				procurementUtils.getEnquiryAttribute(mandatQP, mandatHP, enquiryId,enquiryAttributeMap);
			}
			((ObjectNode) makeModelJson).set("vehicleType", Json.toJson("car"));
			return ok(com.arka.consumerportal.procurementweb.views.html.motor.motorinsurance.render(makeModelJson,
					propUtils, errMsg, rtoResp,enquiryAttributeMap));
		}, httpExecutionContext.current());
	}

	
	public CompletionStage<Result> getModelForMake() {
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryStringMap.putAll(mandatQP);
		return procurementServ.getVehicleMakeModels(queryStringMap, mandatHP).thenApplyAsync(Results::ok);
	}

	public CompletionStage<Result> getFuelTypesForMakeModel() {
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryStringMap.putAll(mandatQP);
		return procurementServ.getFuelTypesForMakeModel(queryStringMap, mandatHP).thenApplyAsync(Results::ok);
	}

	public CompletionStage<Result> getVariantsForMakeModelFuelType() {
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryStringMap.putAll(mandatQP);
		return procurementServ.getVariantsForMakeModelFuel(queryStringMap, mandatHP).thenApplyAsync(Results::ok);
	}

	public CompletionStage<Result> getVehicleCodeForMakeModelFuelTypeVariant() {
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryStringMap.putAll(mandatQP);
		return procurementServ.getVehicleCodeForMakeModelFuelTypeVariant(queryStringMap, mandatHP)
				.thenApplyAsync(Results::ok);
	}

	// for bike temp
	public CompletionStage<Result> getRegistrationDetail(String regNo) {
		Map<String, List<String>> mandatQP =CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		return procurementServ.getRegistrationDetail(regNo, mandatQP, mandatHP).thenApplyAsync(Results::ok);
	}

	public CompletionStage<Result> getRtoDetail(String rtoCode) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		return procurementServ.getRtoDetail(rtoCode, mandatQP, mandatHP).thenApplyAsync(Results::ok);
	}

	public CompletionStage<Result> getRtoDetailsForCar() {
		Map<String, List<String>> mandatQP = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		return procurementServ.getRtoDetailsForCarInsurance(mandatQP, mandatHP).thenApplyAsync(Results::ok);
	}

	public CompletionStage<Result> getCollection() {
		Map<String, List<String>> queryStringMap = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		queryStringMap.putAll(mandatQP);

		return procurementServ.getVehicleDetailsForCarInsurance(queryStringMap, mandatHP).thenApply(Results::ok);
	}

	private CompletionStage<Result> toCompletionStage(Result result) {
		return CompletableFuture.supplyAsync(() -> result, httpExecutionContext.current());
	}

	// For Bike

	public CompletionStage<Result> bikeInsuranceLandingPage() {
		if (session("mispId") != null) {
			return toCompletionStage(
					redirect(com.arka.consumerportal.procurementweb.controllers.misp.routes.MISPController.homePage()));
		}

		// preparing query string to get CARS from Restrictions
		Map<String, List<String>> queryStringMap = new HashMap<>();
		queryStringMap.put("vehicle_type", Arrays.asList("BIKE"));
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		ObjectNode payLoadData = Json.newObject();
		payLoadData.set(PayloadDataKeyEvent.SERVICE_INPUT_PARAMS.name(), Json.toJson(queryStringMap));
		ArrayNode errMsg = Json.newArray();
		
		Arrays.asList(ProcurementErrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		
		return procurementServ.getVehicleMake(queryStringMap, mandatHP).thenApplyAsync(makeModelJson -> {
			JsonNode rtoData = JsonUtils.newJsonObject();
			ArrayNode rtoResp = JsonUtils.newJsonArray();
			payLoadData.put(PayloadDataKeyEvent.MESSAGE.name(), PayloadDataKeyEvent.HTTP_STATUS_CODE_OK.name());
			Map<String, List<String>> queryString = new HashMap<>();
			queryString.put("nor", Arrays.asList("20"));
			queryString.put("pfield", Arrays.asList("city"));
			queryString.put("distinct", Arrays.asList("distinct"));
			rtoData = procurementServ.getRtoDetailsForCarInsurance(queryString, mandatHP).toCompletableFuture().join();
			if (anyCollectionQueryResponseValidator.isValidAnyCollectionQueryResp().test(rtoData)) {
				rtoResp = (ArrayNode) anyCollectionQueryResponseValidator.anyCollectionQueryGetData(rtoData);
			}
			session().put("home", "bike");
			String enquiryId = "";
			Map<String,String> enquiryAttributeMap = new HashMap<>();
			if(CookieUtils.isContainKey(request(), SessionTrackingParams.BIKE_ENQUIRY_ID.getValue())){
				enquiryId = CookieUtils.getCookieValue(request(), SessionTrackingParams.BIKE_ENQUIRY_ID.getValue());
				procurementUtils.getEnquiryAttribute(mandatQP, mandatHP, enquiryId,enquiryAttributeMap);
			}
			((ObjectNode) makeModelJson).set("vehicleType", Json.toJson("bike"));
			return ok(com.arka.consumerportal.procurementweb.views.html.motor.motorinsurance.render(makeModelJson,
					propUtils, errMsg, rtoResp,enquiryAttributeMap));
		}, httpExecutionContext.current());
	}
}
