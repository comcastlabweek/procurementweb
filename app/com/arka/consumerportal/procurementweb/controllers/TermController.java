package com.arka.consumerportal.procurementweb.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.arka.common.constants.HeaderParams;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.productmgmt.constants.CategoryFields;
import com.arka.common.services.ProcurementServ;
import com.arka.common.services.SessionServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.travel.constants.TravelAttributeCode;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.Validation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import akka.actor.ActorSystem;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.WebSocket;

public class TermController extends Controller {

	@Inject
	PropUtils propUtils;

	@Inject
	private ProcurementServ procurementServ;
	
	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	MandatoryParams mandatoryParams;

	private static final Integer INITIAL_MEDICAL_COVER = 50000;

	public CompletionStage<Result> term() {

		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> queryString = new LinkedHashMap<>();
		queryString.put(CategoryFields.CATEGORY_CODE.value(), Arrays.asList(CategoryCode.TRAVEL.name()));
		queryString.put(CriteriaUtils.PFIELD.toString(),
				Arrays.asList(CategoryFields.CATEGORY_CODE.value(), CategoryFields.CATEGORY_STATUS.value()));
		session().put("home", "term");
		JsonNode responseJson = vendorProdMgmtServ.getCategories(queryString, mandatHP).toCompletableFuture().join();
		return CompletableFuture.completedFuture(ok(
				com.arka.consumerportal.procurementweb.views.html.term.term_home.render(responseJson, propUtils)));

	}

}
