package com.arka.consumerportal.procurementweb.models.misp.services;

import com.arka.consumerportal.procurementweb.models.misp.services.impl.SigninServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

@ImplementedBy(SigninServiceImpl.class)
public interface SigninService {

	CompletionStage<JsonNode> signIn(Map<String, List<String>> queryString, Map<String, List<String>> header,
                                     JsonNode bodyJson);

	CompletionStage<JsonNode> signout(Map<String, List<String>> queryString, Map<String, List<String>> header,
                                      JsonNode bodyJson);

	CompletionStage<JsonNode> sendNotification(Map<String, List<String>> queryString, Map<String, List<String>> header,
                                               JsonNode bodyJson);

	CompletionStage<JsonNode> changeOldPassword(Map<String, List<String>> queryString, Map<String, List<String>> header,
                                                JsonNode bodyJson);
}
