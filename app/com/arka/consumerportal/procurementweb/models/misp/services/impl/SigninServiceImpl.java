package com.arka.consumerportal.procurementweb.models.misp.services.impl;

import com.arka.common.services.SessionServ;
import com.arka.consumerportal.procurementweb.models.misp.services.SigninService;
import com.fasterxml.jackson.databind.JsonNode;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

public class SigninServiceImpl implements SigninService {

	@Inject
	SessionServ sessionServ;

	@Override
	public CompletionStage<JsonNode> signIn(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.signIn(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> signout(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.signout(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> sendNotification(Map<String, List<String>> queryString,
			Map<String, List<String>> header,JsonNode bodyJson) {
		return sessionServ.sendNotification(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> changeOldPassword(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.changeOldPassword(queryString, header, bodyJson);
	}

}
