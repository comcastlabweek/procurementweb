/**
 * 
 */
package com.arka.consumerportal.procurementweb.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.arka.common.web.interceptor.ArkaRunTimeException;

/**
 * @author himagirinathan
 *
 */
public class EnquiryRequest implements Serializable {

	private static final long serialVersionUID = -3290926511372879154L;

	public static class EnquiryAttribute implements Serializable {
		
		private static final long serialVersionUID = -5248009902499699843L;
		
		private String categoryAttributeCode;
		private String value;

		public EnquiryAttribute(String categoryAttributeCode, String value) {
			this.categoryAttributeCode = categoryAttributeCode;
			this.value = value;
		}

		public String getCategoryAttributeCode() {
			return categoryAttributeCode;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return "EnquiryAttribute [categoryAttributeCode=" + categoryAttributeCode + ", value=" + value + "]";
		}

	}

	public static class EnquiryRequestBuilder {

		private List<EnquiryAttribute> enquiryAttributes = new ArrayList<>();
		private String categoryId;
		private String userId;
		private String visitorId;

		public EnquiryRequestBuilder addEnquiryAttribute(String categoryAttributeCode, String value) {
			enquiryAttributes.add(new EnquiryAttribute(categoryAttributeCode, value));
			return this;
		}

		public EnquiryRequestBuilder setCategoryId(String categoryId) {
			this.categoryId = categoryId;
			return this;
		}

		public EnquiryRequestBuilder setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public EnquiryRequestBuilder setVisitorId(String visitorId) {
			this.visitorId = visitorId;
			return this;
		}

		public EnquiryRequest build() {
			Map<String,List<String>> error=new HashMap<>();
			if (categoryId == null || categoryId.isEmpty()) {
				error.put("category id error", Arrays.asList("category id can't be null or empty"));
				
			}
			// if (userId == null || userId.isEmpty()) {
			// throw new IllegalStateException("user id can't be null or
			// empty");
			// }
			if (visitorId == null || visitorId.isEmpty()) {
				error.put("visitor id error", Arrays.asList("visitor id can't be null or empty"));
			}
			if (enquiryAttributes == null || enquiryAttributes.isEmpty()) {
				error.put("enquiry attribute error", Arrays.asList("enquiry attribute can't be null or empty"));
			}
			
//			if(error.size()>0){
//				throw new ArkaRunTimeException("error", error);
//			}
			
			return new EnquiryRequest(categoryId, userId, visitorId, enquiryAttributes);
		}
	}

	private String categoryId;
	private String userId;
	private String visitorId;
	private List<EnquiryAttribute> enquiryAttributes;

	private EnquiryRequest(String categoryId, String userId, String visitorId,
			List<EnquiryAttribute> enquiryAttributes) {
		this.categoryId = categoryId;
		this.userId = userId;
		this.visitorId = visitorId;
		this.enquiryAttributes = enquiryAttributes;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public String getUserId() {
		return userId;
	}

	public String getVisitorId() {
		return visitorId;
	}

	public List<EnquiryAttribute> getEnquiryAttributes() {
		return enquiryAttributes;
	}

	@Override
	public String toString() {
		return "EnquiryRequest [categoryId=" + categoryId + ", userId=" + userId + ", visitorId=" + visitorId
				+ ", enquiryAttributes=" + (enquiryAttributes != null ? enquiryAttributes.size() : null) + "]";
	}

}
