/**
 * 
 */
package com.arka.consumerportal.procurementweb.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.QueryConstants;
import com.arka.common.services.ProcurementServ;
import com.arka.common.utils.JsonUtils;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

/**
 * @author ancyrajan
 *
 */
public class ProcurementUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ProcurementUtils.class);
	
	@Inject
	private ProcurementServ procurementServ;
	
	@Inject
	AnyCollectionQueryResponseValidator anyCollectionQueryResponseValidator;

	
	public void getEnquiryAttribute(Map<String, List<String>> mandatQP,Map<String, List<String>> mandatHP, String enquiryId,
			Map<String, String> enquiryAttributeMap) {
		Map<String, List<String>> queryMap = new HashMap<String, List<String>>();
		queryMap.put(QueryConstants.FETCH_COLUMN.value(), Arrays.asList(QueryConstants.CODE.value()));
		JsonNode enquiryResponseJson = procurementServ.getEnquiryAttributes(enquiryId, queryMap, mandatHP)
				.toCompletableFuture().join();
		ObjectNode getYearJson = JsonUtils.newJsonObject();
		if (JsonUtils.isValidField(enquiryResponseJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {					
			enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(e -> {
				if (JsonUtils.isValidField(e, "code") && e.get("code").asText().equals("AC-VEHCODE")) {
					String vehicleId = e.get("value").asText();
					try {
						Map<String, List<String>> queryString = new LinkedHashMap<>();
						queryString.put("code", Arrays.asList(vehicleId));
						queryString.putAll(mandatQP);
						JsonNode makeModelJson = procurementServ.getVehicleDetailsForCarInsurance(queryString, mandatHP)
								.toCompletableFuture().join();
						JsonNode makeModel = anyCollectionQueryResponseValidator
								.anyCollectionQueryGetData(makeModelJson);
						if (makeModel.size() > 0) {
							makeModel.get(0).fields().forEachRemaining(item -> {
									enquiryAttributeMap.put(item.getKey().toString(), item.getValue().asText());
								});
						}
					} catch (ArkaRunTimeException f) {
						logger.error(f.getMessage());
						throw new ArkaRunTimeException(f);
					}
				}
				if (JsonUtils.isValidField(e, "code")
						&& e.get("code").asText().equals(CarAttributeCode.AC_RTOCODE.getCode())) {
					String rtoCode = e.get("value").asText();
					try {
						Map<String, List<String>> queryStr = new LinkedHashMap<>();
						queryStr.put("code", Arrays.asList(rtoCode));
						queryStr.putAll(mandatQP);
						JsonNode rtoJson = procurementServ.getRtoDetailsForCarInsurance(queryStr, mandatHP)
								.toCompletableFuture().join();
						JsonNode rtoModel = anyCollectionQueryResponseValidator.anyCollectionQueryGetData(rtoJson);
						if (rtoModel.size() > 0) {
							rtoModel.get(0).fields().forEachRemaining(item -> {
									if (item.getKey().equals("code")) {
										enquiryAttributeMap.put("rto_code", item.getValue().asText());
									} else if (item.getKey().equals("city")) {
										enquiryAttributeMap.put("rto_city", item.getValue().asText());
									} else {
										enquiryAttributeMap.put(item.getKey().toString(), item.getValue().asText());
									}

								});
						}
					} catch (ArkaRunTimeException g) {
						logger.error(g.getMessage());
						throw new ArkaRunTimeException(g);
					}
				}	
				if (JsonUtils.isValidField(e, "code")
						&& e.get("code").asText().equals(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode() )) {
					getYearJson.put(e.get("code").asText(), e.get("value").asText());
				}
				if (JsonUtils.isValidField(e, "code")
						&& e.get("code").asText().equals(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode() )) {
					getYearJson.put(e.get("code").asText(), e.get("value").asText());
				}
				if (JsonUtils.isValidField(e, "code")
						&& e.get("code").asText().equals(CarAttributeCode.AC_VEHSTATE.getCode() )) {
					getYearJson.put(e.get("code").asText(), e.get("value").asText());
				}				
				enquiryAttributeMap.put(e.get("code").asText(), e.get("value").asText());
		});
			activeEntryTypeOfCall(enquiryResponseJson,enquiryAttributeMap);
			setyeartoMap(getYearJson,enquiryAttributeMap);
		}
	}

	private void setyeartoMap(ObjectNode getYearJson,Map<String, String> enquiryAttributeMap) {
		String regDate = "",vehicleState = "",manufactureDate = "";
		if (JsonUtils.isValidField(getYearJson, CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode())) {
			regDate = getYearJson.get(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode()).asText();
		}
		if (JsonUtils.isValidField(getYearJson, CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode())) {
			manufactureDate = getYearJson.get(CarAttributeCode.AC_CAR_MANUFACTURE_DATE.getCode()).asText();
		}
		if (JsonUtils.isValidField(getYearJson, CarAttributeCode.AC_VEHSTATE.getCode())) {
			vehicleState = getYearJson.get(CarAttributeCode.AC_VEHSTATE.getCode()).asText();
		}
		if(!regDate.isEmpty() && (!vehicleState.isEmpty() && !("newcar").equalsIgnoreCase(vehicleState))){
			enquiryAttributeMap.put("year", regDate.substring(0, 4));
		}else if(!manufactureDate.isEmpty() && (!vehicleState.isEmpty() && ("newcar").equalsIgnoreCase(vehicleState))){
			enquiryAttributeMap.put("year", manufactureDate.substring(0, 4));
		}
	}

	private void activeEntryTypeOfCall(JsonNode enquiryResponseJson,Map<String, String> enquiryAttributeMap) {
		ObjectNode classDefiner = JsonUtils.newJsonObject();
		if (JsonUtils.isValidField(enquiryResponseJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			enquiryResponseJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(e -> {
				if (JsonUtils.isValidField(e, "code") && JsonUtils.isValidField(e, "value")) {
					classDefiner.put(e.get("code").asText(), e.get("value").asText());
				}
			});
		}
		if (JsonUtils.isValidField(classDefiner, CarAttributeCode.AC_REGNO.getCode())) {
			enquiryAttributeMap.put("className", "js__iknowMy__carNo");
		} else if (JsonUtils.isValidField(classDefiner, CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			enquiryAttributeMap.put("className", "js__iDontknowMy__carNo");
		} else {
			enquiryAttributeMap.put("className", "js__buyNewCar");
		}
	}

}
