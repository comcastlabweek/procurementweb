package com.arka.consumerportal.procurementweb.util;

import java.util.Arrays;

import com.arka.consumerportal.procurementweb.constants.ProcurementErrors;
import com.fasterxml.jackson.databind.node.ArrayNode;

import play.libs.Json;

public class ErrorUtil {
	public static ArrayNode getProcurementErrors() {
		ArrayNode errMsg = Json.newArray();
		Arrays.asList(ProcurementErrors.values()).forEach(error -> {
			errMsg.add(error.value().toString());
		});
		return errMsg;
	}
}
