/**
 * 
 */
package com.arka.consumerportal.procurementweb.constants;

/**
 * @author arul.p
 *
 */
public enum GlossaryKeys {
	
	ZERO_DEPROCATION_TOOLTIP_BODY("Zero_Depreciation"),
	APPLY_DISCOUNT_TOOLTIP_BODY("Apply_Discounts"),
	ADDITIONAL_COVER_TOOLTIP_BODY("Addditional_Covers"),
	CAR_NUMBER_TOOLTIP_BODY("Car_Number"),
	NCB_PROTECTION_TOOLTIP_BODY("NCB"),
	IDV_TOOLTIP_BODY("IDV"),
	INVOICE_COVER_TOOLTIP_BODY("Invoice_Cover"),
	ROADSIDE_ASSISTANCE_TOOLTIP_BODY("24*7_Roadside_Assistance"),
	ENGINE_PROTECTOR_TOOTIP_BODY("Engine_protector"),
	DRIVER_COVER_TOOLTIP_BODY("Driver_cover"),
	PASSANGER_COVER_TOOLTIP_BODY("passanger_cover"),
	ACCESSORIES_TOOLTIP_BODY("Accessories"),
	POLICY_DETAILS_TOOLTIP_BODY("Policy_details");
	
	private String value;
	
	GlossaryKeys(String value){
		this.value=value;
	}
	
	public String value() {
		return this.value;
	}
}
