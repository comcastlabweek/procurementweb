package com.arka.consumerportal.procurementweb.constants;

public enum ProcurementErrors {

	
	INVALID_REGISTER_NUMBER("invalid.register.number"),
	INVALID_MODEL_NAME("invalid.model.name"),
	ENTER_VALID_REGISTER_NUMBER("enter.valid.register.number"),
	ENTER_VALID_CAR_DETAILS("enter.valid.car.details"),
	ENTER_VALID_FUEL_TYPE("enter.valid.fuel.type"),
	ENTER_VALID_CAR_MODEL("enter.valid.car.model"),
	ENTER_VALID_REG_YEAR("enter.valid.car.reg.year"),
	ENTER_VALID_REGISTRATION_NUMBER("enter.valid.registration.number"),
	ENTER_VALID_CITY("enter.valid.city"),
	SELECT_CLAIM_OR_NOT_CLAIM("select.claim.or.not.claim"),
	ENTER_VALID_CAR_DEATILS("enter.valid.car.details"),
	ENTER_VALID_RTO_DETAILS("enter.valid.rto.details"),
	SELECT_POLICY_STATUS("select.policy.status"),
	SELECT_CALIM_OR_NOT_CLAIM("select.claim.or.not.claim"),
	ENTER_VALID_NCGLPG_FITTED_VAL("enter.valid.ncglpg.val"),
	ENTER_BTW_NCGLPG_FITTED_VAL("enter.btw.cng.lpg.val"),
	ENTER_VALID_IDV_VAL("enter.valid.idv.val"),
	NOT_A_VALID_EXPIRY_DATE("not.valid.expiry.date"),
	YEAR_SHOULD_BE_GREATERTHAN_CURYEAR("year.greater.than.current.year"),
	YEAR_SHOULD_BE_LESSERTHAN_CURYEAR("year.lesser.than.current.year"),
	POLICY_EXPIRED_ALERT_MSG("policy.expired.alert.msg"),
	REG_DATE_ADVANCED_ALERT("reg.date.advanced.alert"),
	NOT_A_VALID_REG_DATE("not.valid.reg.date"),
	POLICY_WILL_EXPIRED_ALERT_MSG("policy.will.expire.alert.msg"),
	EXPIRY_DATE_CANT_GT_REG_DATE("expiry.date.cant.gt.reg.date"),
	EXPIRY_DATE_CANT_GT_REG_DATE_NINTY("expiry.date.cant.gt.ninty.date"),
	ENTER_VALID_RTO_CODE("enter.valid.rtocode"),
	EXPIRY_DATE_CANT_LESS_CUR_DATE("expiry.date.cant.less.current.date"),
	EXPIRY_DATE_CANT_GT_CUR_DATE("expiry.date.cant.gt.current.date"),
	UNABLE_TO_PROCESS("unable.to.process"),
	SUBSYSTEM_ERROR("subsystem.error"),
	INVALID_NOMINEE("invalid.nominee"),
	INVALID_REGNO("invalid.regno"),
	INVALID_MANUFACTUREYEAR("invalid.manufactureyear"),
	INVALID_ENGINENO("invalid.engineno"),
	INVALID_CHASISNO("invalid.chasisno"),
	INVALID_DOB("invalid.dob"),
	INVALID_PAN("invalid.pan"),
	INVALID_AADHARNO("invalid.aadharno"),
	INVALID_TITLE("invalid.title"),
	INVALID_NAME("invalid.name"),
	INVALID_GENDER("invalid.gender"),
	INVALID_ADDRESS("invalid.address"),
	INVALID_PINCODE("invalid.pincode"),
	INVALID_CITY("invalid.city"),
	INVALID_STATE("invalid.state"),
	INVALID_COUNTRY("invalid.country"),
	INVALID_OCCUPATION("invalid.occupation"),
	INVALID_MOBILE("invalid.mobile"),
	INVALID_EMAIL("invalid.email"),
	INVALID_STARTDATE("invalid.startdate"),
	INVALID_ENDDATE("invalid.enddate"),
	INVALID_VEHICLETYPE("invalid.vehicletype"),
	INVALID_VEHICLEMAKE("invalid.vehiclemake"),
	INVALID_VEHICLEMODEL("invalid.vehiclemodel"),
	INVALID_VEHICLEVARIANT("invalid.vehiclevariant"),
	INVALID_FUEL("invalid.fuel"),
	INVALID_ZONE("invalid.zone"),
	INVALID_REGDATE("invalid.regdate"),
	INVALID_REGLOCATION("invalid.regloc"),
	QUOTES_UNATTAINABLE_FROM_VENDOR("quotes.unattainable.from.vendor");
	
	private String value;

	
	ProcurementErrors(String value){
		this.value=value;
	}
	
	public String value() {
		return this.value;
	}
}
