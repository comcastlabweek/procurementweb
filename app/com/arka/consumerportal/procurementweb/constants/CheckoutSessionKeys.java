package com.arka.consumerportal.procurementweb.constants;

public enum CheckoutSessionKeys {
	
	CAR_KEY("CH_"),
	BIKE_KEY("BH_"),
	HEALTH_KEY("HE_");

	String code;

	private CheckoutSessionKeys(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
