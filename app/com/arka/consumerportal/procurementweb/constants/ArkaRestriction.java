/**
 * 
 */
package com.arka.consumerportal.procurementweb.constants;

/**
 * @author arul.p
 *
 */
public enum ArkaRestriction {
	
	
	
	PASSENGER_COVER_LIMIT("PASSENGER_COVER_LIMIT"),
	
	VOLUNTARY_DEDUCTABLE_AMOUNT("VOLUNTARY_DEDUCTABLE_AMOUNT"),
	
	VEHICLE_REGISTRATION_YEAR_LIMIT("VEHICLE_REGISTRATION_YEAR_LIMIT"),
	
	NCB_PERCENT("NCB_PERCENT"),
	
	APPLY_DISCOUNT_PROFESSION("APPLY_DISCOUNT_PROFESSION");
	
	private String value;

	ArkaRestriction(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}


}
