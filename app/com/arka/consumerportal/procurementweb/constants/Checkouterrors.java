package com.arka.consumerportal.procurementweb.constants;

public enum Checkouterrors {

	INVALID_ENTRY("give.common"),
	
	INVALID_REGISTRATION_NUMBER("give.registerno"),
	
	INVALID_AGE("give.age"),
	
	ERROR_NAME("error.fullname"), 
	
	ERROR_EMAILID("ifemail.has.error"), 
	
	ERROR_NUMBER("mobilenumber.error"),
	
	SHORT_NAME("shortname.hint"),
	
	CONTACT_HINT("contact.hints"),
	
	CONTACT_PERSON_NAME("contactname.hint"),
	
	CONTACT_PERSON_ERROR("contact.person.error"),
	
	INVALID_EMAILID("invalid.email"),
	
	INVALID_MOBILENUMBER("invalid.mobilenumber"),
	
	ERROR_NOMINEENAME("nomineename.error"),
	
	ERROR_NOMINEEAGE("nomineeage.error"),
	
	NOMINEEAGE_ERROR1("error1.nomineeAge"),
	
	NOMINEEAGE_ERROR2("error2.nomineeAge"),

	NOMINEEAGE_ERROR3("error3.nomineeAge"),
	
	INVALID_DATE("invalid.date"),
	
	VALID_NUMBER("number.valid"),
	
	ANNUALINCOME_ERROR("annual.income.error"),
	
	OCCUPATION_ERROR("occupation.error"),
	
	ERROR_NOMINEERELATIONS("nomineerelationships.error"),
	
	ERROR_ADDRESS("address.error"),
	
	ERROR_RESIDENCE_ADDRESS("address.error"),
	
	ERROR_GENDER("gender.error"),
	
	ERROR_MARITAL("marital.error"),
	
	ZERO_HEIGHT_ERROR("zero.height.error"),
	
	ZERO_WEIGHT_ERROR("zero.weight.error"),
	
	MAX_WEIGHT("max.weight"),
	
	NON_MATCHING_DOB("non.matching.d.o.b"),
	
	ERROR_PINCODE("pincode.error"),
	
	VALIDDOBERROR("validdob.error"),
	
	FIRSTNAME_ERROR("firstname.error"),
	
	LASTNAME_ERROR("lastname.error"),
	
	ERROR_STATE("state.error"),
	
	ERROR_CITY("city.error"),
	
	ERROR_DOB("dob.error"),
	
	ERROR_SHORT_NAME("validation.short"),
	
	ERROR_POLICYNUMBER("policynumber.error"),
	
	ERROR_ENGINENUMBER("enginenumber.error"),
	
	ERROR_CHASSISNUMBER("chassisnumber.error"),
	
	ERROR_POLICYINSURER("policyinsurer.error"),
	
	ERROR_LOANPROVIDER("loanprovider.error"),
	
	INVALID_TEXT("valid.text"),
	
	INVALID_NAME("valid.name"),
	
	INVALID_REGISTER_NUMBER("validregisternumber.error"),
	
	INVALID_YEAR("validyear.error"),
	
	ERROR_HEIGHT("height.error"),
	
	ERROR_WEIGHT("weight.error");

	private String value;

	Checkouterrors(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
